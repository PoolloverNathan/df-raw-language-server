use crate::MainToken;
use df_ls_core::DFRawFile;

pub type DebugRaw = DFRawFile<Vec<MainToken>>;
