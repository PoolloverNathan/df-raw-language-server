#![forbid(unsafe_code)]
#![deny(clippy::all)]

mod debug_raw;
mod unary_token_multiple_arguments;

pub use debug_raw::DebugRaw;
use df_ls_core::{
    AllowEmpty, Any, ArgN, BangArgNSequence, Choose, Clamp, DFChar, PipeArguments, Reference,
    ReferenceTo, Referenceable,
};
use df_ls_syntax_analysis::TokenDeserialize;
pub use unary_token_multiple_arguments::DebugUnaryTokenEnum;

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct RefToken {
    /// Argument 1
    #[df_token(token_name = "REF")]
    pub reference: Option<Reference>,
    /// Token `NAME`
    #[df_token(token_name = "NAME")]
    pub name: Vec<String>,
}

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
#[df_token(second_par_check, token_name = "MAIN")]
pub struct MainToken {
    #[df_token(token_name = "TYPE1")]
    pub type_1: Vec<Type1Token>,
    #[df_token(token_name = "TYPE2")]
    pub type_2: Vec<Type2Token>,
    #[df_token(token_name = "TYPE3")]
    pub type_3: Vec<String>,
    #[df_token(token_name = "TYPE4")]
    pub type_4: Vec<Type4Token>,
    #[df_token(token_name = "PROFESSION")]
    pub professions: Vec<ProfessionToken>,

    #[df_token(token_name = "DEBUG")]
    pub debug: Vec<DebugTokens>,
}

/// List for easy debug
#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct DebugTokens {
    #[df_token(token_name = "ALLOW_EMPTY")]
    pub allow_empty: Vec<AllowEmpty<Any>>,
    #[df_token(token_name = "ANY")]
    pub any: Vec<Any>,
    #[df_token(token_name = "ARG_N")]
    pub arg_n: Vec<ArgN>,
    #[df_token(token_name = "ARG_N_2")]
    pub arg_n_2: Option<ArgN>,
    #[df_token(token_name = "ARG_N_CLAMP")]
    pub arg_n_clamp: Vec<Clamp<ArgN, 1, 5>>,
    #[df_token(token_name = "BOOL")]
    pub ref_bool: Vec<bool>,
    #[df_token(token_name = "CHAR")]
    pub ref_char: Vec<char>,
    #[df_token(token_name = "CHOOSE")]
    pub choose: Vec<Choose<i32, String>>,
    #[df_token(token_name = "DF_CHAR")]
    pub df_char: Vec<DFChar>,
    #[df_token(token_name = "INT_1")]
    pub int_1: Vec<i32>,
    #[df_token(token_name = "INT_2")]
    pub int_2: Vec<u32>,
    #[df_token(token_name = "OPTION")]
    pub option: Vec<Option<u32>>,
    #[df_token(token_name = "REF_TO")]
    pub reference_to: Vec<ReferenceTo<ProfessionToken>>,
    #[df_token(token_name = "REF")]
    pub reference: Vec<Reference>,
    #[df_token(token_name = "STRING")]
    pub string: Vec<String>,
    #[df_token(token_name = "TUPLE")]
    pub tuple: Vec<(i32, u32, String)>,
    #[df_token(token_name = "VEC")]
    pub vec: Vec<(Vec<u32>,)>,
    #[df_token(token_name = "PIPE_ARG")]
    pub pipe_arg: Vec<PipeArguments>,
    #[df_token(token_name = "TUPLE_PIPE_ARG")]
    pub tuple_pipe_arg: Vec<(i32, PipeArguments, i32)>,
    #[df_token(token_name = "BANG_ARG_N_SEQ")]
    pub bang_arg_n_seq: Vec<BangArgNSequence>,
    #[df_token(token_name = "BANG_ARG_N_SEQ_CLAMP")]
    pub bang_arg_n_seq_clamp: Vec<Clamp<BangArgNSequence, 1, 5>>,
    #[df_token(token_name = "ENUM_VALUE_VEC")]
    pub enum_value: Vec<(Vec<DebugEnum>,)>,
    #[df_token(token_name = "UNARY_TOKEN_MULTI_ARG")]
    pub unary_token_multi_arg: Vec<DebugUnaryTokenEnum>,

    #[df_token(token_name = "ADDED_OLD")]
    #[df_added(since = "0.30.00")]
    pub added_old: Vec<String>,
    #[df_token(token_name = "ADDED_RECENT")]
    #[df_added(since = "0.47.01")]
    pub added_recent: Vec<String>,
    #[df_token(token_name = "ADDED_FUTURE")]
    #[df_added(since = "1.99.01")]
    pub added_future: Vec<String>,

    #[df_token(token_name = "DEPRECATED_OLD")]
    #[df_deprecated(since = "0.30.00")]
    pub deprecated_old: Vec<String>,
    #[df_token(token_name = "DEPRECATED_RECENT")]
    #[df_deprecated(since = "0.47.01")]
    pub deprecated_recent: Vec<String>,
    #[df_token(token_name = "DEPRECATED_FUTURE")]
    #[df_deprecated(since = "1.99.01")]
    pub deprecated_future: Vec<String>,

    #[df_token(token_name = "ADDED_AND_DEPRECATED")]
    #[df_added(since = "0.47.01", note = "Recent addition")]
    #[df_deprecated(since = "1.99.01", note = "Announced to be removed.")]
    pub add_and_deprecated: Vec<String>,

    #[df_token(token_name = "TOKEN_ISSUE_OLD")]
    #[df_issue(since = "0.32.01", fixed_in = "0.33.01")]
    pub token_issue_old: Vec<String>,
    #[df_token(token_name = "TOKEN_ISSUE_RECENT")]
    #[df_issue(since = "0.47.01", fixed_in = "0.51.01")]
    pub token_issue_recent: Vec<String>,
    #[df_token(token_name = "TOKEN_ISSUE_FUTURE")]
    #[df_issue(since = "1.92.01", fixed_in = "1.93.01")]
    pub token_issue_future: Vec<String>,

    #[df_token(token_name = "TOKEN_ISSUE_INFO")]
    #[df_issue(
        since = "0.22.01",
        fixed_in = "1.93.01",
        link = "https://www.bay12games.com/dwarves/mantisbt/view.php?id=10369",
        severity = "INFO",
        note = "Issue has existed for a long time."
    )]
    pub token_issue_info: Vec<String>,
}

#[derive(Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum DebugEnum {
    #[df_token(token_name = "ENUM1")]
    Enum1,
    #[df_token(token_name = "ENUM2")]
    Enum2,
    #[df_token(token_name = "ENUM3")]
    Enum3,
    #[df_token(token_name = "ENUM4")]
    Enum4,
}
impl Default for DebugEnum {
    fn default() -> Self {
        Self::Enum1
    }
}

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct Type1Token {
    /// Argument 1
    #[df_token(token_name = "TYPE1", on_duplicate_to_parent, primary_token)]
    pub reference: Option<Reference>,
    /// Token `NAME`
    #[df_token(token_name = "ITEM")]
    pub list: Vec<Reference>,
}

#[allow(clippy::large_enum_variant)]
#[derive(Clone, Debug, TokenDeserialize, PartialEq, Eq)]
pub enum Type2Token {
    #[df_token(token_name = "UNIPEDAL")]
    Unipedal(String),
    #[df_token(token_name = "BIPEDAL")]
    Bipedal(HumanToken),
    #[df_token(token_name = "TRIPEDAL")]
    Tripedal(RefToken),
    #[df_token(token_name = "4LEGS")]
    Quadrupedal(RefToken),
}
impl Default for Type2Token {
    fn default() -> Self {
        Self::Unipedal(String::default())
    }
}

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable)]
pub struct HumanToken {
    /// Argument 1
    #[df_token(token_name = "BIPEDAL", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// Token `NAME`
    #[df_token(token_name = "NAME")]
    pub name: Option<String>,
    #[df_token(token_name = "DEAD", on_duplicate_error)]
    pub death: Option<()>,
    #[df_token(token_name = "SYMBOL")]
    pub symbol: Option<AllowEmpty<char>>,
    #[df_token(token_name = "GENDER")]
    pub gender: Option<GenderEnum>,
    #[df_token(token_name = "AGE")]
    pub age: Option<u8>,
    #[df_token(token_name = "JOB")]
    pub jobs: Vec<String>,
    #[df_token(token_name = "EDUCATION")]
    pub educations: Vec<(String, String, u8)>,
    #[df_token(token_name = "HOBBY")]
    pub hobbies: Vec<Choose<Reference, String>>,
    #[df_token(token_name = "MAIN_PROFESSION")]
    pub main_profession: Option<Choose<Reference, String>>,
    #[allow(clippy::type_complexity)]
    #[df_token(token_name = "MULTI")]
    pub multi_choose: Option<Choose<Reference, Choose<Choose<u32, i32>, DFChar>>>,
    #[df_token(token_name = "PROFESSION")]
    pub profession: Option<ProfessionToken>,
    #[df_token(token_name = "PROFESSIONS")]
    pub professions: Vec<(ReferenceTo<ProfessionToken>, u8)>,
}

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable)]
pub struct ProfessionToken {
    #[df_token(token_name = "PROFESSION", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    #[df_token(token_name = "NAME")]
    #[df_alias(token_name = "JOB", discouraged)]
    pub name: Vec<Reference>,
    #[df_token(token_name = "GENDER")]
    pub genders: Vec<GenderEnum>,
    #[df_token(token_name = "HAND")]
    pub handedness: Option<Choose<u8, HandednessEnum>>,
    /// Optional arguments
    #[df_token(token_name = "ARG")]
    pub argument: Option<(String, Option<String>)>,
    /// Optional arguments
    #[df_token(token_name = "ARG_VEC")]
    pub arguments: Vec<(String, Option<String>)>,
    #[df_token(token_name = "SYMBOL")]
    pub symbol: Option<DFChar>,
    #[df_token(token_name = "DIFFICULTY")]
    pub difficulty: Option<u8>,
    #[df_token(token_name = "DIFFICULTY_HARD", on_duplicate_error)]
    pub difficulty_hard: Option<u8>,
    #[df_token(token_name = "TOOL")]
    pub tool: Option<ToolToken>,
}

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable)]
pub struct ToolToken {
    #[df_token(token_name = "TOOL", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    #[df_token(token_name = "NAME")]
    pub name: Option<String>,
}

#[derive(Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum GenderEnum {
    #[df_token(token_name = "MALE")]
    Male,
    #[df_token(token_name = "FEMALE")]
    Female,
    #[df_token(token_name = "OTHER")]
    Other,
}

impl Default for GenderEnum {
    fn default() -> Self {
        Self::Other
    }
}

#[derive(Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum HandednessEnum {
    #[df_token(token_name = "LEFT")]
    Left,
    #[df_token(token_name = "RIGHT")]
    Right,
    #[df_token(token_name = "AMBIDEXTROUS")]
    Ambidextrous,
    #[df_token(token_name = "NO_HANDS")]
    NoHands,
}

impl Default for HandednessEnum {
    fn default() -> Self {
        Self::Right
    }
}

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable)]
pub struct Type4Token {
    /// Argument 1
    #[df_token(token_name = "TYPE4", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<(String, ReferenceTo<Self>, String, String)>,
    /// Token `NAME`
    #[df_token(token_name = "NAME")]
    pub name: Option<String>,
}
