use crate::common::{self, opt_lit_to_string};
use quote::{quote, quote_spanned};
use syn::spanned::Spanned;

#[derive(Clone, Debug, Default)]
/// Combined info/metadata about the token.
pub struct DfTokenInfo {
    pub token: DfGeneralTokenInfo,
    pub aliases: Vec<DfAliasInfo>,
    pub added: Option<DfAddedInfo>,
    pub deprecated: Option<DfDeprecatedInfo>,
    pub issues: Vec<DfIssueInfo>,
}

#[derive(Clone, Debug, Default)]
/// General info about the token.
pub struct DfGeneralTokenInfo {
    /// The main token name used for this token.
    pub token_name: String,
    /// If the main token (or alias) is found again it will
    /// go back up the stack and try to create a new item.
    pub on_duplicate_to_parent: bool,
    /// If the main token (or alias) is found again it will
    /// give an error instead of going back up the stack.
    pub on_duplicate_error: bool,
    /// Marks the token as the primary token in the structure.
    /// The primary token is usually 1 level higher in the nesting structure.
    pub primary_token: bool,
}

#[derive(Clone, Debug, Default)]
/// Token alias info
pub struct DfAliasInfo {
    /// The token name of the alias.
    pub token_name: String,
    /// If the use of the alias is discouraged it will add a warning.
    pub discouraged: bool,
    /// Note with additional info, this will be added to diagnostic message.
    pub note: Option<String>,
}

#[derive(Clone, Debug, Default)]
/// Info about when the token was added to DF.
pub struct DfAddedInfo {
    /// Version number of first version where token as added (even if broken).
    /// Format should be "x.xx.xx" leading zeros should be added where needed.
    pub since: String,
    /// Note with additional info, this will be added to diagnostic message.
    pub note: Option<String>,
}

#[derive(Clone, Debug, Default)]
/// Info about when the token was deprecated/removed in DF.
pub struct DfDeprecatedInfo {
    /// Version number of first version where token as deprecated/removed.
    /// Format should be "x.xx.xx" leading zeros should be added where needed.
    pub since: String,
    /// Note with additional info, this will be added to diagnostic message.
    pub note: Option<String>,
}

#[derive(Clone, Debug, Default)]
/// Info about issue related to the token.
pub struct DfIssueInfo {
    /// Version number of first version where the issue was introduced.
    /// Format should be "x.xx.xx" leading zeros should be added where needed.
    pub since: Option<String>,
    /// Version number of first version where the issue was fixed.
    /// Format should be "x.xx.xx" leading zeros should be added where needed.
    pub fixed_in: Option<String>,
    /// Link to issue tracker or other resource with info.
    /// Link should be start with `https://`.
    pub link: Option<String>,
    /// This will change the warning to an error severity.
    pub severity: SeverityIssue,
    /// Note with additional info, this will be added to diagnostic message.
    pub note: Option<String>,
}

#[derive(Clone, Debug)]
pub enum SeverityIssue {
    Error,
    Warn,
    Info,
    Hint,
}

impl Default for SeverityIssue {
    fn default() -> Self {
        Self::Warn
    }
}

pub fn get_token_de_info(field: &syn::Field) -> (DfTokenInfo, proc_macro2::TokenStream) {
    let mut token_info = DfTokenInfo::default();
    let mut errors = quote! {};
    // Loop over all the line of attributes (`#[...]`).
    for (_i, attr) in field.attrs.iter().enumerate() {
        // Add all group sections
        // Add `#[df_token(...)]`
        if let Some((info, error)) = get_general_token_info(attr) {
            // Add errors
            errors = quote! {
                #errors
                #error
            };
            if let Some(info) = info {
                token_info.token = info;
            }
        }
        // Add `#[df_alias(...)]`
        if let Some((info, error)) = get_alias_info(attr) {
            // Add errors
            errors = quote! {
                #errors
                #error
            };
            if let Some(info) = info {
                token_info.aliases.push(info);
            }
        }
        // Add `#[df_added(...)]`
        if let Some((info, error)) = get_added_info(attr) {
            // Add errors
            errors = quote! {
                #errors
                #error
            };
            if let Some(info) = info {
                token_info.added = Some(info);
            }
        }
        // Add `#[df_deprecated(...)]`
        if let Some((info, error)) = get_deprecated_info(attr) {
            // Add errors
            errors = quote! {
                #errors
                #error
            };
            if let Some(info) = info {
                token_info.deprecated = Some(info);
            }
        }
        // Add `#[df_issue(...)]`
        if let Some((info, error)) = get_issue_info(attr) {
            // Add errors
            errors = quote! {
                #errors
                #error
            };
            if let Some(info) = info {
                token_info.issues.push(info);
            }
        }
    }
    if token_info.token.token_name.is_empty() {
        let error = quote_spanned! {
            field.span() =>
            compile_error!(
                "`#[df_token(token_name = \"...\")]` was missing or empty."
            );
        };
        errors = quote! {
            #errors
            #error
        };
    }
    (token_info, errors)
}

// Parse `#[df_token(...)]` to `DfGeneralTokenInfo`
fn get_general_token_info(
    attr: &syn::Attribute,
) -> Option<(Option<DfGeneralTokenInfo>, proc_macro2::TokenStream)> {
    let mut token_name: Option<String> = None;
    let mut on_duplicate_to_parent = false;
    let mut on_duplicate_error = false;
    let mut primary_token = false;

    let meta_items = common::get_meta_items(attr, "df_token").unwrap();
    // If an different argument was found, skip this.
    for meta_item in meta_items? {
        match meta_item {
            // Parse `#[df_token(token_name = "FOO")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("token_name") => {
                token_name = opt_lit_to_string(Some(m.lit));
            }
            // Parse `#[df_token(on_duplicate_to_parent)]`
            syn::NestedMeta::Meta(syn::Meta::Path(m)) if m.is_ident("on_duplicate_to_parent") => {
                on_duplicate_to_parent = true;
            }
            // Parse `#[df_token(on_duplicate_error)]`
            syn::NestedMeta::Meta(syn::Meta::Path(m)) if m.is_ident("on_duplicate_error") => {
                on_duplicate_error = true;
            }
            // Parse `#[df_token(primary_token)]`
            syn::NestedMeta::Meta(syn::Meta::Path(m)) if m.is_ident("primary_token") => {
                primary_token = true;
            }
            _ => (),
        };
    }
    let mut errors = quote! {};
    if token_name.is_none() {
        let error = quote_spanned! {
            attr.span() =>
            compile_error!(
                "`token_name` was missing or empty."
            );
        };
        errors = quote! {
            #errors
            #error
        };
        return Some((None, errors));
    }
    Some((
        Some(DfGeneralTokenInfo {
            token_name: token_name.unwrap(),
            on_duplicate_to_parent,
            on_duplicate_error,
            primary_token,
        }),
        errors,
    ))
}

// Parse `#[df_alias(...)]` to `DfAliasInfo`
fn get_alias_info(
    attr: &syn::Attribute,
) -> Option<(Option<DfAliasInfo>, proc_macro2::TokenStream)> {
    let mut token_name: Option<String> = None;
    let mut discouraged = false;
    let mut note: Option<String> = None;

    let meta_items = common::get_meta_items(attr, "df_alias").unwrap();
    // If an different argument was found, skip this.
    for meta_item in meta_items? {
        match meta_item {
            // Parse `#[df_alias(token_name = "FOO")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("token_name") => {
                token_name = opt_lit_to_string(Some(m.lit));
            }
            // Parse `#[df_alias(discouraged)]`
            syn::NestedMeta::Meta(syn::Meta::Path(m)) if m.is_ident("discouraged") => {
                discouraged = true;
            }
            // Parse `#[df_alias(note = "...")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("note") => {
                note = opt_lit_to_string(Some(m.lit));
            }
            _ => (),
        };
    }
    let mut errors = quote! {};
    if token_name.is_none() {
        let error = quote_spanned! {
            attr.span() =>
            compile_error!(
                "`token_name` was missing or empty."
            );
        };
        errors = quote! {
            #errors
            #error
        };
        return Some((None, errors));
    }
    Some((
        Some(DfAliasInfo {
            token_name: token_name.unwrap(),
            discouraged,
            note,
        }),
        errors,
    ))
}

// Parse `#[df_added(...)]` to `DfAddedInfo`
fn get_added_info(
    attr: &syn::Attribute,
) -> Option<(Option<DfAddedInfo>, proc_macro2::TokenStream)> {
    let mut since: Option<String> = None;
    let mut note: Option<String> = None;

    let meta_items = common::get_meta_items(attr, "df_added").unwrap();
    // If an different argument was found, skip this.
    for meta_item in meta_items? {
        match meta_item {
            // Parse `#[df_added(since = "0.51.0")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("since") => {
                since = opt_lit_to_string(Some(m.lit));
            }
            // Parse `#[df_added(note = "...")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("note") => {
                note = opt_lit_to_string(Some(m.lit));
            }
            _ => (),
        };
    }
    let mut errors = quote! {};
    if since.is_none() {
        let error = quote_spanned! {
            attr.span() =>
            compile_error!(
                "`since` was missing or empty."
            );
        };
        errors = quote! {
            #errors
            #error
        };
        return Some((None, errors));
    }
    Some((
        Some(DfAddedInfo {
            since: since.unwrap(),
            note,
        }),
        errors,
    ))
}

// Parse `#[df_deprecated(...)]` to `DfDeprecatedInfo`
fn get_deprecated_info(
    attr: &syn::Attribute,
) -> Option<(Option<DfDeprecatedInfo>, proc_macro2::TokenStream)> {
    let mut since: Option<String> = None;
    let mut note: Option<String> = None;

    let meta_items = common::get_meta_items(attr, "df_deprecated").unwrap();
    // If an different argument was found, skip this.
    for meta_item in meta_items? {
        match meta_item {
            // Parse `#[df_deprecated(since = "0.51.0")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("since") => {
                since = opt_lit_to_string(Some(m.lit));
            }
            // Parse `#[df_deprecated(note = "...")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("note") => {
                note = opt_lit_to_string(Some(m.lit));
            }
            _ => (),
        };
    }
    let mut errors = quote! {};
    if since.is_none() {
        let error = quote_spanned! {
            attr.span() =>
            compile_error!(
                "`since` was missing or empty."
            );
        };
        errors = quote! {
            #errors
            #error
        };
        return Some((None, errors));
    }
    Some((
        Some(DfDeprecatedInfo {
            since: since.unwrap(),
            note,
        }),
        errors,
    ))
}

// Parse `#[df_issue(...)]` to `DfIssueInfo`
fn get_issue_info(
    attr: &syn::Attribute,
) -> Option<(Option<DfIssueInfo>, proc_macro2::TokenStream)> {
    let mut since: Option<String> = None;
    let mut fixed_in: Option<String> = None;
    let mut link: Option<String> = None;
    let mut severity: Option<SeverityIssue> = None;
    let mut note: Option<String> = None;

    let meta_items = common::get_meta_items(attr, "df_issue").unwrap();
    let mut errors = quote! {};
    // If an different argument was found, skip this.
    for meta_item in meta_items? {
        match meta_item {
            // Parse `#[df_issue(since = "0.51.0")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("since") => {
                since = opt_lit_to_string(Some(m.lit));
            }
            // Parse `#[df_issue(fixed_in = "0.51.0")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("fixed_in") => {
                fixed_in = opt_lit_to_string(Some(m.lit));
            }
            // Parse `#[df_issue(link = "https://")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("link") => {
                let link_text = opt_lit_to_string(Some(m.lit));
                if let Some(link_text) = &link_text {
                    if !link_text.starts_with("http://") && !link_text.starts_with("https://") {
                        let error = quote_spanned! {
                            attr.span() =>
                            compile_error!(
                                "`link` does not start with `http(s)://`."
                            );
                        };
                        errors = quote! {
                            #errors
                            #error
                        };
                    }
                } else {
                    let error = quote_spanned! {
                        attr.span() =>
                        compile_error!(
                            "`link` can not be empty."
                        );
                    };
                    errors = quote! {
                        #errors
                        #error
                    };
                }
                link = link_text;
            }
            // Parse `#[df_issue(severity = "ERROR")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("severity") => {
                let severity_text = opt_lit_to_string(Some(m.lit));
                if let Some(severity_text) = severity_text {
                    severity = match severity_text.as_ref() {
                        "ERROR" => Some(SeverityIssue::Error),
                        "WARN" => Some(SeverityIssue::Warn),
                        "INFO" => Some(SeverityIssue::Info),
                        "HINT" => Some(SeverityIssue::Hint),
                        _ => {
                            let error = quote_spanned! {
                                attr.span() =>
                                compile_error!(
                                    "`severity` must be one of `ERROR`, `WARN`, `INFO`, `HINT`."
                                );
                            };
                            errors = quote! {
                                #errors
                                #error
                            };
                            None
                        }
                    }
                } else {
                    let error = quote_spanned! {
                        attr.span() =>
                        compile_error!(
                            "`severity` can not be empty."
                        );
                    };
                    errors = quote! {
                        #errors
                        #error
                    };
                }
            }
            // Parse `#[df_issue(note = "...")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("note") => {
                note = opt_lit_to_string(Some(m.lit));
            }
            _ => (),
        };
    }
    Some((
        Some(DfIssueInfo {
            since,
            fixed_in,
            link,
            severity: severity.unwrap_or_default(),
            note,
        }),
        errors,
    ))
}
