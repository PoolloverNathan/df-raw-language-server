use serde::{Deserialize, Serialize};
use std::fmt::Display;

#[allow(clippy::upper_case_acronyms)]
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[non_exhaustive]
pub enum TokenArgument {
    TVInteger(i64),
    TVCharacter(char),
    TVArgN(u8),
    TVBangArgN(u16),
    TVReference(String),
    TVString(String),
    TVPipeArguments(Vec<Self>),
    TVBangArgNSequence(Vec<Self>),
    TVEmpty,
}

impl TokenArgument {
    pub fn argument_to_token_type_name(&self) -> String {
        match self {
            TokenArgument::TVInteger(_) => "Integer".to_string(),
            TokenArgument::TVCharacter(_) => "char".to_string(),
            TokenArgument::TVArgN(_) => "ArgN".to_string(),
            TokenArgument::TVBangArgN(_) => "BangArgN".to_string(),
            TokenArgument::TVReference(_) => "Reference".to_string(),
            TokenArgument::TVString(_) => "String".to_string(),
            TokenArgument::TVPipeArguments(_) => "PipeArguments".to_string(),
            TokenArgument::TVBangArgNSequence(_) => "BangArgNSequence".to_string(),
            TokenArgument::TVEmpty => "Empty".to_string(),
        }
    }
}

impl Default for TokenArgument {
    fn default() -> Self {
        TokenArgument::TVEmpty
    }
}

impl Display for TokenArgument {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "`{}`",
            match self {
                TokenArgument::TVInteger(v) => v.to_string(),
                TokenArgument::TVCharacter(v) => v.to_string(),
                TokenArgument::TVArgN(v) => v.to_string(),
                TokenArgument::TVBangArgN(v) => v.to_string(),
                TokenArgument::TVReference(v) => v.to_owned(),
                TokenArgument::TVString(v) => v.to_owned(),
                TokenArgument::TVPipeArguments(v) => {
                    v.iter()
                        .map(|v| v.to_string())
                        .collect::<Vec<_>>()
                        .join(",")
                }
                TokenArgument::TVBangArgNSequence(v) => {
                    v.iter()
                        .map(|v| v.to_string())
                        .collect::<Vec<_>>()
                        .join(",")
                }
                TokenArgument::TVEmpty => "".to_owned(),
            }
        )
    }
}
