use super::super::{TokenArgument, TokenDeserializeBasics};
use super::{LoopControl, TokenDeserialize};
use df_ls_core::DfLsConfig;
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_lexical_analysis::TreeCursor;

impl TokenDeserialize for TokenArgument {
    fn deserialize_tokens(
        cursor: &mut TreeCursor,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        config: &DfLsConfig,
    ) -> Result<Box<Self>, ()> {
        let mut token_argument = TokenArgument::default();
        let node = cursor.node();
        match node.kind().as_ref() {
            "token_argument_integer" => {
                let value = TokenDeserializeBasics::deserialize_tokens(cursor, source, diagnostics);
                if let Ok(value) = value {
                    token_argument = TokenArgument::TVInteger(value);
                }
                // Else message should be already added to diagnostics
            }
            "token_argument_character" => {
                token_argument = TokenArgument::TVCharacter(
                    TokenDeserializeBasics::deserialize_tokens(cursor, source, diagnostics)?,
                );
            }
            "token_argument_arg_n" => {
                token_argument = TokenArgument::TVArgN(TokenDeserializeBasics::deserialize_tokens(
                    cursor,
                    source,
                    diagnostics,
                )?);
            }
            "token_argument_reference" => {
                token_argument = TokenArgument::TVReference(
                    TokenDeserializeBasics::deserialize_tokens(cursor, source, diagnostics)?,
                );
            }
            "token_argument_string" => {
                token_argument = TokenArgument::TVString(
                    TokenDeserializeBasics::deserialize_tokens(cursor, source, diagnostics)?,
                );
            }
            "token_argument_pipe_arguments" => {
                match super::td_token::deserialize_token_arguments(
                    cursor,
                    source,
                    diagnostics,
                    config,
                ) {
                    Ok(arguments) => {
                        // `Vec<Argument>` to `Vec<TokenArgument>`
                        let token_arguments = arguments.into_iter().map(|arg| arg.value).collect();
                        token_argument = TokenArgument::TVPipeArguments(token_arguments);
                    }
                    Err(err) => {
                        return Err(err);
                    }
                }
            }
            "token_argument_empty" => {
                token_argument = TokenArgument::TVEmpty;
            }
            "token_argument_bang_arg_n" => {
                token_argument = TokenArgument::TVBangArgN(
                    TokenDeserializeBasics::deserialize_tokens(cursor, source, diagnostics)?,
                );
            }
            "token_argument_bang_arg_n_sequence" => {
                match super::td_token::deserialize_token_arguments(
                    cursor,
                    source,
                    diagnostics,
                    config,
                ) {
                    Ok(arguments) => {
                        // `Vec<Argument>` to `Vec<TokenArgument>`
                        let token_arguments = arguments.into_iter().map(|arg| arg.value).collect();
                        token_argument = TokenArgument::TVBangArgNSequence(token_arguments);
                    }
                    Err(err) => {
                        return Err(err);
                    }
                }
            }
            "EOF" => {}
            others => {
                log::error!("Found an unknown node of kind: `{}`", others);
                debug_assert!(false, "Found an unknown node of kind: `{}`", others);
            }
        }
        Ok(Box::new(token_argument))
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        _config: &DfLsConfig,
        new_self: Box<Self>,
    ) -> (LoopControl, Box<Self>) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_allowed_tokens(_config: &DfLsConfig) -> Option<Vec<String>> {
        None
    }
}
