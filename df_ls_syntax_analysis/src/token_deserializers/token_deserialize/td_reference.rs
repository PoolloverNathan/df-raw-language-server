use super::super::{Argument, Token, TokenArgument, TryFromArgument, TryFromArgumentGroup};
use super::TokenDeserialize;
use df_ls_core::Reference;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};

// Deserialize a token with following pattern: `[REF:REF]`
crate::token_deserialize_unary_token!(Reference);

// ------------------------- Convert a group of arguments to Self -----------------------

impl TryFromArgumentGroup for Reference {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl TryFromArgument for Reference {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match &arg.value {
                TokenArgument::TVReference(v) => Ok(Reference(v.clone())),
                TokenArgument::TVString(v) => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo::new(arg.node.get_range()),
                            "reference_is_string",
                        );
                    }
                    Ok(Reference(v.clone()))
                }
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => arg.value.argument_to_token_type_name(),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        "Reference".to_owned()
    }
}

// -------------------------Convert from TokenArgument -----------------------

impl From<Reference> for TokenArgument {
    fn from(item: Reference) -> TokenArgument {
        TokenArgument::TVReference(item.0)
    }
}

impl From<Option<Reference>> for TokenArgument {
    fn from(item: Option<Reference>) -> TokenArgument {
        match item {
            Some(v) => TokenArgument::TVReference(v.0),
            None => TokenArgument::TVEmpty,
        }
    }
}
