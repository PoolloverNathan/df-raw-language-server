use super::super::{Argument, Token, TokenArgument, TryFromArgument, TryFromArgumentGroup};
use super::TokenDeserialize;
use df_ls_core::{ArgN, Clamp};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use std::convert::TryFrom;

// Deserialize a token with following pattern: `[REF:ARG2]` or `[REF:ARG10]`
crate::token_deserialize_unary_token!(ArgN);

// ------------------------- Convert a group of arguments to Self -----------------------

impl TryFromArgumentGroup for ArgN {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl TryFromArgument for ArgN {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match arg.value {
                TokenArgument::TVArgN(v) => Ok(ArgN(v)),
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => arg.value.argument_to_token_type_name(),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        "ArgN".to_owned()
    }
}

// -------------------------Convert from TokenArgument -----------------------

impl From<ArgN> for TokenArgument {
    fn from(item: ArgN) -> TokenArgument {
        TokenArgument::TVArgN(item.0)
    }
}

impl From<Option<ArgN>> for TokenArgument {
    fn from(item: Option<ArgN>) -> TokenArgument {
        match item {
            Some(v) => TokenArgument::TVArgN(v.0),
            None => TokenArgument::TVEmpty,
        }
    }
}

// -------------------------Implement Clamp -----------------------

impl<const L: isize, const H: isize> TryFromArgument for Clamp<ArgN, L, H> {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // In debug mode warn user about when clamp exceeds allowable value range.
        #[cfg(debug_assertions)]
        {
            if (H as i64) > (<ArgN>::MAX as i64) {
                panic!(
                    "Clamp exceeds range of type. High: {} > {}::MAX = {} \
                    for Clamp<{}, {}, {}>",
                    H,
                    "ArgN",
                    <ArgN>::MAX,
                    "ArgN",
                    L,
                    H
                );
            }
            if (L as i64) < (<ArgN>::MIN as i64) {
                panic!(
                    "Clamp exceeds range of type. Low: {} < {}::MIN = {} \
                    for Clamp<{}, {}, {}>",
                    L,
                    "ArgN",
                    <ArgN>::MIN,
                    "ArgN",
                    L,
                    H
                );
            }
        }
        if let Some(arg) = arg_opt {
            match arg.value {
                TokenArgument::TVArgN(value_u8) => {
                    // Make sure we do not go over the type limits.
                    let low = std::cmp::max(L as u8, <ArgN>::MIN as u8);
                    let high = std::cmp::min(H as u8, <ArgN>::MAX as u8);

                    if value_u8 > high {
                        if add_diagnostics_on_err {
                            diagnostics.add_message(
                                DMExtraInfo {
                                    range: arg.node.get_range(),
                                    message_template_data: hash_map! {
                                        "max_value" => high.to_string(),
                                    },
                                },
                                "too_large_int",
                            );
                        }
                        return Err(());
                    }
                    if value_u8 < low {
                        if add_diagnostics_on_err {
                            diagnostics.add_message(
                                DMExtraInfo {
                                    range: arg.node.get_range(),
                                    message_template_data: hash_map! {
                                        "min_value" => low.to_string(),
                                    },
                                },
                                "too_small_int",
                            );
                        }
                        return Err(());
                    }
                    Ok(Self::from(match <ArgN>::try_from(value_u8) {
                        Ok(v) => v,
                        Err(err) => {
                            log::error!("{}", err);
                            panic!("Number conversion failed, `{}` to `{}`", value_u8, "ArgN");
                        }
                    }))
                }
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => arg.value.argument_to_token_type_name(),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        let low = std::cmp::max(L as i64, <ArgN>::MIN as i64);
        let high = std::cmp::min(H as i64, <ArgN>::MAX as i64);
        format!(
            "`ARGx` ({}, limited to min: {}, max: {})",
            "ArgN", low, high
        )
    }
}

impl<const L: isize, const H: isize> From<Clamp<ArgN, L, H>> for TokenArgument {
    fn from(item: Clamp<ArgN, L, H>) -> TokenArgument {
        TokenArgument::TVArgN(item.value.0)
    }
}

impl<const L: isize, const H: isize> From<Option<Clamp<ArgN, L, H>>> for TokenArgument {
    fn from(item: Option<Clamp<ArgN, L, H>>) -> TokenArgument {
        match item {
            Some(v) => TokenArgument::TVArgN(v.value.0),
            None => TokenArgument::TVEmpty,
        }
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_tree_structure;
    use crate::test_utils::SyntaxTestBuilder;
    use df_ls_diagnostics::{Position, Range};
    use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_arg_n_correct() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [REF:ARG0]
                [REF:ARG2]
                [REF:ARG5]
                [REF:ARG9]
                [REF:ARG10]
                [REF:ARG200]",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec![])
        .add_test_syntax_diagnostics_ranges(vec![]);

        test_tree_structure!(
            test_builder,
            [
                ArgN => ArgN(0),
                (ArgN,) => (ArgN(2),),
                ArgN => ArgN(5),
                ArgN => ArgN(9),
                ArgN => ArgN(10),
                ArgN => ArgN(200),
            ]
        );
    }

    #[test]
    fn test_u8_overflow() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [REF:ARG5000]
                [REF:ARG256]",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec!["expected_integer", "expected_integer"])
        .add_test_syntax_diagnostics_ranges(vec![
            Range {
                start: Position {
                    line: 1,
                    character: 21,
                },
                end: Position {
                    line: 1,
                    character: 28,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 21,
                },
                end: Position {
                    line: 2,
                    character: 27,
                },
            },
        ]);

        test_tree_structure!(
            test_builder,
            [
                Option<ArgN> =!,
                Option<ArgN> =!,
            ]
        );
    }

    #[test]
    fn test_arg_n_leading_zeros() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [REF:ARG05]
                [REF:ARG007]",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec!["wrong_arg_type", "wrong_arg_type"])
        .add_test_syntax_diagnostics_ranges(vec![
            Range {
                start: Position {
                    line: 1,
                    character: 21,
                },
                end: Position {
                    line: 1,
                    character: 26,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 21,
                },
                end: Position {
                    line: 2,
                    character: 27,
                },
            },
        ]);

        test_tree_structure!(
            test_builder,
            [
                Option<ArgN> =!,
                Option<ArgN> =!,
            ]
        );
    }
}
