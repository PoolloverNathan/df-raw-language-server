use super::super::TokenDeserializeBasics;
use super::{LoopControl, Token, TokenDeserialize};
use df_ls_core::{DFRawFile, DfLsConfig};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_lexical_analysis::TreeCursor;

/// Deserialize a DF Raw File.
/// A DF Raw File starts with a header followed by one or more tokens.
impl<T: Default + TokenDeserialize> TokenDeserialize for DFRawFile<T> {
    fn deserialize_tokens(
        cursor: &mut TreeCursor,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        config: &DfLsConfig,
    ) -> Result<Box<Self>, ()> {
        let mut df_raw = Box::new(Self::default());
        // Keep track of if the first object token was deserialized.
        let mut first_object_deserialized = false;
        loop {
            let node = cursor.node();
            // If we reach the limit, stop parsing the file further.
            if diagnostics.check_message_limit_reached() {
                crate::mark_rest_of_file_as_unchecked(cursor, diagnostics, &node);
                break;
            }
            match node.kind().as_ref() {
                "header" => {
                    if let Ok(value) =
                        TokenDeserializeBasics::deserialize_tokens(cursor, source, diagnostics)
                    {
                        df_raw.header = value
                    }
                    if Token::consume_token(cursor).is_err() {
                        break;
                    }
                }
                "token" => {
                    // if first token was already correctly deserialized,
                    // we are on the second `[OBJECT:..]` token or some other token
                    if first_object_deserialized {
                        // Mark current node as `unknown_token`
                        if let Ok(token) =
                            Token::deserialize_tokens(cursor, source, diagnostics, config)
                        {
                            let token_name = token.get_token_name().unwrap().value.to_owned();
                            diagnostics.add_message(
                                DMExtraInfo {
                                    range: node.get_range(),
                                    message_template_data: hash_map! {
                                        "token_name" => format!("`{}`", token_name),
                                    },
                                },
                                "unknown_token",
                            );
                        }
                        crate::mark_rest_of_file_as_unchecked(cursor, diagnostics, &node);
                        break;
                    }
                    if let Ok(value) =
                        TokenDeserialize::deserialize_tokens(cursor, source, diagnostics, config)
                    {
                        df_raw.token_structure = *value;
                        first_object_deserialized = true;
                        // Only go to next sibling if there is one, if none: break.
                        // We have reached the end of the file, so have to go up the stack
                        let new_node = cursor.node();
                        if new_node.next_sibling().is_none() {
                            cursor.goto_parent();
                            break;
                        }
                        // If node did not change: break
                        // This will prevent infinite loops
                        if new_node == node {
                            break;
                        }
                        continue;
                    } else {
                        return Err(());
                    }
                }
                "comment" => {
                    // Just consume `comment` and move on to next token.
                    if Token::consume_token(cursor).is_err() {
                        break;
                    }
                }
                "ERROR" => {
                    // Can safely be ignored. Diagnostics already added by Lexical Analysis.
                    if Token::consume_token(cursor).is_err() {
                        break;
                    }
                }
                "EOF" => break,
                others => {
                    log::error!("Found an unknown node of kind: {}", others);
                    break;
                }
            }
            // If node did not change: break
            // This will prevent infinite loops
            let new_node = cursor.node();
            if new_node == node {
                break;
            }
        }
        Ok(df_raw)
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        _config: &DfLsConfig,
        new_self: Box<Self>,
    ) -> (LoopControl, Box<Self>) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_allowed_tokens(_config: &DfLsConfig) -> Option<Vec<String>> {
        None
    }
}
