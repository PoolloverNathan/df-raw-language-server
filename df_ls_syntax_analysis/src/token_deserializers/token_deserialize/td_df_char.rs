use super::super::{Argument, Token, TokenArgument, TryFromArgument, TryFromArgumentGroup};
use super::TokenDeserialize;
use df_ls_core::DFChar;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};

// Deserialize a token with following pattern: `[REF:char/u8]` or `[REF:'c'/125]`
crate::token_deserialize_unary_token!(DFChar);

// ------------------------- Convert a group of arguments to Self -----------------------

impl TryFromArgumentGroup for DFChar {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl TryFromArgument for DFChar {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match arg.value {
                TokenArgument::TVCharacter(v) => Ok(DFChar(v)),
                TokenArgument::TVInteger(value_i64) => {
                    if value_i64 > (u8::MAX as i64) {
                        if add_diagnostics_on_err {
                            diagnostics.add_message(
                                DMExtraInfo {
                                    range: arg.node.get_range(),
                                    message_template_data: hash_map! {
                                        "max_value" => u8::MAX.to_string(),
                                    },
                                },
                                "too_large_int",
                            );
                        }
                        return Err(());
                    }
                    if value_i64 < (u8::MIN as i64) {
                        if add_diagnostics_on_err {
                            diagnostics.add_message(
                                DMExtraInfo {
                                    range: arg.node.get_range(),
                                    message_template_data: hash_map! {
                                        "min_value" => u8::MIN.to_string(),
                                    },
                                },
                                "too_small_int",
                            );
                        }
                        return Err(());
                    }
                    let value_u8: u8 = value_i64 as u8;
                    match df_cp437::convert_cp437_to_utf8_char(value_u8) {
                        Ok(value) => Ok(DFChar(value)),
                        Err(err) => {
                            log::error!("DFChar error: {}", err);
                            Err(())
                        }
                    }
                }
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => arg.value.argument_to_token_type_name(),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        "Integer (u8) or char".to_owned()
    }
}

// -------------------------Convert from TokenArgument -----------------------

impl From<DFChar> for TokenArgument {
    fn from(item: DFChar) -> TokenArgument {
        TokenArgument::TVCharacter(item.0)
    }
}

impl From<Option<DFChar>> for TokenArgument {
    fn from(item: Option<DFChar>) -> TokenArgument {
        match item {
            Some(v) => TokenArgument::TVCharacter(v.0),
            None => TokenArgument::TVEmpty,
        }
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_tree_structure;
    use crate::test_utils::SyntaxTestBuilder;
    use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

    #[test]
    fn test_df_char_correct() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [REF:'c']
                [REF:''']
                [REF:'\"']
                [REF:'0']
                [REF:'P']
                [REF:'#']
                [REF:'*']
                [REF:'`']",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec![])
        .add_test_syntax_diagnostics_ranges(vec![]);

        test_tree_structure!(
            test_builder,
            [
                DFChar => DFChar('c'),
                (DFChar,) => (DFChar('\''),),
                DFChar => DFChar('\"'),
                DFChar => DFChar('0'),
                DFChar => DFChar('P'),
                DFChar => DFChar('#'),
                DFChar => DFChar('*'),
                DFChar => DFChar('`'),
            ]
        );
    }

    #[test]
    fn test_df_char_number_correct() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [REF:99] 'c'
                [REF:39] '''
                [REF:34] '\"'
                [REF:48] '0'
                [REF:80] 'P'
                [REF:35] '#'
                [REF:42] '*'
                [REF:96] '`'
                [REF:184] '╕'
                [REF:219] '█'
                [REF:231] 'τ'
                [REF:1] '☺'
                [REF:12] '♀'
                ",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec![])
        .add_test_syntax_diagnostics_ranges(vec![]);

        test_tree_structure!(
            test_builder,
            [
                DFChar => DFChar('c'),
                (DFChar,) => (DFChar('\''),),
                DFChar => DFChar('\"'),
                DFChar => DFChar('0'),
                DFChar => DFChar('P'),
                DFChar => DFChar('#'),
                DFChar => DFChar('*'),
                DFChar => DFChar('`'),
                DFChar => DFChar('╕'),
                DFChar => DFChar('█'),
                DFChar => DFChar('τ'),
                DFChar => DFChar('☺'),
                DFChar => DFChar('♀'),
            ]
        );
    }
}
