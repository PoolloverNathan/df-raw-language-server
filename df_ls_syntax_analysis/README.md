# DF RAW Syntax

This folder include the grammar/syntax of the language.
This is used to evaluate files on the language server.

This code is mainly auto-generated and included in the binary.

The `./grammar/grammar.js` has contains all the grammar.
