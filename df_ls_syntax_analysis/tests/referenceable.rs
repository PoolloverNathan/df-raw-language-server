#![allow(clippy::needless_update)]

use df_ls_core::ReferenceTo;
use df_ls_debug_structure::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_reference_to_self_tuple() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE4]

            [TYPE4:string:REF:String:More]
                [NAME:some name]
            [TYPE4:string:REF2:String:More]
                [NAME:some name]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_4: vec![
                Type4Token {
                    reference: Some((
                        "string".to_owned(),
                        ReferenceTo::new("REF".to_owned()),
                        "String".to_owned(),
                        "More".to_owned(),
                    )),
                    name: Some("some name".to_owned()),
                    ..Default::default()
                },
                Type4Token {
                    reference: Some((
                        "string".to_owned(),
                        ReferenceTo::new("REF2".to_owned()),
                        "String".to_owned(),
                        "More".to_owned(),
                    )),
                    name: Some("some name".to_owned()),
                    ..Default::default()
                },
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
