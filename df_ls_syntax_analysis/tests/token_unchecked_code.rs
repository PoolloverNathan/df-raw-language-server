use df_ls_core::{ArgN, Choose, Reference};
use df_ls_debug_structure::*;
use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_token_unchecked_code_enum_value() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:DEBUG]

            [ENUM_VALUE_VEC:ENUM1:ENUM2:ENUM3:ENUM4]
            [ENUM_VALUE_VEC:ENUM1:ENUM_:ENUM3:ENUM4]
            [ENUM_VALUE_VEC:ENUM1:ENUM2:ENUM3:ENUM_]
            [ENUM_VALUE_VEC:ENUM1:ENUM2:ENUM3:ENUM4:_]
            [ENUM_VALUE_VEC:ENUM1:ENUM2:ENUM3:ENUM4:]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            debug: vec![DebugTokens {
                enum_value: vec![(vec![
                    DebugEnum::Enum1,
                    DebugEnum::Enum2,
                    DebugEnum::Enum3,
                    DebugEnum::Enum4,
                ],)],
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![
        "wrong_enum_value",
        "unchecked_code",
        "wrong_enum_value",
        "wrong_arg_type",
        "wrong_arg_type",
    ])
    .add_test_syntax_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 4,
                character: 34,
            },
            end: Position {
                line: 4,
                character: 39,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 39,
            },
            end: Position {
                line: 4,
                character: 51,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 46,
            },
            end: Position {
                line: 5,
                character: 51,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 52,
            },
            end: Position {
                line: 6,
                character: 53,
            },
        },
        Range {
            start: Position {
                line: 7,
                character: 52,
            },
            end: Position {
                line: 7,
                character: 52,
            },
        },
    ])
    .run_test();
}

#[test]
fn test_token_unchecked_code_unary_token() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:DEBUG]

            [ARG_N:ARG1]
            [ARG_N:ARG_]
            [ARG_N:ARG_:ARG1]
            [ARG_N:ARG_:ARG_]
            [ARG_N:ARG1:ARG_]

            [UNARY_TOKEN_MULTI_ARG:OPTION1:REF]
            [UNARY_TOKEN_MULTI_ARG:OPTION_:REF]
            [UNARY_TOKEN_MULTI_ARG:OPTION1:333]
            [UNARY_TOKEN_MULTI_ARG:OPTION1:333:str]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            debug: vec![DebugTokens {
                arg_n: vec![ArgN(1)],
                unary_token_multi_arg: vec![DebugUnaryTokenEnum::Option1(Reference(
                    "REF".to_owned(),
                ))],
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![
        "wrong_arg_type",
        "wrong_arg_type",
        "unchecked_code",
        "wrong_arg_type",
        "unchecked_code",
        "wrong_arg_number",
        "wrong_enum_value",
        "unchecked_code",
        "wrong_arg_type",
        "wrong_arg_type",
        "unchecked_code",
    ])
    .add_test_syntax_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 4,
                character: 19,
            },
            end: Position {
                line: 4,
                character: 23,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 19,
            },
            end: Position {
                line: 5,
                character: 23,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 23,
            },
            end: Position {
                line: 5,
                character: 28,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 19,
            },
            end: Position {
                line: 6,
                character: 23,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 23,
            },
            end: Position {
                line: 6,
                character: 28,
            },
        },
        Range {
            start: Position {
                line: 7,
                character: 23,
            },
            end: Position {
                line: 7,
                character: 28,
            },
        },
        Range {
            start: Position {
                line: 10,
                character: 35,
            },
            end: Position {
                line: 10,
                character: 42,
            },
        },
        Range {
            start: Position {
                line: 10,
                character: 42,
            },
            end: Position {
                line: 10,
                character: 46,
            },
        },
        Range {
            start: Position {
                line: 11,
                character: 43,
            },
            end: Position {
                line: 11,
                character: 46,
            },
        },
        Range {
            start: Position {
                line: 12,
                character: 43,
            },
            end: Position {
                line: 12,
                character: 46,
            },
        },
        Range {
            start: Position {
                line: 12,
                character: 46,
            },
            end: Position {
                line: 12,
                character: 50,
            },
        },
    ])
    .run_test();
}

#[test]
fn test_token_unchecked_code_choose() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:DEBUG]

            [CHOOSE:02]
            [CHOOSE:str]
            [CHOOSE:|]
            [CHOOSE:|:test]
            [CHOOSE:|:1:2:3:4]
            [CHOOSE:|:]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            debug: vec![DebugTokens {
                choose: vec![Choose::Choice1(2), Choose::Choice2("str".to_owned())],
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![
        "wrong_arg_type",
        "wrong_arg_type",
        "unchecked_code",
        "wrong_arg_type",
        "unchecked_code",
        "wrong_arg_type",
        "unchecked_code",
    ])
    .add_test_syntax_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 5,
                character: 20,
            },
            end: Position {
                line: 5,
                character: 21,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 20,
            },
            end: Position {
                line: 6,
                character: 21,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 21,
            },
            end: Position {
                line: 6,
                character: 26,
            },
        },
        Range {
            start: Position {
                line: 7,
                character: 20,
            },
            end: Position {
                line: 7,
                character: 21,
            },
        },
        Range {
            start: Position {
                line: 7,
                character: 21,
            },
            end: Position {
                line: 7,
                character: 29,
            },
        },
        Range {
            start: Position {
                line: 8,
                character: 20,
            },
            end: Position {
                line: 8,
                character: 21,
            },
        },
        Range {
            start: Position {
                line: 8,
                character: 21,
            },
            end: Position {
                line: 8,
                character: 22,
            },
        },
    ])
    .run_test();
}

#[test]
fn test_token_unchecked_code_clamp_and_ref_to() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:DEBUG]

            [ARG_N_CLAMP:ARG_:str]
            [ARG_N_CLAMP:ARG_:str:6]
            [REF_TO:6:str]
            [REF_TO:6:9:str]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            debug: vec![DebugTokens {
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![
        "wrong_arg_type",
        "unchecked_code",
        "wrong_arg_type",
        "unchecked_code",
        "wrong_arg_type",
        "unchecked_code",
        "wrong_arg_type",
        "unchecked_code",
    ])
    .add_test_syntax_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 3,
                character: 25,
            },
            end: Position {
                line: 3,
                character: 29,
            },
        },
        Range {
            start: Position {
                line: 3,
                character: 29,
            },
            end: Position {
                line: 3,
                character: 33,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 25,
            },
            end: Position {
                line: 4,
                character: 29,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 29,
            },
            end: Position {
                line: 4,
                character: 35,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 20,
            },
            end: Position {
                line: 5,
                character: 21,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 21,
            },
            end: Position {
                line: 5,
                character: 25,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 20,
            },
            end: Position {
                line: 6,
                character: 21,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 21,
            },
            end: Position {
                line: 6,
                character: 27,
            },
        },
    ])
    .run_test();
}

#[test]
fn test_token_unchecked_code_tuple() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:DEBUG]

            [TUPLE:8:6:str]
            [TUPLE:s:6:str]
            [TUPLE:8:s:str]
            [TUPLE:8:s:str:8:8]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            debug: vec![DebugTokens {
                tuple: vec![(8, 6, "str".to_owned())],
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![
        "wrong_arg_type",
        "unchecked_code",
        "wrong_arg_type",
        "unchecked_code",
        "wrong_arg_type",
        "unchecked_code",
    ])
    .add_test_syntax_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 4,
                character: 19,
            },
            end: Position {
                line: 4,
                character: 20,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 20,
            },
            end: Position {
                line: 4,
                character: 26,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 21,
            },
            end: Position {
                line: 5,
                character: 22,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 22,
            },
            end: Position {
                line: 5,
                character: 26,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 21,
            },
            end: Position {
                line: 6,
                character: 22,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 22,
            },
            end: Position {
                line: 6,
                character: 30,
            },
        },
    ])
    .run_test();
}
