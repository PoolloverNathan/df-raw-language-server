#![allow(clippy::needless_update)]

use df_ls_core::{Reference, ReferenceTo};
use df_ls_debug_structure::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_multi_level_back_up_stack() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE2]

            [BIPEDAL:MONSTER]
                [NAME:test]
                [PROFESSION:SLEEPING]
                    [NAME:SLEEPING]
                    [TOOL:BED]
                        [NAME:bed]
            [BIPEDAL:HUMAN]
                [NAME:also sleepy]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_2: vec![
                Type2Token::Bipedal(HumanToken {
                    reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                    name: Some("test".to_owned()),
                    profession: Some(ProfessionToken {
                        reference: Some(ReferenceTo::new("SLEEPING".to_owned())),
                        name: vec![Reference("SLEEPING".to_owned())],
                        tool: Some(ToolToken {
                            reference: Some(ReferenceTo::new("BED".to_owned())),
                            name: Some("bed".to_owned()),
                        }),
                        ..Default::default()
                    }),
                    ..Default::default()
                }),
                Type2Token::Bipedal(HumanToken {
                    reference: Some(ReferenceTo::new("HUMAN".to_owned())),
                    name: Some("also sleepy".to_owned()),
                    ..Default::default()
                }),
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
