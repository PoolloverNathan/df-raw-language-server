#![allow(clippy::needless_update)]

use df_ls_core::Reference;
use df_ls_debug_structure::*;
use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

/// Testing if no newline at the and will still work
/// This triggered errors on the last token before
#[test]
fn non_newline_token() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE1]
            [TYPE1:DOG]
                [ITEM:T1]",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                list: vec![Reference("T1".to_owned())],
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}

/// Testing if no newline at the and will still work
/// This triggered errors on the last token before
/// This Last token is misspelled and will thus go back up the stack
#[test]
fn non_newline_token_misspelled() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE1]
            [TYPE1:DOG]
                [ITE:T1]",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec!["unknown_token"])
    .add_test_syntax_diagnostics_ranges(vec![Range {
        start: Position {
            line: 3,
            character: 16,
        },
        end: Position {
            line: 3,
            character: 24,
        },
    }])
    .run_test();
}
