#![allow(clippy::needless_update)]

use df_ls_core::{Choose, ReferenceTo};
use df_ls_debug_structure::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_enum_value_derive() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE2]

            [BIPEDAL:MONSTER]
                [NAME:test]
                [GENDER:FEMALE]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                gender: Some(GenderEnum::Female),
                ..Default::default()
            })],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}

/// Regression test for #61
#[test]
fn test_enum_value_in_vec_derive() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:PROFESSION]

            [PROFESSION:MONSTER]
                [GENDER:FEMALE]
                [GENDER:MALE]
                [GENDER:OTHER]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                genders: vec![GenderEnum::Female, GenderEnum::Male, GenderEnum::Other],
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}

/// Check if Enum allowed in `Choose`
#[test]
fn test_enum_value_in_choose_derive() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:PROFESSION]

            [PROFESSION:MONSTER]
                [HAND:LEFT]
            [PROFESSION:RAT]
                [HAND:AMBIDEXTROUS]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            professions: vec![
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                    handedness: Some(Choose::Choice2(HandednessEnum::Left)),
                    ..Default::default()
                },
                ProfessionToken {
                    reference: Some(ReferenceTo::new("RAT".to_owned())),
                    handedness: Some(Choose::Choice2(HandednessEnum::Ambidextrous)),
                    ..Default::default()
                },
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
