#![allow(clippy::needless_update)]

use df_ls_core::ReferenceTo;
use df_ls_debug_structure::*;
use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_option_value() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:PROFESSION]

            [PROFESSION:MONSTER]
                [ARG:string:string]
            [PROFESSION:MONSTER2]
                [ARG:string]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            professions: vec![
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                    argument: Some(("string".to_owned(), Some("string".to_owned()))),
                    ..Default::default()
                },
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER2".to_owned())),
                    argument: Some(("string".to_owned(), None)),
                    ..Default::default()
                },
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}

#[test]
fn test_option_error() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:PROFESSION]

            [PROFESSION:MONSTER]
                [ARG:string:string:string]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                argument: None,
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec!["wrong_arg_number"])
    .add_test_syntax_diagnostics_ranges(vec![Range {
        start: Position {
            line: 4,
            character: 34,
        },
        end: Position {
            line: 4,
            character: 41,
        },
    }])
    .run_test();
}

#[test]
fn test_option_vec_with_error() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:PROFESSION]

            [PROFESSION:MONSTER]
                [ARG_VEC:'a':'a']
                [ARG_VEC:String:String]
                [ARG_VEC:666:666]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                arguments: vec![("String".to_owned(), Some("String".to_owned()))],
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![
        "wrong_arg_type",
        "unchecked_code",
        "wrong_arg_type",
        "unchecked_code",
    ])
    .add_test_syntax_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 4,
                character: 25,
            },
            end: Position {
                line: 4,
                character: 28,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 28,
            },
            end: Position {
                line: 4,
                character: 32,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 25,
            },
            end: Position {
                line: 6,
                character: 28,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 28,
            },
            end: Position {
                line: 6,
                character: 32,
            },
        },
    ])
    .run_test();
}
