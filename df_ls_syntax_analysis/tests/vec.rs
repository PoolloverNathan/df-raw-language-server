#![allow(clippy::needless_update)]

use df_ls_core::Reference;
use df_ls_debug_structure::*;
use df_ls_diagnostics::lsp_types::*;
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
fn test_vec_derive_struct() {
    let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "header
            [NAME:some string]
            [NAME:some other string]
            [NAME:short]",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![]);

    test_tree_structure!(
        test_builder,
        [RefToken => RefToken {
            reference: None,
            name: vec![
                "some string".to_owned(),
                "some other string".to_owned(),
                "short".to_owned(),
            ],
        }]
    );
}

#[test]
fn test_vec_derive_struct_mistypes_ref() {
    let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "header
            [NAME:some string]
            [REF:SOMEREF]
            [NAME:some more strings]
            [NAMEB:some other string]
            [NAME:short]
            [NAME:a bit longer]
            [NAME:also some (2) numbers 5]",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![]);

    test_tree_structure!(
        test_builder,
        [
            RefToken => RefToken {
                reference: Some(Reference("SOMEREF".to_owned())),
                name: vec!["some string".to_owned(), "some more strings".to_owned()],
            },
            // The `[NAMEB:some other string]` has not been deserialized yet
            String => String::from("some other string"),
            RefToken => RefToken {
                reference: None,
                name: vec![
                    "short".to_owned(),
                    "a bit longer".to_owned(),
                    "also some (2) numbers 5".to_owned(),
                ],
            },
        ]
    );
}

#[test]
fn test_vec_derive_struct_with_error() {
    let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "header
            [NAME:some string]
            [NAME:some other string]
            [NAME:short]
            [NAME:'a']
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_syntax_diagnostics_codes(vec!["wrong_arg_type"])
    .add_test_syntax_diagnostics_ranges(vec![Range {
        start: Position {
            line: 4,
            character: 18,
        },
        end: Position {
            line: 4,
            character: 21,
        },
    }]);

    test_tree_structure!(
        test_builder,
        [RefToken => RefToken {
            reference: None,
            name: vec![
                "some string".to_owned(),
                "some other string".to_owned(),
                "short".to_owned()
            ],
        },]
    );
}
