#![allow(clippy::needless_update)]

use df_ls_core::Reference;
use df_ls_debug_structure::*;
use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

/// Regression test for issue #3 and #2
#[test]
fn misspelled_token() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE1]

            [TYPE1:DOG]
                [ITM:T1]
                [ITEM:T1]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec!["unknown_token", "unchecked_code"])
    .add_test_syntax_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 4,
                character: 16,
            },
            end: Position {
                line: 4,
                character: 24,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 24,
            },
            end: Position {
                line: 6,
                character: 12,
            },
        },
    ])
    .run_test();
}

/// Regression test for issue #21
#[test]
fn misspelled_object_token() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE1]

            [TYPE:DOG]
                [ITEM:T1]
                [ITEM:T1]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "".to_owned(), // TODO this should still be correctly deserialized. See #20
        token_structure: vec![],
    })
    .add_test_syntax_diagnostics_codes(vec!["token_not_expected", "unchecked_code"])
    .add_test_syntax_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 3,
                character: 12,
            },
            end: Position {
                line: 3,
                character: 22,
            },
        },
        Range {
            start: Position {
                line: 3,
                character: 22,
            },
            end: Position {
                line: 6,
                character: 12,
            },
        },
    ])
    .run_test();
}
