#![allow(clippy::needless_update)]

use df_ls_core::Reference;
use df_ls_debug_structure::*;
use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn error_in_token_ref() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE1]

            [TYPE1:DOG]
                [ITE[M:T1]",
        )
        .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket"])
        .add_test_lexer_diagnostics_ranges(vec![Range {
            start: Position {
                line: 4,
                character: 20,
            },
            end: Position {
                line: 4,
                character: 20,
            },
        }]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec!["unknown_token", "unchecked_code"])
    .add_test_syntax_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 4,
                character: 16,
            },
            end: Position {
                line: 4,
                character: 20,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 20,
            },
            end: Position {
                line: 4,
                character: 26,
            },
        },
    ])
    .run_test();
}
