/// Provide custom error with links for when application panics (unrecoverable error).
#[macro_export]
macro_rules! setup_panic {
    () => {
        #[allow(unused_imports)]
        use colored::*;
        #[allow(unused_imports)]
        use std::panic::{self, PanicInfo};

        // This code is inspired by the `human-panic` crate.
        // Only use custom panic when `RUST_BACKTRACE` is not set.
        match ::std::env::var("RUST_BACKTRACE") {
            Err(_) => {
                panic::set_hook(Box::new(move |info: &PanicInfo| {
                    let payload = info.payload();
                    let panic_message = if let Some(s) = payload.downcast_ref::<&str>() {
                        s.to_string()
                    } else if let Some(s) = payload.downcast_ref::<String>() {
                        s.clone()
                    } else {
                        String::new()
                    };
                    let mut file = "unknown file";
                    let mut line = 0;
                    let mut column = 0;
                    if let Some(location) = info.location() {
                        file = location.file();
                        line = location.line();
                        column = location.column();
                    }
                    let panic_location = format!("{}:{}:{}", file, line, column);

                    // Print panic message
                    println!(
                        "{}: {}\nLocation: {}",
                        "PANIC".bright_red(),
                        panic_message,
                        panic_location
                    );
                    // Print report issue links
                    println!(
                        "{}: The application encountered an error it could not recover from.\n\
                        If you report this we might be able to fix this in the future.\n\
                        https://gitlab.com/df-modding-tools/df-raw-language-server/-/issues/new",
                        "PANIC".bright_red()
                    );
                }));
            }
            Ok(_) => {}
        }
    };
}
