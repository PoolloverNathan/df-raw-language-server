"use strict";

import { workspace } from "vscode";
import { LanguageClientOptions } from "vscode-languageclient/node";

export function getClientOptions(): LanguageClientOptions {
    return {
        // Register the server for plain text documents
        documentSelector: [
            { scheme: "file", language: "df_raw" },
            { scheme: "untitled", language: "df_raw" },
        ],
        outputChannelName: "DF Raw Language Server",
        synchronize: {
            // Notify the server about file changes to '.clientrc files contain in the workspace
            fileEvents: workspace.createFileSystemWatcher("**/.clientrc"),
        },
    };
}
