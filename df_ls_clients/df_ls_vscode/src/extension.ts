"use strict";

import { window, ExtensionContext, OutputChannel } from "vscode";
import { LanguageClient } from "vscode-languageclient/node";

import { getClientOptions } from "./client";
import { Logger } from './logger';
import { getServerOptions } from "./server";

let client: LanguageClient | undefined;
export let logger: Logger = new Logger();

async function startLangServer() {
    // Start whole language server client (may include starting server too)
    const client_option = getClientOptions();
    const server_option = getServerOptions();
    client = new LanguageClient(`df-raw-ls-vscode`, 'Dwarf Fortress RAW LS', server_option, client_option);

    if (client === undefined) {
        logger.error("DF Language Client is undefined.");
        return;
    }
    // Start the client. This will also launch the server
    await client.start();
    // context.subscriptions.push(client.start());
}


export function activate(context: ExtensionContext) {
    startLangServer();
}

export function deactivate(): Thenable<void> | undefined {
    if (!client) {
        return undefined;
    }
    // The client will shutdown the server too (using messaging)
    return client.stop();
}
