
import { window, OutputChannel } from "vscode";

export class Logger {
    private output_channel_client: OutputChannel
    private output_channel_server: OutputChannel

    public constructor() {
        this.output_channel_client = window.createOutputChannel('DF Language Client');
        this.output_channel_server = window.createOutputChannel('DF Language Server');
    }

    private entry_prefix(): string {
        let date = new Date();
        let hour = date.getUTCHours().toString().padStart(2, '0');
        let min = date.getUTCMinutes().toString().padStart(2, '0');
        let sec = date.getUTCSeconds().toString().padStart(2, '0');
        let time_str = `${hour}:${min}:${sec}`;

        return `${time_str}`;
    }

    private add_message(level: string, message: string) {
        let prefix = this.entry_prefix();
        let level_p = level.padEnd(5, ' ');
        this.output_channel_client.appendLine(`${prefix} [${level_p}] ${message}`);
    }

    public info(...messages: any[]) {
        this.add_message("INFO", messages.join(' '));
    }

    public warn(...messages: any[]) {
        this.add_message("WARN", messages.join(' '));
    }

    public error(...messages: any[]) {
        this.add_message("ERROR", messages.join(' '));
    }

    public debug(...messages: any[]) {
        this.add_message("DEBUG", messages.join(' '));
    }

    public log_server(message: string) {
        this.output_channel_server.append(`${message}`);
    }
}
