This project is licensed under MIT OR Apache-2.0.
For license files see: [MIT](./LICENSE-MIT), [Apache-2.0](./LICENSE-APACHE).