# Change log
In this file you will find all changes of the Dwarf Fortress language server.
This application follows the [Semantic Versioning standard](https://semver.org/).

## Unreleased

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## Version 0.4.1 (2022-12-13)

- Remove pre-release flag for VSCode Extension.

## Version 0.4.0 (2022-12-09)

- Improved error handling in VSCode extension (#121)
- Allow all skills in `MUSIC_SKILL` token. (#125)
- Mark only part of token with error on `wrong_arg_number` (#120)
- Added support for marking tokens with:
  - Added since.
  - Deprecated since. (#83)
  - Issue (in case of known DF issues/bugs). (#84)
- Support adding of multiple aliases.
- Switched from GNU libc to musl libc for Linux build.

## Version 0.3.0-rc.1 (2022-11-12)

- Fixed/Added various different tokens.
- Allow use of the Language Server over stdin/stdout.
- Improved parsing and checking speed.
- Improved code documentation.
- Improved error logger.
- Improved syntax highlighting.
- Added support for `ARGn`, `!ARGn` and `PipeArguments`.
- Implemented clamping of values within a range.
- Improved crash handling.
- Added `Any` argument.
- Fixed String vs Reference conflicts.

## Version 0.2.0-beta.1 (2022-01-01)

- Implement more tokens: Graphics, Inorganic, Items, Creatures and Creature variations.
- Added logging.
- Fixed various bugs.

## Version 0.2.0-alpha.1 (2021-08-01)

This is the initial version of the language server.
This version includes the following:
- Lexical Analysis for DF Raw files.
- Semantic Analysis for some of the DF Raw language parts:
body, body_detail_plan, building, creature (work in progress), descriptor_color, descriptor_pattern,
descriptor_shape, entity, interaction, item (some), language, material_template, plant, reaction,
tissue_template.
- This version is based on: Dwarf Fortress Classic 0.47.05 (2021-01-28)
- VSCode client
- Current supported features:
  - Syntax highlighting
  - Error reporting
