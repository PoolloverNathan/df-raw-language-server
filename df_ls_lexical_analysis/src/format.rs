use super::{Node, TreeCursor};
use colored::*;

/// Format settings
#[derive(Debug, Clone, Copy)]
pub struct AstFormatSettings {
    pub color: bool,
    pub tab_width: usize,
    /// Print input code correctly (indentation, no comments)
    pub print_formatted_code: bool,
    /// Show comments in Formatted code (might trow things off)
    pub show_comments: bool,
    pub replace_comment_whitespace: bool,
    pub print_comment_content: bool,
    /// Add S-expression to output
    pub print_s_exp: bool,
    /// Show `:`, `[` and `]`
    pub show_unnamed: bool,
    pub print_token_content: bool,
    /// Use old printing method
    pub use_old_format: bool,
}

impl Default for AstFormatSettings {
    fn default() -> Self {
        Self {
            color: true,
            tab_width: 2,
            print_formatted_code: true,
            show_comments: true,
            replace_comment_whitespace: true,
            print_comment_content: false,
            print_s_exp: true,
            show_unnamed: true,
            print_token_content: true,
            use_old_format: false,
        }
    }
}

pub fn print_ast(node: Node, source: &str, settings: &AstFormatSettings) {
    if settings.print_formatted_code {
        println!(
            "Code: \n{}\n---",
            format_code(node.clone(), source, settings)
        );
    }

    if settings.print_s_exp {
        println!(
            "Tree: \n{}\n---",
            match settings.use_old_format {
                true => node.to_sexp(0),
                false => format_tree(node, source, settings),
            }
        );
    }
}

pub fn format_code(node: Node, source: &str, settings: &AstFormatSettings) -> String {
    let mut tree_cursor = node.walk();

    // walk the tree depth-first
    format_code_node(&mut tree_cursor, 0, source, settings)
}

fn format_code_node(
    tree_cursor: &mut TreeCursor,
    tree_depth: usize,
    source: &str,
    settings: &AstFormatSettings,
) -> String {
    let node = tree_cursor.node();
    if node.is_named() {
        match node.kind().as_ref() {
            "header" => format!(
                "{}\n",
                node.utf8_text(source.as_bytes())
                    .expect("Source file contains non-UTF8 characters.")
                    .to_owned()
            ),
            "token_name"
            | "token_argument_integer"
            | "token_argument_character"
            | "token_argument_arg_n"
            | "token_argument_reference"
            | "token_argument_string"
            | "token_argument_pipe_arguments"
            | "token_argument_bang_arg_n"
            | "token_argument_bang_arg_n_sequence" => node
                .utf8_text(source.as_bytes())
                .expect("Source file contains non-UTF8 characters.")
                .to_owned(),
            "comment" => {
                if settings.show_comments {
                    let comment = node
                        .utf8_text(source.as_bytes())
                        .expect("Source file contains non-UTF8 characters.")
                        .to_owned();
                    if settings.replace_comment_whitespace {
                        let reformat_comment = comment
                            .as_bytes()
                            .split(|c| c.is_ascii_whitespace())
                            .filter(|c| !c.is_empty())
                            .map(|c| std::str::from_utf8(c).unwrap())
                            .fold("".to_owned(), |combiner, c| format!("{} {}", combiner, c));
                        let reformat_comment = reformat_comment.trim();
                        if !reformat_comment.is_empty() {
                            format!("{}\n", reformat_comment)
                        } else {
                            "".to_owned()
                        }
                    } else {
                        comment
                    }
                } else {
                    "".to_owned()
                }
            }
            _ => format_code_children(tree_cursor, tree_depth + 1, source, settings),
        }
    } else {
        match node.kind().as_ref() {
            "]" => format!("{}\n", node_code_string(node, settings)),
            _ => node_code_string(node, settings),
        }
    }
}

fn format_code_children(
    tree_cursor: &mut TreeCursor,
    tree_depth: usize,
    source: &str,
    settings: &AstFormatSettings,
) -> String {
    let mut output = "".to_owned();
    // go to first child
    if tree_cursor.goto_first_child() {
        output = format_code_node(tree_cursor, tree_depth, source, settings);
        while tree_cursor.goto_next_sibling() {
            output = format!(
                "{}{}",
                output,
                format_code_node(tree_cursor, tree_depth, source, settings)
            );
        }
        tree_cursor.goto_parent();
    }

    output
}

fn node_code_string(node: Node, _settings: &AstFormatSettings) -> String {
    node.kind()
}

pub fn format_tree(node: Node, source: &str, settings: &AstFormatSettings) -> String {
    let mut tree_cursor = node.walk();

    // walk the tree depth-first
    format_node(&mut tree_cursor, 0, source, settings)
}

fn format_children(
    tree_cursor: &mut TreeCursor,
    tree_depth: usize,
    source: &str,
    settings: &AstFormatSettings,
) -> String {
    let mut output = "".to_owned();
    // go to first child
    if tree_cursor.goto_first_child() {
        output = format_node(tree_cursor, tree_depth, source, settings);
        while tree_cursor.goto_next_sibling() {
            output = format!(
                "{}{}",
                output,
                format_node(tree_cursor, tree_depth, source, settings)
            );
        }
        tree_cursor.goto_parent();
    }

    output
}

fn format_node(
    tree_cursor: &mut TreeCursor,
    tree_depth: usize,
    source: &str,
    settings: &AstFormatSettings,
) -> String {
    let node = tree_cursor.node();
    if node.is_named() || (!node.is_named() && settings.show_unnamed) {
        if node.named_child_count() > 0 || (settings.show_unnamed && node.child_count() > 0) {
            format!(
                "{}({}\n{}{})\n",
                " ".repeat(tree_depth * settings.tab_width),
                node_string(&node, source, false, settings),
                format_children(tree_cursor, tree_depth + 1, source, settings),
                " ".repeat(tree_depth * settings.tab_width),
            )
        } else {
            // Has no children
            format!(
                "{}({})\n",
                " ".repeat(tree_depth * settings.tab_width),
                node_string(&node, source, true, settings),
            )
        }
    } else {
        // Nothing to add
        "".to_owned()
    }
}

fn node_string(
    node: &Node,
    source: &str,
    allow_content: bool,
    settings: &AstFormatSettings,
) -> String {
    let label = node_string_label(node, source, allow_content, settings);
    if !settings.color {
        return label;
    }
    if node.is_named() {
        label
    } else {
        label.yellow().to_string()
    }
}

fn node_string_label(
    node: &Node,
    source: &str,
    allow_content: bool,
    settings: &AstFormatSettings,
) -> String {
    if settings.print_token_content
        && allow_content
        && (node.kind() != "comment" || settings.print_comment_content)
        && node.is_named()
    {
        let content = node
            .utf8_text(source.as_bytes())
            .expect("Non UTF-8 Characters found");
        format!("{}: `{}`", node.kind(), content)
    } else {
        node.kind()
    }
}
