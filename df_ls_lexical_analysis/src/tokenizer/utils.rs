use super::*;
use df_ls_diagnostics::{DMExtraInfo, DiagnosticsInfo};

/// Handler for cases where we have `OkWithPrefixFound`.
pub(super) fn handle_prefix(prefix: DataNode, diagnostics: &mut DiagnosticsInfo) {
    diagnostics.add_message(
        // No extra template data needed
        DMExtraInfo::new(prefix.get_range()),
        "unexpected_characters",
    );
}
