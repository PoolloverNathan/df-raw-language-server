#![cfg(debug_assertions)]

use df_ls_diagnostics::{lsp_types::*, DiagnosticsInfo};
use pretty_assertions::assert_eq;

pub fn test_diagnostics(expected_diagnostics: &[Diagnostic], given_info: &DiagnosticsInfo) {
    let given_diagnostics = given_info.get_diagnostic_list();
    log::info!("Given Diagnostics:\n{:#?}", given_diagnostics);

    assert_diagnostics(expected_diagnostics, given_diagnostics);
}

pub fn assert_diagnostics(expected_diagnostics: &[Diagnostic], given_diagnostics: &[Diagnostic]) {
    assert_eq!(
        given_diagnostics.len(),
        expected_diagnostics.len(),
        "Diagnostics lists are not the same size."
    );
    for (expected, given) in expected_diagnostics.iter().zip(given_diagnostics.iter()) {
        assert_eq!(expected, given);
    }
}
