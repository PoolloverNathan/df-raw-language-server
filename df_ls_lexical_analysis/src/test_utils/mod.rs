#![cfg(debug_assertions)]

use crate::Tree;
use df_ls_diagnostics::{lsp_types::Diagnostic, Range};
use std::rc::Rc;

pub mod logging;
pub mod test_diagnostics;
pub mod test_diagnostics_codes;
pub mod test_diagnostics_ranges;
pub mod test_formatted_source;
pub mod test_s_exp;

#[derive(Debug, Clone)]
pub struct LexerTestBuilder {
    source: String,
    s_exp: Option<String>,
    formatted_source: Option<String>,
    lexer_diagnostics: Option<Vec<Diagnostic>>,
    lexer_diagnostics_codes: Option<Vec<String>>,
    lexer_diagnostics_ranges: Option<Vec<Range>>,
}

impl LexerTestBuilder {
    /// Create a new test case
    #[must_use = "Don't forget to call `.run_test()`"]
    pub fn test_source<S: AsRef<str>>(source: S) -> Self {
        Self {
            source: source.as_ref().to_owned(),
            s_exp: None,
            formatted_source: None,
            lexer_diagnostics: None,
            lexer_diagnostics_codes: None,
            lexer_diagnostics_ranges: None,
        }
    }

    /// Start running all the tests.
    /// This function will panic if the test failed.
    pub fn run_test(self) {
        self.run_test_with_result();
    }

    /// Start running all the tests.
    /// This function will panic if the test failed.
    /// It will return the output of the lexer so it can be used in the Syntax Analysis.
    pub fn run_test_with_result(self) -> (Rc<Tree>, String) {
        logging::setup_logger();

        // Start parsing the source
        let (tree, diagnostics_info) = crate::do_lexical_analysis(&self.source);

        // Test S-Expression
        if let Some(s_exp) = self.s_exp {
            test_s_exp::test_s_exp(s_exp, &tree, &self.source);
        }

        // Test formatting source code
        if let Some(formatted_source) = &self.formatted_source {
            test_formatted_source::test_formatted_source(formatted_source, &tree, &self.source);
        }

        // Test full lexer diagnostics message
        if let Some(lexer_diagnostics) = &self.lexer_diagnostics {
            test_diagnostics::test_diagnostics(lexer_diagnostics, &diagnostics_info);
        }

        // Test lexer diagnostic codes
        if let Some(lexer_diagnostics_codes) = &self.lexer_diagnostics_codes {
            test_diagnostics_codes::test_diagnostics_codes(
                lexer_diagnostics_codes,
                &diagnostics_info,
            );
        }

        // Test lexer diagnostic ranges
        if let Some(lexer_diagnostics_ranges) = &self.lexer_diagnostics_ranges {
            test_diagnostics_ranges::test_diagnostics_ranges(
                lexer_diagnostics_ranges,
                &diagnostics_info,
            );
        }

        (tree, self.source)
    }

    /// Add test to validate the S-Expression.
    ///
    /// Example S-Expression:
    /// ```txt
    /// (raw_file
    ///   (header)
    ///   (comment)
    ///   (token
    ///     ([)
    ///     (token_name)
    ///     (token_arguments
    ///       (:)
    ///       (token_argument_reference)
    ///     )
    ///     (])
    ///   )
    ///   (comment)
    ///   (EOF)
    /// )
    /// ```
    #[must_use = "Don't forget to call `.run_test()`"]
    pub fn add_test_s_exp<S: AsRef<str>>(mut self, s_exp: S) -> Self {
        self.s_exp = Some(s_exp.as_ref().to_owned());
        self
    }

    /// Add test to validate the formatting of the source code.
    #[must_use = "Don't forget to call `.run_test()`"]
    pub fn add_test_formatted_source<S: AsRef<str>>(mut self, formatted_source: S) -> Self {
        self.formatted_source = Some(formatted_source.as_ref().to_owned());
        self
    }

    /// Add test to validate a full diagnostic message.
    #[must_use = "Don't forget to call `.run_test()`"]
    pub fn add_test_lexer_diagnostics(mut self, diagnostics: Vec<Diagnostic>) -> Self {
        self.lexer_diagnostics = Some(diagnostics);
        self
    }

    /// Add test to validate the diagnostic codes.
    ///
    /// Example:
    /// ```rust,ignore
    /// vec!["missing_newline"]
    /// ```
    #[must_use = "Don't forget to call `.run_test()`"]
    pub fn add_test_lexer_diagnostics_codes(mut self, diagnostics_codes: Vec<&str>) -> Self {
        self.lexer_diagnostics_codes = Some(
            diagnostics_codes
                .into_iter()
                .map(|s| s.to_owned())
                .collect(),
        );
        self
    }

    /// Add test to validate the diagnostic ranges.
    ///
    /// Example:
    /// ```rust,ignore
    /// vec![Range {
    ///     start: Position {
    ///         line: 0,
    ///         character: 0,
    ///     },
    ///     end: Position {
    ///         line: 0,
    ///         character: 17,
    ///     },
    /// }]
    /// ```
    #[must_use = "Don't forget to call `.run_test()`"]
    pub fn add_test_lexer_diagnostics_ranges(mut self, diagnostics_ranges: Vec<Range>) -> Self {
        self.lexer_diagnostics_ranges = Some(diagnostics_ranges);
        self
    }
}
