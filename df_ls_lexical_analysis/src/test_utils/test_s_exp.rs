#![cfg(debug_assertions)]

use crate::{AstFormatSettings, Tree};
use pretty_assertions::assert_eq;
use std::rc::Rc;

pub fn test_s_exp(expected_s_exp: String, given_tree: &Rc<Tree>, source: &str) {
    let given_s_exp = crate::format::format_tree(
        given_tree.root_node(),
        source,
        &AstFormatSettings {
            color: false,
            tab_width: 2,
            print_formatted_code: true,
            show_comments: true,
            replace_comment_whitespace: true,
            print_comment_content: false,
            print_s_exp: true,
            show_unnamed: true,
            print_token_content: true,
            use_old_format: false,
        },
    );
    log::info!("Given S-Expression:\n{}", given_s_exp);

    assert_eq!(expected_s_exp, given_s_exp);
}
