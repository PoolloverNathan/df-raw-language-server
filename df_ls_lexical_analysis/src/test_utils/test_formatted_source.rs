#![cfg(debug_assertions)]

use crate::{AstFormatSettings, Tree};
use pretty_assertions::assert_eq;
use std::rc::Rc;

pub fn test_formatted_source(expected_formatted_source: &str, given_tree: &Rc<Tree>, source: &str) {
    let given_formatted_source = crate::format::format_code(
        given_tree.root_node(),
        source,
        &AstFormatSettings {
            color: false,
            tab_width: 2,
            print_formatted_code: true,
            show_comments: true,
            replace_comment_whitespace: true,
            print_comment_content: false,
            print_s_exp: true,
            show_unnamed: true,
            print_token_content: true,
            use_old_format: false,
        },
    );
    log::info!("Given Formatted Code:\n{}", given_formatted_source);

    assert_eq!(expected_formatted_source, given_formatted_source);
}
