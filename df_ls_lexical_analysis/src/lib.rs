#![forbid(unsafe_code)]
#![deny(clippy::all)]
#![allow(clippy::manual_map)]

pub mod format;
mod tokenizer;
pub mod utils;

#[cfg(debug_assertions)]
pub mod test_utils;

use df_ls_diagnostics::DiagnosticsInfo;
use std::rc::Rc;
pub use tokenizer::{Node, Point, Range, Tree, TreeCursor};
// Only used for fuzzing
pub use format::AstFormatSettings;
pub use tokenizer::tokenize_df_raw_file;
pub(crate) use utils::mark_rest_of_file_as_unchecked_tok;

pub fn do_lexical_analysis(source: &str) -> (Rc<Tree>, DiagnosticsInfo) {
    tokenizer::tokenize_df_raw_file(source.to_owned(), true)
}

pub fn print_ast(tree: &Tree, source: &str, settings: &AstFormatSettings) {
    let root_node = tree.root_node();
    format::print_ast(root_node, source, settings);
}
