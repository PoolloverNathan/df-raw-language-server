use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

#[test]
fn test_end_of_file_1() {
    LexerTestBuilder::test_source("")
        .add_test_s_exp(
            "(raw_file
  (EOF: ``)
)
",
        )
        .add_test_lexer_diagnostics_codes(vec!["missing_header"])
        .add_test_lexer_diagnostics_ranges(vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }])
        .run_test();
}

#[test]
fn test_end_of_file_1_1() {
    LexerTestBuilder::test_source("  ")
        .add_test_s_exp(
            "(raw_file
  (EOF: ``)
)
",
        )
        .add_test_lexer_diagnostics_codes(vec!["missing_header"])
        .add_test_lexer_diagnostics_ranges(vec![Range {
            start: Position {
                line: 0,
                character: 2,
            },
            end: Position {
                line: 0,
                character: 2,
            },
        }])
        .run_test();
}

#[test]
fn test_end_of_file_1_2() {
    LexerTestBuilder::test_source("  \n")
        .add_test_s_exp(
            "(raw_file
  (EOF: ``)
)
",
        )
        .add_test_lexer_diagnostics_codes(vec!["missing_header"])
        .add_test_lexer_diagnostics_ranges(vec![Range {
            start: Position {
                line: 0,
                character: 2,
            },
            end: Position {
                line: 0,
                character: 2,
            },
        }])
        .run_test();
}

#[test]
fn test_end_of_file_2() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard
",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![])
    .add_test_lexer_diagnostics_ranges(vec![])
    .run_test();
}

#[test]
fn test_end_of_file_2_1() {
    LexerTestBuilder::test_source("descriptor_color_standard")
        .add_test_s_exp(
            "(raw_file
  (header: `descriptor_color_standard`)
  (EOF: ``)
)
",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![])
        .run_test();
}

#[test]
fn test_end_of_file_3() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard

        [",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (comment)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_token_name", "missing_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 2,
                character: 9,
            },
            end: Position {
                line: 2,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 9,
            },
            end: Position {
                line: 2,
                character: 9,
            },
        },
    ])
    .run_test();
}

#[test]
fn test_end_of_file_4() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard

        [TEST",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (comment)
  (token
    ([)
    (token_name: `TEST`)
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![Range {
        start: Position {
            line: 2,
            character: 13,
        },
        end: Position {
            line: 2,
            character: 13,
        },
    }])
    .run_test();
}

#[test]
fn test_end_of_file_5() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard

        [TEST:",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (comment)
  (token
    ([)
    (token_name: `TEST`)
    (token_arguments
      (:)
      (token_argument_empty: ``)
    )
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![Range {
        start: Position {
            line: 2,
            character: 14,
        },
        end: Position {
            line: 2,
            character: 14,
        },
    }])
    .run_test();
}

#[test]
fn test_end_of_file_6() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard

        [TEST:string",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (comment)
  (token
    ([)
    (token_name: `TEST`)
    (token_arguments
      (:)
      (token_argument_string: `string`)
    )
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![Range {
        start: Position {
            line: 2,
            character: 20,
        },
        end: Position {
            line: 2,
            character: 20,
        },
    }])
    .run_test();
}

#[test]
fn test_end_of_file_7() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard

        [TEST::",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (comment)
  (token
    ([)
    (token_name: `TEST`)
    (token_arguments
      (:)
      (token_argument_empty: ``)
      (:)
      (token_argument_empty: ``)
    )
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![Range {
        start: Position {
            line: 2,
            character: 15,
        },
        end: Position {
            line: 2,
            character: 15,
        },
    }])
    .run_test();
}

#[test]
fn test_end_of_file_8() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard

        [TEST::str",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (comment)
  (token
    ([)
    (token_name: `TEST`)
    (token_arguments
      (:)
      (token_argument_empty: ``)
      (:)
      (token_argument_string: `str`)
    )
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![Range {
        start: Position {
            line: 2,
            character: 18,
        },
        end: Position {
            line: 2,
            character: 18,
        },
    }])
    .run_test();
}

#[test]
fn test_end_of_file_9() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard

        [TEST::REF",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (comment)
  (token
    ([)
    (token_name: `TEST`)
    (token_arguments
      (:)
      (token_argument_empty: ``)
      (:)
      (token_argument_reference: `REF`)
    )
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![Range {
        start: Position {
            line: 2,
            character: 18,
        },
        end: Position {
            line: 2,
            character: 18,
        },
    }])
    .run_test();
}

#[test]
fn test_end_of_file_10() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard
        [TEST:REF]",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (comment)
  (token
    ([)
    (token_name: `TEST`)
    (token_arguments
      (:)
      (token_argument_reference: `REF`)
    )
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![])
    .add_test_lexer_diagnostics_ranges(vec![])
    .run_test();
}
