use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

/// Test Windows encoded newline files.
/// instead of `\n` (LF) they have `\r\n` (CRLF)
/// For more info see: https://en.wikipedia.org/wiki/Newline#Representation
#[test]
fn test_crlf() {
    LexerTestBuilder::test_source(
        "creature_domestic\r
        \r
        [OBJECT:CREATURE]\r
        \r
        [CREATURE:DOG]\r
            [CASTE:FEMALE]\r
                [FEMALE]\r
            [CASTE:MALE]\r
                [MALE]\r
            [SELECT_CASTE:ALL]\r
                [NATURAL]\r
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `creature_domestic`)
  (comment)
  (token
    ([)
    (token_name: `OBJECT`)
    (token_arguments
      (:)
      (token_argument_reference: `CREATURE`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `CREATURE`)
    (token_arguments
      (:)
      (token_argument_reference: `DOG`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `CASTE`)
    (token_arguments
      (:)
      (token_argument_reference: `FEMALE`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `FEMALE`)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `CASTE`)
    (token_arguments
      (:)
      (token_argument_reference: `MALE`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `MALE`)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `SELECT_CASTE`)
    (token_arguments
      (:)
      (token_argument_reference: `ALL`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `NATURAL`)
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![])
    .add_test_lexer_diagnostics_ranges(vec![])
    .run_test();
}

#[test]
fn test_crlf_error_case() {
    LexerTestBuilder::test_source(
        "creature_ocean_new\r
        \r
        [CREATURE:CRAB_MAN]\r
        \r
        Missing start bracket\r
        AMPHIBIOUS]\r
        \r
        Missing end bracket\r
        [COPY_TAGS_FROM:CRAB\r
        \r
        Missing first part of token\r
        [:A DF Raw Language Server developer with the head and pincers of a crab.]\r
        \r
        Missing token\r
        []\r
        \r
        First part of token has to be of type 'token_name'\r
        [crab man:crab men:crab man]\r
        [100]\r
        ['c']\r
        \r
        Should not be able to spread tokens across multiple lines (probably)\r
        THIS HAS NOT BEEN IMPLEMENTED YET, SO RIGHT NOW IT'S JUST SEEN AS A NORMAL TOKEN\r
        [\r
            DESCRIPTION\r
            :This and above DESCRIPTION should be considered comments.\r
        ]\r
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `creature_ocean_new`)
  (comment)
  (token
    ([)
    (token_name: `CREATURE`)
    (token_arguments
      (:)
      (token_argument_reference: `CRAB_MAN`)
    )
    (])
  )
  (comment)
  (ERROR: `]`)
  (comment)
  (token
    ([)
    (token_name: `COPY_TAGS_FROM`)
    (token_arguments
      (:)
      (token_argument_reference: `CRAB`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: ``)
    (token_arguments
      (:)
      (token_argument_string: `A DF Raw Language Server developer with the head and pincers of a crab.`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: ``)
    (ERROR: `crab man`)
    (token_arguments
      (:)
      (token_argument_string: `crab men`)
      (:)
      (token_argument_string: `crab man`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: ``)
    (ERROR: `100`)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: ``)
    (ERROR: `'c'`)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (comment)
  (ERROR: `]`)
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![
        "unexpected_end_bracket",
        "missing_end_bracket",
        "missing_token_name",
        "missing_token_name",
        "missing_token_name",
        "unexpected_characters",
        "missing_token_name",
        "unexpected_characters",
        "missing_token_name",
        "unexpected_characters",
        "missing_token_name",
        "missing_end_bracket",
        "unexpected_end_bracket",
    ])
    .add_test_lexer_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 5,
                character: 18,
            },
            end: Position {
                line: 5,
                character: 19,
            },
        },
        Range {
            start: Position {
                line: 8,
                character: 28,
            },
            end: Position {
                line: 8,
                character: 28,
            },
        },
        Range {
            start: Position {
                line: 11,
                character: 9,
            },
            end: Position {
                line: 11,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 14,
                character: 9,
            },
            end: Position {
                line: 14,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 17,
                character: 9,
            },
            end: Position {
                line: 17,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 17,
                character: 9,
            },
            end: Position {
                line: 17,
                character: 17,
            },
        },
        Range {
            start: Position {
                line: 18,
                character: 9,
            },
            end: Position {
                line: 18,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 18,
                character: 9,
            },
            end: Position {
                line: 18,
                character: 12,
            },
        },
        Range {
            start: Position {
                line: 19,
                character: 9,
            },
            end: Position {
                line: 19,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 19,
                character: 9,
            },
            end: Position {
                line: 19,
                character: 12,
            },
        },
        Range {
            start: Position {
                line: 23,
                character: 9,
            },
            end: Position {
                line: 23,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 23,
                character: 9,
            },
            end: Position {
                line: 23,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 26,
                character: 8,
            },
            end: Position {
                line: 26,
                character: 9,
            },
        },
    ])
    .run_test();
}
