use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

/// This is a test for issue #38
#[test]
fn test_volume_mb() {
    LexerTestBuilder::test_source(
        "creature_domestic

        [VOLUME_mB:DOG]
        [VOLUME_mB:0:10000]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `creature_domestic`)
  (comment)
  (token
    ([)
    (token_name: `VOLUME_mB`)
    (token_arguments
      (:)
      (token_argument_reference: `DOG`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `VOLUME_mB`)
    (token_arguments
      (:)
      (token_argument_integer: `0`)
      (:)
      (token_argument_integer: `10000`)
    )
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![])
    .add_test_lexer_diagnostics_ranges(vec![])
    .run_test();
}
