use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

#[test]
fn test_token_argument_integer_permutations() {
    LexerTestBuilder::test_source(
        "Token Argument Integer
        [A:5]
        [A:0]
        [A:10]
        [A:-1]
        [A:-0]
        [A:-10]
        [A:250]
        [A:-250]
        [A:05]
        [A:0005]
        [A:-0006]
        [A:1234567890123456789]
        [A:0000000000000000000000000]
        [A:12:32]
        [A:00000000000000000000000000000000000000000000000000000001]
        [A:-00000000000000000000000000000000000000000000000000000001]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `Token Argument Integer`)
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `5`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `0`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `10`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `-1`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `-0`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `-10`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `250`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `-250`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `05`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `0005`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `-0006`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `1234567890123456789`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `0000000000000000000000000`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `12`)
      (:)
      (token_argument_integer: `32`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `00000000000000000000000000000000000000000000000000000001`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_integer: `-00000000000000000000000000000000000000000000000000000001`)
    )
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![])
    .add_test_lexer_diagnostics_ranges(vec![])
    .run_test();
}
