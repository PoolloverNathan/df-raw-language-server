use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

#[test]
fn test_brackets_permutations() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard

        []
        [[]
        []]
        [
        ]
        [  ]
        ]  [
        [ a ]
        [ [ ]
        [ ] ]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (comment)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (token
    ([)
    (token_name: ``)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (ERROR: `]`)
  (comment)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (comment)
  (ERROR: `]`)
  (comment)
  (token
    ([)
    (token_name: ``)
    (ERROR: `  `)
    (])
  )
  (comment)
  (ERROR: `]`)
  (comment)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: ``)
    (ERROR: ` a `)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: ``)
    (ERROR: ` `)
    (])
  )
  (token
    ([)
    (token_name: ``)
    (ERROR: ` `)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: ``)
    (ERROR: ` `)
    (])
  )
  (comment)
  (ERROR: `]`)
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![
        "missing_token_name",
        "missing_token_name",
        "missing_end_bracket",
        "missing_token_name",
        "missing_token_name",
        "unexpected_end_bracket",
        "missing_token_name",
        "missing_end_bracket",
        "unexpected_end_bracket",
        "missing_token_name",
        "unexpected_characters",
        "unexpected_end_bracket",
        "missing_token_name",
        "missing_end_bracket",
        "missing_token_name",
        "unexpected_characters",
        "missing_token_name",
        "unexpected_characters",
        "missing_end_bracket",
        "missing_token_name",
        "unexpected_characters",
        "missing_token_name",
        "unexpected_characters",
        "unexpected_end_bracket",
    ])
    .add_test_lexer_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 2,
                character: 9,
            },
            end: Position {
                line: 2,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 3,
                character: 9,
            },
            end: Position {
                line: 3,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 3,
                character: 9,
            },
            end: Position {
                line: 3,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 3,
                character: 10,
            },
            end: Position {
                line: 3,
                character: 10,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 9,
            },
            end: Position {
                line: 4,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 10,
            },
            end: Position {
                line: 4,
                character: 11,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 9,
            },
            end: Position {
                line: 5,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 9,
            },
            end: Position {
                line: 5,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 8,
            },
            end: Position {
                line: 6,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 7,
                character: 9,
            },
            end: Position {
                line: 7,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 7,
                character: 9,
            },
            end: Position {
                line: 7,
                character: 11,
            },
        },
        Range {
            start: Position {
                line: 8,
                character: 8,
            },
            end: Position {
                line: 8,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 8,
                character: 12,
            },
            end: Position {
                line: 8,
                character: 12,
            },
        },
        Range {
            start: Position {
                line: 8,
                character: 12,
            },
            end: Position {
                line: 8,
                character: 12,
            },
        },
        Range {
            start: Position {
                line: 9,
                character: 9,
            },
            end: Position {
                line: 9,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 9,
                character: 9,
            },
            end: Position {
                line: 9,
                character: 12,
            },
        },
        Range {
            start: Position {
                line: 10,
                character: 9,
            },
            end: Position {
                line: 10,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 10,
                character: 9,
            },
            end: Position {
                line: 10,
                character: 10,
            },
        },
        Range {
            start: Position {
                line: 10,
                character: 10,
            },
            end: Position {
                line: 10,
                character: 10,
            },
        },
        Range {
            start: Position {
                line: 10,
                character: 11,
            },
            end: Position {
                line: 10,
                character: 11,
            },
        },
        Range {
            start: Position {
                line: 10,
                character: 11,
            },
            end: Position {
                line: 10,
                character: 12,
            },
        },
        Range {
            start: Position {
                line: 11,
                character: 9,
            },
            end: Position {
                line: 11,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 11,
                character: 9,
            },
            end: Position {
                line: 11,
                character: 10,
            },
        },
        Range {
            start: Position {
                line: 11,
                character: 12,
            },
            end: Position {
                line: 11,
                character: 13,
            },
        },
    ])
    .run_test();
}
