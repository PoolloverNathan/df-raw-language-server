use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

#[test]
fn test_basic_grammar() {
    LexerTestBuilder::test_source(
        "creature_domestic

        [OBJECT:CREATURE]

        [CREATURE:DOG]
            [CASTE:FEMALE]
                [FEMALE]
            [CASTE:MALE]
                [MALE]
            [SELECT_CASTE:ALL]
                [NATURAL]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `creature_domestic`)
  (comment)
  (token
    ([)
    (token_name: `OBJECT`)
    (token_arguments
      (:)
      (token_argument_reference: `CREATURE`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `CREATURE`)
    (token_arguments
      (:)
      (token_argument_reference: `DOG`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `CASTE`)
    (token_arguments
      (:)
      (token_argument_reference: `FEMALE`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `FEMALE`)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `CASTE`)
    (token_arguments
      (:)
      (token_argument_reference: `MALE`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `MALE`)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `SELECT_CASTE`)
    (token_arguments
      (:)
      (token_argument_reference: `ALL`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `NATURAL`)
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![])
    .add_test_lexer_diagnostics_ranges(vec![])
    .run_test();
}
