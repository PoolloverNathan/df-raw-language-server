# Dwarf Fortress RAW language server
[![Discord](https://img.shields.io/discord/762761192510455850?logo=discord)](https://discord.gg/6eKf5ZY)

Clients/Extensions:<br/>
[![VSCode extension](https://img.shields.io/visual-studio-marketplace/i/df-modding-tools.dwarf-fortress-raw-vscode?label=VSCode%20Installs&logo=visual-studio-code)][VSCode_Marketplace] (Windows and Linux supported)

Language server (shared between clients):<br/>
[![pipeline status](https://gitlab.com/df-modding-tools/df-raw-language-server/badges/master/pipeline.svg)](https://gitlab.com/df-modding-tools/df-raw-language-server/-/commits/master)
[![coverage report](https://gitlab.com/df-modding-tools/df-raw-language-server/badges/master/coverage.svg)](https://codecov.io/gl/df-modding-tools/df-raw-language-server)

Dwarf Fortress RAW language server is an application that allows
[Dwarf Fortress RAW files](https://dwarffortresswiki.org/index.php/Raw_file)
to be checked for correctness.
This allows modders of the game ([Dwarf Fortress][Dwarf_Fortress]) to detect problems in their mod files.

This repo is work in progress and might not work in some cases.

## Supported DF Versions

This version of DF Language Server is compatible with DF Version: **0.47.05** (2021/01/28)<br/>
We also support the previous 2 versions of DF*: (Going forward)

*: Previous versions are supported as closely as possible, in case of big breaking changes we might
break partial or full support of previous versions if not feasible.

Latest DF RAW Language Server version: `0.4.1` (2022-12-13)

We will start/have started working on supporting the Steam/Itch.io versions.
Check our [Discord][Discord] for updates. [All help is welcome!](#contribute)

## What can it do?

Syntax Highlighting and Error reporting:<br />
![general](image/general.png)

<details>
<summary><b>View more Features and Screenshots</b></summary>

1. It reads a DF Raw file and add Syntax Highlighting:<br />
![syntax_highlighting](image/syntax_highlighting.png)

1. And works correctly with your theme:<br />
![syntax_highlighting](image/theme2.png)
![syntax_highlighting](image/theme3.png)

1. It checks if all the tokens are correct and have the correct arguments:<br />
![wrong_arg_type](image/wrong_arg_type.png)<br /><br />
![wrong_enum_value](image/wrong_enum_value.png)<br /><br />
![wrong_arg_number_to_many](image/wrong_arg_number_to_many.png)

1. It warns you of about best practices:<br />
![warnings](image/warnings.png)

And much more to come!
</details>

### Future plans

We have many things we still want to add.
If you want to see what we have planned check out our [Roadmap](Roadmap.md).
But adding these features might take some time. [So all help is welcome!](#contribute)

## Supported Editors and IDEs

We are currently only supporting [VS Code](https://code.visualstudio.com/).
But the language server is able to work on other Editors and IDEs.

- ✅: Working and tested
- 👥: Community member creation (not part of this repo or group)
- 🛠: Work in progress, partly implemented
- ❌: Not Implemented
- ⛔: Known to not (yet) support language servers

| IDE                                                      | Status & Download |
| -------------------------------------------------------- | ----------------- |
| [VS Code](https://code.visualstudio.com/)                | ✅ [Download][VSCode_Marketplace]|
| [Visual Studio](https://visualstudio.microsoft.com/)     | ❌     |
| [Atom](https://atom.io/)                                 | 🛠     |
| [Sublime Text](https://www.sublimetext.com/)             | ❌     |
| [Eclipse](https://www.eclipse.org/ide/)                  | ❌     |
| [Vim/NeoVim](https://neovim.io/)                         | ❌     |
| [Emacs](https://www.gnu.org/software/emacs/)             | 🛠     |
| [Notepad++](https://notepad-plus-plus.org/)              | ⛔     |
| ... others ...                                           | ❌     |

If you have questions or suggestions about supported IDEs, contact us though [Discord][Discord].

## Contribute
<a name="contribute"></a>

If you want to contribute, join our [Discord][Discord].
We are always looking for more people to help us develop, test, research, ...
You don't need to be a programmer or know about modding in DF to help us out.

There are a few good places to start:
- Look at the open
[beginner issues](https://gitlab.com/df-modding-tools/df-raw-language-server/-/issues?label_name%5B%5D=Beginner)
or [research needed issues](https://gitlab.com/df-modding-tools/df-raw-language-server/-/issues?label_name%5B%5D=Research+Needed)
- Check the [`#syntax-room[1-2]`][Discord] channel for any questions that we might have in there.
- Ask in [`#development`][Discord] channel what we need help with, we would love to see you join!
- Just test the application and report bugs, through [Discord][Discord] or
[GitLab](https://gitlab.com/df-modding-tools/df-raw-language-server/-/issues).

## Development

You can find a full development guide on this page: [Developement.md](/docs/Development.md)

## License

The code in this project is licensed under the MIT or Apache 2.0 license.

All documentation[^1] is licensed under
[GNU FDL](https://www.gnu.org/licenses/fdl-1.3.html),
[MIT license](https://choosealicense.com/licenses/mit/) and/or
[Creative Commons Attribution-ShareAlike 3.0 Unported license](https://creativecommons.org/licenses/by-sa/3.0/)

This makes the documentation both compatible with the
[Dwarf Fortress Wiki](https://dwarffortresswiki.org) and [Wikipedia](https://www.wikipedia.org/).

All contributions, code and documentation, to this project will be similarly licensed.

[^1]: This includes all Rust Doc, documentation and other info in this codebase.

[^2]: This is implemented, but some parts of it don't work correctly (will not produce proper errors).

[Discord]: https://discord.gg/6eKf5ZY
[VSCode_Marketplace]: https://marketplace.visualstudio.com/items?itemName=df-modding-tools.dwarf-fortress-raw-vscode
[Dwarf_Fortress]: https://bay12games.com/dwarves/
