# Syntax Highlight Tests

This folder contains some files that can be used to check if a Syntax highlighter
and Lexer is correctly implemented.

Some file will be ending with the `.df-invalid` extension.
This is because these files will be incorrect (and thus contain errors).
Changing the extension will thus make sure we don't see any errors in our project while working on it.

File that end on `.txt` and `.df` should be correct.
So if they contain errors they should be shown.
