# Syntax Highlighting

Syntax Highlighting is the process of providing color to the code.
Most IDEs have support for this in various ways. But each IDE has a slightly different
approach to this.

In this document we will explain how most highlighters work and the rules we
apply in our supported clients/IDEs.

We hope this will both be useful for maintaining our supported clients and creating
new syntax highlighter support for other clients.

First of all, a syntax highlighter is not perfect. Especially for the DF Mod language.
You will see that there are various spacial cases that a syntax highlighter can
not deal with because of the complex rules.
But we have done out best to make it as good as possible.
Some features of the language can only be handled by more complex parser, like our
Language server.

## How do most Syntax Highlighters work?

Syntax highlighting is done differently in all IDEs, but
most IDEs have syntax highlighters that work on files or sections and they are checked
independently of each other.
On each file/section they will run the syntax highlighting code.

It first detects what language it is. It gets this info though the file extension,
user or project settings. Or it might try to detect it using some other way.

Once it knows what language to use it will get the correct syntax rules and start applying them.

The syntax rules are usually defined in config file, separate from the parsing code and use
[regex](https://en.wikipedia.org/wiki/Regular_expression) rules to split the text into
different part.

Each part is given a tag/token/label to indicate what kind of style should be applied.

The color for each label is then found in the theme and that color is applied to that tag.
Because most modern IDEs have theme support we do not pick the colors, but the colors
of the theme are used. This limits the amount of colors we can use in the file.

## DF Raw Language

In this section we will explain how the DF Raw Syntax Highlighter is structured.
As our examples we will use the VSCode implementation.
You can [find the final code here](../df_ls_clients/df_ls_vscode/syntax-highlighter/df_raw.tmLanguage.json).

### Names

Lets us first define some term so we can use them below:
- **Header**: The first line of a DF Raw file, usually the filename without the extension.
- **Token**: DF Raw file are build from tokens, each token starts with `[` and ends with `]`.
Example: `[COLOR:AMBER]`
- **Token Name**: The first value just after `[` in a token. Example: in `[COLOR:AMBER]`,
`COLOR` is the token name.
- **Token Arguments**: All other arguments after the token name.
All arguments are separated by `:`.
- **Comments**: All other text outside of the header and tokens. This includes the newlines.
- **String**: A token argument that contains (multiple) lower case characters and or spaces.
- **Number/Integers**: A token argument that contains only numbers. Decimal numbers are not allowed.
- **Character**: A token argument that contains a number or one character surrounded by
single quotes (`'`).
- **Reference** (REF): A token argument that contains only uppercase characters.

Some lesser used names:
- **Arg N**: An argument filled in by DF. Example: `ARG2`.
- **Bang Arg N**: An argument filled in by DF. Example: `!ARG2`.
- **Pipe Arguments**: A list of arguments within 1 token argument.
Each argument is separated by the `|` pipe character.

### DF Raw file layout.

Every valid DF Raw file follows the following format.
```
<header>
(
    <token>
    or
    <comment>
) repeated multiple times.
```

Example DF Raw file:
```df_raw
descriptor_color_standard

[OBJECT:DESCRIPTOR_COLOR]

[COLOR:AMBER]
    [NAME:amber]
    [WORD:AMBER]
    [RGB:255:191:0]
```

### Regex rules
We try to keep the rules below up-to-date. But they might very from the files
in the repo or the file in production. Please let us know if you spot any differences.

Regex rules are generally not easy to read. So we hope the explanation below helps.

Note that in some files the `\` need to be escaped.
For example: `\\w` is the `\w` regex rule for "word".
This is not always the case, but depends on the file and format.

Regex rules are generally the same everywhere.
But there might be small differences.
In the examples below we follow [this definition](https://macromates.com/manual/en/regular_expressions).
Because this is the spec that VSCode follows.

#### Header

Regex: `\A([ \t]*)([^\[\]\r\n]+)`

Explanation and parts:
```
\A                       | Beginning of File
([ \t]*)                 | 1st capture group that allows a sequence of
                         | spaces and tabs. Will be matched as "#comment".
(                        | 2nd capture group. Will be labeled as a "section header".
    [^\[\]\r\n]+         | The header requires at least one character
                         | it can not contain `[`, `]`, `\r` or `\n` characters.
)                        |
```

Example:
```
descriptor_color_standard
```

#### Comment

Regex: `[^\[\]]+`

Explanation and parts:
```
[^\[\]]+                 | Any character sequence that does not contain a `[` or `]`
                         | character. It must also not be captured by "#header" or "#token"
                         | Will be labeled as a "line comment".
```

Example:
```
This is a comment, just some text not in brackets. (and not the header)
```

#### Token

Regex: `(\[)([^\]\r\n]+)(\])`

Explanation and parts:
```
(\[)                     | 1st capture group, the open bracket.
                         | Will be labeled as "punctuation begin token".
([^\]\r\n]+)             | 2nd capture group, everything inside the token.
                         | Allow everything inside the token except `]`, `\r`, `\n`.
                         | Will be matched with "token_body".
(\])                     | 3rd capture group, the close bracket.
                         | Will be marked as "punctuation end token".
```

Note: The 2nd capture group does allow for the `[` character because it is allowed as
a character. Hence why it is not in the 'except' group. See issue #111.

Examples:
```
[OBJECT:DESCRIPTOR_COLOR]
[COLOR:AMBER]
[NAME:amber]
[WORD:AMBER]
[RGB:255:191:0]
```

#### Token body

Regex: `([^\:\[\]\r\n]+)(:[^\]\r\n]+)?`

Explanation and parts:
```
(                        | 1st capture group, the token name.
                         | Will be matched with "#token_name".
    [^\:\[\]\r\n]+       | Everything before the `:` will be matched to this except for
                         | the following characters: `:`, `[`, `]`, `\r` and `\n`.
)                        |
(                        | 2nd capture group, all arguments of the token.
                         | Will be matched with "#token_arguments".
    :                    | The `:` character to separated `#token_name` from
                         | `#token_arguments`
                         | The `:` is included in the `#token_arguments` grouping.
    [^\]\r\n]+           | Allow everything inside the token except `]`, `\r`, `\n`.
)?                       | Token arguments are optional.
```

Note: The 2nd capture group does allow for the `[` character because it is allowed as
a character. See issue #111.

Examples:
```
OBJECT:DESCRIPTOR_COLOR
COLOR:AMBER
NAME:amber
WORD:AMBER
RGB:255:191:0
```

#### Token name

Regex: `((?:VOLUME_mB)|(?:[A-Z][A-Z_0-9]*))`

Explanation and parts:
```
(                        | 1st capture group, the string of the token name.
                         | Will be labeled as "keyword control type".
    (?:VOLUME_mB)        | Special case because this is the only token name that includes a
                         | lower case character. (Issue #38)
    |                    | OR
    (?:                  | Non-capture group, just to group part together.
        [A-Z]            | First character must be an uppercase letter.
                         | Must be at least one character.
        [A-Z_0-9]*       | All other allowed characters. (Uppercase, numbers and `_`)
    )                    |
)                        |
```

Examples:
```
OBJECT
COLOR
NAME
WORD
DISPLAY_COLOR
```

#### Token arguments

This regex expression will be recursively applied to the rest of the token body.

Regex: `(:)([^\]:]*)([^\\]]*)`

Explanation and parts:
```
(:)                      | 1st capture group, the colon.
                         | Will be labeled as "punctuation token separator".
(                        | 2nd capture group, an argument.
                         | This will be further matched against all types:
                         | - `#token_argument_integer`
                         | - `#token_argument_character`
                         | - `#token_argument_arg_n`
                         | - `#token_argument_reference`
                         | - `#token_argument_string`
                         | - `#token_argument_pipe_arguments`
                         | The order of the above is important because some values will
                         | match on more then one.
    [^\]:]*              | Allow everything inside the token except `]` and `:`.
                         | The "Empty" argument is allowed.
)                        |
(                        | 3rd capture group, all other arguments.
                         | Will be matched with "#token_arguments".
                         | This part will thus be matched against itself again until
                         | this part is empty.
    [^\]]*               | Allow everything inside the token except `]`.
                         | Can also be empty is there are no more arguments.
)                        |
```

Empty tokens are allowed and look like this: `[NAME:]`.

Examples:
```
:DESCRIPTOR_COLOR
:AMBER
:amber
:AMBER:AMBERS
:255:191:0
:
```

#### Token Argument Integer

Matches any positive or negative integer values (Z/ℤ).
DF RAW does not have support for decimal/rational numbers (Q/ℚ).

Regex: `-?[0-9]+$`

Explanation and parts:
```
                         | The whole regex should be labeled as
                         | "constant numeric".
-?                       | The number can start with a `-` symbol,
                         | denoting a negative number.
[0-9]+                   | Any sequence of numbers.
                         | At least one number is required.
$                        | Next character matches: End of string/line.
```

Note: The last `$` is present because of special cases where there are some characters left over.
In those cases it should try to match with something else.
In most cases it would fall back to `#token_argument_string`.
An example of this is `[A:10m]`, `10` would match as an integer, but the `m`
would make it a string. It is thus not a full match for integer.
The same rules apply to all other token arguments.

Leading zeros do not effect the value of the number and will be ignored when parsing. (issue #113)

Examples:
```
5
-5
-900
00900
255
123456789
```

#### Token Argument Character

Regex: `(')[^\]:\r\n](')$`

Explanation and parts:
```
                         | The whole regex should be labeled as
                         | "string quoted single".
(')                      | 1st capture group, the single open quote.
                         | Will be labeled as "punctuation definition character".
[^\]:\r\n]               | Allow everything inside the token except `]`, `:`, `\r` and `\n`.
                         | Matches exactly 1 character.
(')                      | 1st capture group, the single open quote.
                         | Will be labeled as "punctuation definition character".
$                        | Next character matches: End of string/line.
```

Examples:
```
'z'
'k'
'!'
'''
'₧'
```

#### Token Argument Arg N

Arg N is a very special case that is not widely used in DF Raw files.
This only applies to certain tokens. But is still something that is part
of the language.

These arguments are generally only valid in `BP_LAYERS` tokens and alike,
in the section `BODY_DETAIL_PLAN`.

Regex: `ARG(?:0|[1-9][0-9]*)$`

Explanation and parts:
```
                         | The whole regex should be labeled as
                         | "variable language".
ARG                      | A sequence starting with "ARG"
(?:0|[1-9][0-9]*)        | followed by any sequence of numbers.
                         | At least 1 number is required.
                         | No leading zeros allowed.
$                        | Next character matches: End of string/line.
```

Examples:
```
ARG1
ARG3
ARG5
ARG10
ARG0
```

#### Token Argument Reference

The following regex looks more complex because it does not accept `!` in most cases.
Only when in following format: `!ARGn` anywhere in the Reference.

Regex: `(?:[0-9]_*)*([A-Z][A-Z_0-9]*)$`

Explanation and parts:
```
                         | The whole regex should be labeled as
                         | "entity name type".
(?:                      | Non-capture group, optional prefix
    [0-9]_*              | A reference start with a number followed by underscore sequence.
)*                       | `*` makes the group before optional and repeatable.
(                        | 1st capture group.
                         | Will be matched with "#token_argument_bang_arg_n".
    [A-Z]                | Requires at least 1 uppercase character.
    [A-Z_0-9]*           | An uppercase character, underscore or number.
                         | `*` makes the last selection before optional and repeatable.
)                        |
$                        | Next character matches: End of string/line.
```

Examples:
```
REF
REF2
2_REF
569REF
```

#### Token Argument String

Regex: `([^\|\[\]:]+)$`

Explanation and parts:
```
                         | The whole regex should be labeled as
                         | "string unquoted".
(                        | 1st capture group.
                         | Will be matched with "#token_argument_bang_arg_n".
    [^\|\[\]:]+          | Allow everything except `|`, `[`, `]` and `:`.
                         | Matches at least 1 character.
)                        |
$                        | Next character matches: End of string/line.
```

Examples:
```
string
String_string
>=<.-,+*)('&%$
UPPER_a
A chain of 5 meters
```

#### Token Argument Pipe Arguments

Note that this regex is very similar to `#token_argument_string`.
The difference is in that this argument accepts the `|` (pipe) character.
That is the only difference.

Regex: `([^\[\]:]+)$`

Explanation and parts:
```
                         | The whole regex should be labeled as
                         | "variable other" or "string interpolated".
(                        | 1st capture group.
    [^\[\]:]+            | Allow everything except `[`, `]` and `:`.
                         | Matches at least 1 character.
)                        |
$                        | Next character matches: End of string/line.
```

Note that the label for this might not be correct.
But we wanted to make this stand out from other strings so you can
clearly see the difference between a string and a pipe argument.

Examples:
```
string|string
string|0
multiple|pipe|chars
-5|are|OK
>=<.-,+*|'&%$
```

#### Token Argument Bang Arg N

Regex: `!ARG(?:0|[1-9][0-9]*)`

Explanation and parts:
```
                         | The whole regex should be labeled as
                         | "variable language".
!ARG                     | A sequence starting with "!ARG"
(?:0|[1-9][0-9]*)        | followed by any sequence of numbers.
                         | At least 1 number is required.
                         | No leading zeros allowed.
                         | This expression can be matched multiple
                         | times in a string/references.
```

Examples:
```
!ARG1
!ARG3
!ARG5
!ARG10
!ARG0
```
