# VSCode install instructions

## Test the VSCode extension

Here are some instructions that help you with installing and debugging the plugin.

### Installing

Download the latest version through the [VS Marketplace][VSCode_Marketplace]

#### Latest (development) build
See the "Pre-Release version" (if any) that might be available through
the [VS Marketplace][VSCode_Marketplace].

TODO: Add instructions and screenshot.

### Debug Extension
To debug the extension there are a few options you have where you can find different info.
Open the output console at the bottom of your screen (`Ctrl + Shift + Y`) -> `Output` -> Dropdown:
 - `DF Language Client` for the VSCode Client log. (Typescript)
 - `DF Language Server` for the DF Language Server log. (Rust)
 - `Log (Window)` for process info (if server crashes).
 - (`DF Raw Language Server` for info about what server responses are coming back.) (No longer used)

Logs are stored in:
- Linux: `/home/username/.local/share/df_language_server/`
- Windows: `C:\Users\username\AppData\Local\DF_Modding_Tools\DF_Language_Server\data\`
- Mac: `/Users/username/Library/Application Support/org.DF_Modding_Tools.DF_Language_Server/`

Here you will find different info that might be useful for debugging any problems.
