use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Copy, Debug, PartialEq, Default, Eq, Hash)]
pub struct BangArgN(pub u16);

impl BangArgN {
    pub const MIN: u16 = u16::MIN;
    pub const MAX: u16 = u16::MAX;
}

impl From<u16> for BangArgN {
    fn from(item: u16) -> Self {
        BangArgN(item)
    }
}

impl From<BangArgN> for u16 {
    fn from(item: BangArgN) -> Self {
        item.0
    }
}
