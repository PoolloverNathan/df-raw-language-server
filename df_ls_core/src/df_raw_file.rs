use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq, Eq)]
pub struct DFRawFile<T: Default> {
    pub header: String,
    pub token_structure: T,
}
