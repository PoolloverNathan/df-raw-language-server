use df_ls_core::{Choose, Clamp, DFChar, Reference, ReferenceTo};
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_structure::*;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_building() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "building_custom

            [OBJECT:BUILDING]

            [BUILDING_WORKSHOP:SOAP_MAKER]
                [NAME:Soap Maker's Workshop]
                [NAME_COLOR:7:0:1]
                [DIM:3:3]
                [WORK_LOCATION:2:2]
                [BUILD_LABOR:SOAP_MAKER]
                [BUILD_KEY:CUSTOM_SHIFT_S]
                [BLOCK:1:0:0:1]
                [BLOCK:2:0:0:1]
                [BLOCK:3:0:0:0]
                [TILE:0:1:' ':' ':150]
                [TILE:0:2:' ':' ':'/']
                [TILE:0:3:'-':' ':' ']
                [COLOR:0:1:0:0:0:0:0:0:6:0:0]
                [COLOR:0:2:0:0:0:0:0:0:6:0:0]
                [COLOR:0:3:6:0:0:0:0:0:0:0:0]
                [TILE:1:1:' ':' ':'=']
                [TILE:1:2:'-':' ':8]
                [TILE:1:3:' ':' ':150]
                [COLOR:1:1:0:0:0:0:0:0:6:0:0]
                [COLOR:1:2:6:0:0:0:0:0:6:0:0]
                [COLOR:1:3:0:0:0:0:0:0:6:0:0]
                [TILE:2:1:'-':' ':8]
                [TILE:2:2:' ':' ':8]
                [TILE:2:3:' ':150:' ']
                [COLOR:2:1:6:0:0:0:0:0:6:0:0]
                [COLOR:2:2:0:0:0:0:0:0:6:0:0]
                [COLOR:2:3:0:0:0:6:0:0:0:0:0]
                [TILE:3:1:150:' ':8]
                [TILE:3:2:' ':' ':8]
                [TILE:3:3:' ':240:' ']
                [COLOR:3:1:6:0:0:0:0:0:6:7:0]
                [COLOR:3:2:0:0:0:0:0:0:6:7:0]
                [COLOR:3:3:0:0:0:7:0:1:0:0:0]
                [BUILD_ITEM:1:BUCKET:NONE:NONE:NONE]
                    [EMPTY]
                    [CAN_USE_ARTIFACT]
                [BUILD_ITEM:1:NONE:NONE:NONE:NONE]
                    [BUILDMAT]
                    [WORTHLESS_STONE_ONLY]
                    [CAN_USE_ARTIFACT]

            [BUILDING_FURNACE:FINISHING_FORGE]
                [NAME:Finishing Forge]
                [NAME_COLOR:7:0:1]
                [BUILD_LABOR:ARCHITECT]
                [BUILD_KEY:CUSTOM_F]
                [DIM:3:3]
                [WORK_LOCATION:2:2]
                [BLOCK:1:0:0:0]
                [BLOCK:2:0:0:0]
                [BLOCK:3:0:0:0]
                [TILE:0:1:32:7:8]
                [TILE:0:2:32:32:177]
                [TILE:0:3:35:35:177]
                [COLOR:0:1:0:0:1:0:0:1:7:0:0]
                [COLOR:0:2:0:0:1:0:0:1:7:0:0]
                [COLOR:0:3:7:0:1:0:0:1:7:0:1]
                [TILE:1:1:229:61:32]
                [TILE:1:2:45:32:8]
                [TILE:1:3:32:35:177]
                [COLOR:1:1:0:0:1:0:0:1:7:0:0]
                [COLOR:1:2:7:0:1:0:0:1:7:0:0]
                [COLOR:1:3:0:0:1:0:0:1:7:0:1]
                [TILE:2:1:32:240:32]
                [TILE:2:2:229:32:32]
                [TILE:2:3:7:35:177]
                [COLOR:2:1:7:0:1:7:0:0:7:0:0]
                [COLOR:2:2:0:0:1:0:0:1:7:0:0]
                [COLOR:2:3:0:0:1:7:0:1:7:0:1]
                [TILE:3:1:42:240:178]
                [TILE:3:2:43:210:93]
                [TILE:3:3:229:178:178]
                [COLOR:3:1:7:0:1:7:0:0:7:0:0]
                [COLOR:3:2:7:0:0:7:0:0:7:7:0]
                [COLOR:3:3:7:0:0:7:0:0:7:0:0]
                [BUILD_ITEM:1:ANVIL:NONE:NONE:NONE][CAN_USE_ARTIFACT]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DFRaw {
        header: "building_custom".to_owned(),
        token_structure: vec![ObjectToken {
            building_tokens: vec![
                BuildingToken::Workshop(BuildingGeneralToken {
                    reference: Some(ReferenceTo::new("SOAP_MAKER".to_owned())),
                    name: Some("Soap Maker\'s Workshop".to_owned()),
                    name_color: Some((7, 0, 1)),
                    dim: Some((Clamp::new(3), Clamp::new(3))),
                    work_location: Some((Clamp::new(2), Clamp::new(2))),
                    build_labor: vec![LaborEnum::SoapMaking],
                    build_key: Some(KeyBindEnum::CustomShiftS),
                    block: vec![
                        (1, false, vec![false, true]),
                        (2, false, vec![false, true]),
                        (3, false, vec![false, false]),
                    ],
                    tile: vec![
                        (
                            Clamp::new(0),
                            1,
                            DFChar(' '),
                            vec![DFChar(' '), DFChar('û')],
                        ),
                        (
                            Clamp::new(0),
                            2,
                            DFChar(' '),
                            vec![DFChar(' '), DFChar('/')],
                        ),
                        (
                            Clamp::new(0),
                            3,
                            DFChar('-'),
                            vec![DFChar(' '), DFChar(' ')],
                        ),
                        (
                            Clamp::new(1),
                            1,
                            DFChar(' '),
                            vec![DFChar(' '), DFChar('=')],
                        ),
                        (
                            Clamp::new(1),
                            2,
                            DFChar('-'),
                            vec![DFChar(' '), DFChar('◘')],
                        ),
                        (
                            Clamp::new(1),
                            3,
                            DFChar(' '),
                            vec![DFChar(' '), DFChar('û')],
                        ),
                        (
                            Clamp::new(2),
                            1,
                            DFChar('-'),
                            vec![DFChar(' '), DFChar('◘')],
                        ),
                        (
                            Clamp::new(2),
                            2,
                            DFChar(' '),
                            vec![DFChar(' '), DFChar('◘')],
                        ),
                        (
                            Clamp::new(2),
                            3,
                            DFChar(' '),
                            vec![DFChar('û'), DFChar(' ')],
                        ),
                        (
                            Clamp::new(3),
                            1,
                            DFChar('û'),
                            vec![DFChar(' '), DFChar('◘')],
                        ),
                        (
                            Clamp::new(3),
                            2,
                            DFChar(' '),
                            vec![DFChar(' '), DFChar('◘')],
                        ),
                        (
                            Clamp::new(3),
                            3,
                            DFChar(' '),
                            vec![DFChar('≡'), DFChar(' ')],
                        ),
                    ],
                    color: vec![
                        (
                            Clamp::new(0),
                            1,
                            Choose::Choice2((0, 0, 0)),
                            vec![Choose::Choice2((0, 0, 0)), Choose::Choice2((6, 0, 0))],
                        ),
                        (
                            Clamp::new(0),
                            2,
                            Choose::Choice2((0, 0, 0)),
                            vec![Choose::Choice2((0, 0, 0)), Choose::Choice2((6, 0, 0))],
                        ),
                        (
                            Clamp::new(0),
                            3,
                            Choose::Choice2((6, 0, 0)),
                            vec![Choose::Choice2((0, 0, 0)), Choose::Choice2((0, 0, 0))],
                        ),
                        (
                            Clamp::new(1),
                            1,
                            Choose::Choice2((0, 0, 0)),
                            vec![Choose::Choice2((0, 0, 0)), Choose::Choice2((6, 0, 0))],
                        ),
                        (
                            Clamp::new(1),
                            2,
                            Choose::Choice2((6, 0, 0)),
                            vec![Choose::Choice2((0, 0, 0)), Choose::Choice2((6, 0, 0))],
                        ),
                        (
                            Clamp::new(1),
                            3,
                            Choose::Choice2((0, 0, 0)),
                            vec![Choose::Choice2((0, 0, 0)), Choose::Choice2((6, 0, 0))],
                        ),
                        (
                            Clamp::new(2),
                            1,
                            Choose::Choice2((6, 0, 0)),
                            vec![Choose::Choice2((0, 0, 0)), Choose::Choice2((6, 0, 0))],
                        ),
                        (
                            Clamp::new(2),
                            2,
                            Choose::Choice2((0, 0, 0)),
                            vec![Choose::Choice2((0, 0, 0)), Choose::Choice2((6, 0, 0))],
                        ),
                        (
                            Clamp::new(2),
                            3,
                            Choose::Choice2((0, 0, 0)),
                            vec![Choose::Choice2((6, 0, 0)), Choose::Choice2((0, 0, 0))],
                        ),
                        (
                            Clamp::new(3),
                            1,
                            Choose::Choice2((6, 0, 0)),
                            vec![Choose::Choice2((0, 0, 0)), Choose::Choice2((6, 7, 0))],
                        ),
                        (
                            Clamp::new(3),
                            2,
                            Choose::Choice2((0, 0, 0)),
                            vec![Choose::Choice2((0, 0, 0)), Choose::Choice2((6, 7, 0))],
                        ),
                        (
                            Clamp::new(3),
                            3,
                            Choose::Choice2((0, 0, 0)),
                            vec![Choose::Choice2((7, 0, 1)), Choose::Choice2((0, 0, 0))],
                        ),
                    ],
                    build_item: vec![
                        BuildItemToken {
                            build_item: Some((
                                1,
                                ItemReferenceArg {
                                    item_type: Reference("BUCKET".to_owned()),
                                    item_subtype: Choose::Choice1(NoSubtypeEnum::None),
                                },
                                Choose::Choice2((NoneEnum::None, NoneEnum::None)),
                            )),
                            can_use_artifact: Some(()),
                            empty: Some(()),
                            ..Default::default()
                        },
                        BuildItemToken {
                            build_item: Some((
                                1,
                                ItemReferenceArg {
                                    item_type: Reference("NONE".to_owned()),
                                    item_subtype: Choose::Choice1(NoSubtypeEnum::None),
                                },
                                Choose::Choice2((NoneEnum::None, NoneEnum::None)),
                            )),
                            build_material: Some(()),
                            can_use_artifact: Some(()),
                            worthless_stone_only: Some(()),
                            ..Default::default()
                        },
                    ],
                    needs_magma: None,
                }),
                BuildingToken::Furnace(BuildingGeneralToken {
                    reference: Some(ReferenceTo::new("FINISHING_FORGE".to_owned())),
                    name: Some("Finishing Forge".to_owned()),
                    name_color: Some((7, 0, 1)),
                    dim: Some((Clamp::new(3), Clamp::new(3))),
                    work_location: Some((Clamp::new(2), Clamp::new(2))),
                    build_labor: vec![LaborEnum::Architect],
                    build_key: Some(KeyBindEnum::CustomF),
                    block: vec![
                        (1, false, vec![false, false]),
                        (2, false, vec![false, false]),
                        (3, false, vec![false, false]),
                    ],
                    tile: vec![
                        (
                            Clamp::new(0),
                            1,
                            DFChar(' '),
                            vec![DFChar('•'), DFChar('◘')],
                        ),
                        (
                            Clamp::new(0),
                            2,
                            DFChar(' '),
                            vec![DFChar(' '), DFChar('▒')],
                        ),
                        (
                            Clamp::new(0),
                            3,
                            DFChar('#'),
                            vec![DFChar('#'), DFChar('▒')],
                        ),
                        (
                            Clamp::new(1),
                            1,
                            DFChar('σ'),
                            vec![DFChar('='), DFChar(' ')],
                        ),
                        (
                            Clamp::new(1),
                            2,
                            DFChar('-'),
                            vec![DFChar(' '), DFChar('◘')],
                        ),
                        (
                            Clamp::new(1),
                            3,
                            DFChar(' '),
                            vec![DFChar('#'), DFChar('▒')],
                        ),
                        (
                            Clamp::new(2),
                            1,
                            DFChar(' '),
                            vec![DFChar('≡'), DFChar(' ')],
                        ),
                        (
                            Clamp::new(2),
                            2,
                            DFChar('σ'),
                            vec![DFChar(' '), DFChar(' ')],
                        ),
                        (
                            Clamp::new(2),
                            3,
                            DFChar('•'),
                            vec![DFChar('#'), DFChar('▒')],
                        ),
                        (
                            Clamp::new(3),
                            1,
                            DFChar('*'),
                            vec![DFChar('≡'), DFChar('▓')],
                        ),
                        (
                            Clamp::new(3),
                            2,
                            DFChar('+'),
                            vec![DFChar('╥'), DFChar(']')],
                        ),
                        (
                            Clamp::new(3),
                            3,
                            DFChar('σ'),
                            vec![DFChar('▓'), DFChar('▓')],
                        ),
                    ],
                    color: vec![
                        (
                            Clamp::new(0),
                            1,
                            Choose::Choice2((0, 0, 1)),
                            vec![Choose::Choice2((0, 0, 1)), Choose::Choice2((7, 0, 0))],
                        ),
                        (
                            Clamp::new(0),
                            2,
                            Choose::Choice2((0, 0, 1)),
                            vec![Choose::Choice2((0, 0, 1)), Choose::Choice2((7, 0, 0))],
                        ),
                        (
                            Clamp::new(0),
                            3,
                            Choose::Choice2((7, 0, 1)),
                            vec![Choose::Choice2((0, 0, 1)), Choose::Choice2((7, 0, 1))],
                        ),
                        (
                            Clamp::new(1),
                            1,
                            Choose::Choice2((0, 0, 1)),
                            vec![Choose::Choice2((0, 0, 1)), Choose::Choice2((7, 0, 0))],
                        ),
                        (
                            Clamp::new(1),
                            2,
                            Choose::Choice2((7, 0, 1)),
                            vec![Choose::Choice2((0, 0, 1)), Choose::Choice2((7, 0, 0))],
                        ),
                        (
                            Clamp::new(1),
                            3,
                            Choose::Choice2((0, 0, 1)),
                            vec![Choose::Choice2((0, 0, 1)), Choose::Choice2((7, 0, 1))],
                        ),
                        (
                            Clamp::new(2),
                            1,
                            Choose::Choice2((7, 0, 1)),
                            vec![Choose::Choice2((7, 0, 0)), Choose::Choice2((7, 0, 0))],
                        ),
                        (
                            Clamp::new(2),
                            2,
                            Choose::Choice2((0, 0, 1)),
                            vec![Choose::Choice2((0, 0, 1)), Choose::Choice2((7, 0, 0))],
                        ),
                        (
                            Clamp::new(2),
                            3,
                            Choose::Choice2((0, 0, 1)),
                            vec![Choose::Choice2((7, 0, 1)), Choose::Choice2((7, 0, 1))],
                        ),
                        (
                            Clamp::new(3),
                            1,
                            Choose::Choice2((7, 0, 1)),
                            vec![Choose::Choice2((7, 0, 0)), Choose::Choice2((7, 0, 0))],
                        ),
                        (
                            Clamp::new(3),
                            2,
                            Choose::Choice2((7, 0, 0)),
                            vec![Choose::Choice2((7, 0, 0)), Choose::Choice2((7, 7, 0))],
                        ),
                        (
                            Clamp::new(3),
                            3,
                            Choose::Choice2((7, 0, 0)),
                            vec![Choose::Choice2((7, 0, 0)), Choose::Choice2((7, 0, 0))],
                        ),
                    ],
                    build_item: vec![BuildItemToken {
                        build_item: Some((
                            1,
                            ItemReferenceArg {
                                item_type: Reference("ANVIL".to_owned()),
                                item_subtype: Choose::Choice1(NoSubtypeEnum::None),
                            },
                            Choose::Choice2((NoneEnum::None, NoneEnum::None)),
                        )),
                        can_use_artifact: Some(()),
                        ..Default::default()
                    }],
                    needs_magma: None,
                }),
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
