use df_ls_core::ReferenceTo;
use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_structure::*;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_pattern() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "descriptor_pattern_iris_eye

            [OBJECT:DESCRIPTOR_PATTERN]

            [COLOR_PATTERN:IRIS_EYE_AMBER]
                [PATTERN:IRIS_EYE]
                [CP_COLOR:WHITE]
                [CP_COLOR:BLACK]
                [CP_COLOR:AMBER]

            [COLOR_PATTERN:PUPIL_EYE_AMBER]
                [PATTERN:PUPIL_EYE]
                [CP_COLOR:BLACK]
                [CP_COLOR:AMBER]

            [COLOR_PATTERN:SPOTS_ORANGE_BLACK]
                [PATTERN:SPOTS]
                [CP_COLOR:ORANGE]
                [CP_COLOR:BLACK]

            [COLOR_PATTERN:WRONG]
                [PATTERN:SPO]
                [CP_COLOR:ORANGE]
                [CP_COLOR:BLACK]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DFRaw {
        header: "descriptor_pattern_iris_eye".to_owned(),
        token_structure: vec![ObjectToken {
            pattern_tokens: vec![
                PatternToken {
                    reference: Some(ReferenceTo::new("IRIS_EYE_AMBER".to_owned())),
                    pattern: Some(PatternEnum::IrisEye),
                    cp_color: vec![
                        ReferenceTo::new("WHITE".to_owned()),
                        ReferenceTo::new("BLACK".to_owned()),
                        ReferenceTo::new("AMBER".to_owned()),
                    ],
                },
                PatternToken {
                    reference: Some(ReferenceTo::new("PUPIL_EYE_AMBER".to_owned())),
                    pattern: Some(PatternEnum::PupilEye),
                    cp_color: vec![
                        ReferenceTo::new("BLACK".to_owned()),
                        ReferenceTo::new("AMBER".to_owned()),
                    ],
                },
                PatternToken {
                    reference: Some(ReferenceTo::new("SPOTS_ORANGE_BLACK".to_owned())),
                    pattern: Some(PatternEnum::Spots),
                    cp_color: vec![
                        ReferenceTo::new("ORANGE".to_owned()),
                        ReferenceTo::new("BLACK".to_owned()),
                    ],
                },
                PatternToken {
                    reference: Some(ReferenceTo::new("WRONG".to_owned())),
                    pattern: None,
                    cp_color: vec![
                        ReferenceTo::new("ORANGE".to_owned()),
                        ReferenceTo::new("BLACK".to_owned()),
                    ],
                },
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec!["wrong_enum_value"])
    .add_test_syntax_diagnostics_ranges(vec![Range {
        start: Position {
            line: 21,
            character: 25,
        },
        end: Position {
            line: 21,
            character: 28,
        },
    }])
    .run_test();
}
