use df_ls_core::ReferenceTo;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_structure::*;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;
use indexmap::indexmap;

#[test]
fn test_graphics() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "graphics_example

            [OBJECT:GRAPHICS]

            [TILE_PAGE:DWARVES]
                [FILE:example/dwarves.bmp]
                [TILE_DIM:16:16]
                [PAGE_DIM:4:1]

            [CREATURE_GRAPHICS:DWARF]
                [DEFAULT:DWARVES:0:0:ADD_COLOR]
                [MINER:DWARVES:1:0:AS_IS:DEFAULT]

                Custom position example:

                [MANAGER:DWARVES:2:0:AS_IS:DEFAULT]

            [CREATURE_GRAPHICS:HORSE]
                [DEFAULT:DWARVES:3:0:AS_IS:DEFAULT]
                [CHILD:DWARVES:3:1:AS_IS:DEFAULT]
                [CHILD:DWARVES:3:1:AS_IS:ANIMATED]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DFRaw {
        header: "graphics_example".to_owned(),
        token_structure: vec![ObjectToken {
            graphics_tokens: vec![
                GraphicsToken::TilePage(TilePageToken {
                    reference: Some(ReferenceTo::new("DWARVES".to_owned())),
                    file: Some("example/dwarves.bmp".to_owned()),
                    tile_dimensions: Some((16, 16)),
                    page_dimensions: Some((4, 1)),
                }),
                GraphicsToken::CreatureGraphics(CreatureGraphicsToken {
                    reference: Some(ReferenceTo::new("DWARF".to_owned())),
                    main_texture_tokens: indexmap! {
                        "DEFAULT".to_owned() => vec![(
                            ReferenceTo::new(
                                "DWARVES".to_owned(),
                            ),
                            0,
                            0,
                            ColorTypeEnum::AddColor,
                            None,
                        )],
                    },
                    other_graphics_tokens: indexmap! {
                        "MINER".to_owned() => vec![(
                            ReferenceTo::new(
                                "DWARVES".to_owned(),
                            ),
                            1,
                            0,
                            ColorTypeEnum::AsIs,
                            TextureTypeEnum::Default,
                        )],
                        "MANAGER".to_owned() => vec![(
                            ReferenceTo::new(
                                "DWARVES".to_owned(),
                            ),
                            2,
                            0,
                            ColorTypeEnum::AsIs,
                            TextureTypeEnum::Default,
                        )],
                    },
                }),
                GraphicsToken::CreatureGraphics(CreatureGraphicsToken {
                    reference: Some(ReferenceTo::new("HORSE".to_owned())),
                    main_texture_tokens: indexmap! {
                        "DEFAULT".to_owned() => vec![(
                            ReferenceTo::new(
                                "DWARVES".to_owned(),
                            ),
                            3,
                            0,
                            ColorTypeEnum::AsIs,
                            Some(TextureTypeEnum::Default),
                        )],
                    },
                    other_graphics_tokens: indexmap! {
                        "CHILD".to_owned() => vec![(
                            ReferenceTo::new(
                                "DWARVES".to_owned(),
                            ),
                            3,
                            1,
                            ColorTypeEnum::AsIs,
                            TextureTypeEnum::Default,
                        ),(
                            ReferenceTo::new(
                                "DWARVES".to_owned(),
                            ),
                            3,
                            1,
                            ColorTypeEnum::AsIs,
                            TextureTypeEnum::Animated,
                        )],
                    },
                }),
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
