use crate::{
    AmmoToken, ArmorToken, BuildingToken, FoodToken, GlovesToken, HelmToken, InstrumentToken,
    ItemReferenceArg, ItemSpecificEnum, KeyBindEnum, MaterialTokenArgWithNoneNone,
    MaterialTokenArgWithReagentMat, NoneEnum, PantsToken, ShieldToken, ShoesToken, SiegeAmmoToken,
    SkillEnum, ToolToken, ToyToken, TrapCompToken, WeaponToken,
};
use df_ls_core::{AllowEmpty, Choose, Clamp, Reference, ReferenceTo, Referenceable};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_syntax_analysis::{Token, TokenDeserialize, TryFromArgumentGroup};
use serde::{Deserialize, Serialize};

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct ReactionToken {
    /// Defines a new reaction
    #[df_token(token_name = "REACTION", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// Defines the name used by the reaction in-game.
    #[df_token(token_name = "NAME")]
    pub name: Option<String>,
    /// Provides a text description of the reaction ingame when it is highlighted in the building UI.
    /// Multiple `DESCRIPTION` tokens can be defined in a reaction, and each will appear on a new line.
    /// The pop-up box that contains the description is limited to 325 characters total.
    ///
    /// Alternatively, this token can reference a `DESCRIPTION` token in an existing tool definition
    /// by replacing string with `USE_TOOL:<ITEM_SUBTYPE>`; or an existing instrument definition with
    /// `USE_INSTRUMENT:<ITEM_SUBTYPE>`.
    #[df_token(token_name = "DESCRIPTION")]
    pub description: Vec<AllowEmpty<Choose<ReactionDescriptionUseItemTokenArg, String>>>,
    /// This version of the reaction is not used by dwarves at home in a fortress, but rather
    /// the wanderers of Adventure Mode. When using this token, it will be allowed for adventurers
    /// of any race, without editing Entity files.
    #[df_token(token_name = "ADVENTURE_MODE_ENABLED")]
    pub adventure_mode_enabled: Option<()>,
    /// Amount of attributes given per skill improvement.
    ///
    /// Default is 10.
    ///
    /// Adding since: v0.47.01
    // TODO always positive?
    #[df_token(token_name = "ATTRIBUTE_IP")]
    pub attribute_ip: Option<i32>,
    /// The reaction will be queued automatically if the reaction reagents are all present.
    #[df_token(token_name = "AUTOMATIC")]
    pub automatic: Option<()>,
    /// Sets the building that the reaction will be performed in, and the button used to queue
    /// the reaction once that building's menu is accessed in-game
    #[df_token(token_name = "BUILDING")]
    pub building: Vec<(ReferenceTo<BuildingToken>, Choose<KeyBindEnum, NoneEnum>)>, // 2nd param is KeyBind
    /// Puts the reaction in a category. Categories are custom submenus for reaction menus.
    /// The category ID is a unique identifier for the category. It is only used in the raws, and
    /// will not appear in the game.
    ///
    /// If you're defining multiple categories within the same reaction - for example, if you
    /// intend the reaction to be nested two deep, and haven't yet defined the super-category -
    /// the last CATEGORY token within the reaction definition is the one that the reaction will
    /// appear in.
    #[df_token(token_name = "CATEGORY")]
    pub category: Option<ReactionCategoryToken>,
    /// Requires that the reaction either use up a unit of coal or charcoal or
    /// else be performed at a magma workshop
    #[df_token(token_name = "FUEL")]
    pub fuel: Option<()>,
    /// Sets the maximum number of times a reaction is allowed to run when using stacked reagents.
    /// This can be used to ensure that the reaction doesn't repeat until
    /// the entire stack is depleted.
    #[df_token(token_name = "MAX_MULTIPLIER")]
    pub max_multiplier: Option<u32>,
    /// Skill used by the reaction
    #[df_token(token_name = "SKILL")]
    pub skill: Option<SkillEnum>,
    /// Amount of skill given per product made.
    ///
    /// Default is 30.
    ///
    /// Adding since: v0.47.01
    // TODO always positive?
    #[df_token(token_name = "SKILL_IP")]
    pub skill_ip: Option<i32>,
    /// Proportion of how much the skill level effects the outcome (example: quality) of the result.
    ///
    /// The skill roll is `random(basic range) + random((skill level * multiplier)/2 + 1) +
    /// random((skill level * multiplier)/2 + 1)`.
    /// random(x) returns a number between 0 and x-1, so basic range is always 1 or more.
    /// Higher skill rolls give better results.
    ///
    /// Arguments:
    /// - Basic Range: The maximum value for the basic range. Need to be equal or bigger then `1`.
    /// - Skill level multiplier: Used to increase effectiveness of the skill level.
    ///
    /// Default is `[SKILL_ROLL_RANGE:11:5]`
    ///
    /// Adding since: v0.47.01
    #[df_token(token_name = "SKILL_ROLL_RANGE")]
    pub skill_roll_range: Option<(Clamp<u32, 1, { u32::MAX as isize }>, u32)>,
    /// Reagent must contain writing.
    /// Arguments are: `<chance>:<reagent/product target>:<type of improvement>:<mat tokens>`đđ
    /// `<mat tokens>` might consists of 3 arguments,
    /// for example: `GET_MATERIAL_FROM_REAGENT:reagent:REACTION_PRODUCT_ID`
    /// Other times it has 2 extra arguments like: `INORGANIC:BRONZE_COATING`
    // |-----0-----|-1-|----2-----|----------3----------|---------------------4--------------------|
    // [IMPROVEMENT:100:instrument:INSTRUMENT_PIECE:BODY:GET_MATERIAL_FROM_REAGENT:drum  :NONE     ]
    // [IMPROVEMENT:100:jug       :GLAZED               :GET_MATERIAL_FROM_REAGENT:glaze :GLAZE_MAT]
    // [IMPROVEMENT:100:a         :INSTRUMENT_PIECE:BODY:METAL                    :FRAME           ]
    // [IMPROVEMENT:100:target    :SPIKES               :GET_MATERIAL_FROM_REAGENT:gem   :NONE     ]
    #[df_token(token_name = "IMPROVEMENT")]
    pub improvement: Vec<(
        u8,                             // chance
        Reference,                      // ReferenceTo<ReagentToken> // reagent
        ImprovementTypeTokenArg,        // improvement_type
        MaterialTokenArgWithReagentMat, // Material arguments
    )>,
    /// Requires a given reagent as an input for a reaction
    #[df_token(token_name = "REAGENT")]
    pub reagents: Vec<ReagentToken>,
    /// See description on `ProductToken`
    #[df_token(token_name = "PRODUCT")]
    pub products: Vec<ProductToken>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct ReagentToken {
    /// Requires a given reagent as an input for a reaction
    /// Arguments are: `<name/id>:<quantity>:<item token>:<mat tokens>`
    /// `<mat tokens>` and `<item token>` might consists of 2 arguments,
    /// for example: `BOULDER:NO_SUBTYPE` or `INORGANIC:COAL_BITUMINOUS`
    #[df_token(token_name = "REAGENT", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    // [REAGENT:A            :1  :NONE       :NONE      :NONE     :NONE           ]
    // [REAGENT:lye          :150:LIQUID_MISC:NONE      :LYE                      ]
    // [REAGENT:lye container:1  :NONE       :NONE      :NONE     :NONE           ]
    // [REAGENT:A            :1  :BOULDER    :NO_SUBTYPE:INORGANIC:COAL_BITUMINOUS]
    pub reference: Option<(
        ReferenceTo<Self>,                              // Name/id
        u32,                                            // Quantity
        Choose<(NoneEnum, NoneEnum), ItemReferenceArg>, // Item token
        MaterialTokenArgWithNoneNone,                   // Material token
    )>,
    /// Reagent material must have the `[BONE]` token.
    #[df_token(token_name = "ANY_BONE_MATERIAL")]
    pub any_bone_material: Option<()>,
    /// Reagent material must have the `[HORN]` token.
    #[df_token(token_name = "ANY_HORN_MATERIAL")]
    pub any_horn_material: Option<()>,
    /// Reagent material must have the `[LEATHER]` token.
    #[df_token(token_name = "ANY_LEATHER_MATERIAL")]
    pub any_leather_material: Option<()>,
    /// Reagent material must have the `[PEARL]` token.
    #[df_token(token_name = "ANY_PEARL_MATERIAL")]
    pub any_pearl_material: Option<()>,
    /// Reagent material must be subordinate to a `PLANT` object.
    #[df_token(token_name = "ANY_PLANT_MATERIAL")]
    pub any_plant_material: Option<()>,
    /// Reagent material must have the `[SHELL]` token.
    #[df_token(token_name = "ANY_SHELL_MATERIAL")]
    pub any_shell_material: Option<()>,
    /// Reagent material must have the `[SILK]` token.
    #[df_token(token_name = "ANY_SILK_MATERIAL")]
    pub any_silk_material: Option<()>,
    /// Reagent material must have the `[SOAP]` token.
    #[df_token(token_name = "ANY_SOAP_MATERIAL")]
    pub any_soap_material: Option<()>,
    /// Reagent is made of a tissue having `[TISSUE_SHAPE:STRANDS]`, intended for matching hair and
    /// wool. Must be used with `[USE_BODY_COMPONENT]`.
    #[df_token(token_name = "ANY_STRAND_TISSUE")]
    pub any_strand_tissue: Option<()>,
    /// Reagent material must have the `[TOOTH]` token.
    #[df_token(token_name = "ANY_TOOTH_MATERIAL")]
    pub any_tooth_material: Option<()>,
    /// Reagent material must have the `[YARN]` token.
    #[df_token(token_name = "ANY_YARN_MATERIAL")]
    pub any_yarn_material: Option<()>,
    /// Reagent material must have the `[SOIL_SAND]` token.
    #[df_token(token_name = "IS_SAND_MATERIAL")]
    pub is_sand_material: Option<()>,
    /// Reagent material must have the `[ITEMS_HARD]` token.
    #[df_token(token_name = "HARD_ITEM_MATERIAL")]
    pub hard_item_material: Option<()>,
    /// Reagent material must have the `[ITEMS_METAL]` token.
    #[df_token(token_name = "METAL_ITEM_MATERIAL")]
    pub metal_item_material: Option<()>,
    /// Reagent has to be a bag. Intended to be used with an item type of `BOX`, to prevent chests,
    /// coffers, and other containers from being used instead.
    #[df_token(token_name = "BAG")]
    pub bag: Option<()>,
    /// Reagent is able to be used to build structures (Stone, Wood, Blocks, Bars?).
    #[df_token(token_name = "BUILDMAT")]
    pub build_material: Option<()>,
    /// Reagent can be an Artifact. Using `[PRESERVE_REAGENT]` with this is strongly advised.
    #[df_token(token_name = "CAN_USE_ARTIFACT")]
    pub can_use_artifact: Option<()>,
    /// Allows the reagent to be an item that is otherwise reserved for use by a hospital.
    #[df_token(token_name = "CAN_USE_HOSPITAL_RESERVED")]
    pub can_use_hospital_reserved: Option<()>,
    /// Allows the reagent to be an item that is otherwise reserved for use by a location.
    #[df_token(token_name = "CAN_USE_LOCATION_RESERVED")]
    pub can_use_location_reserved: Option<()>,
    /// Reagent is a container that holds the specified reagent.
    #[df_token(token_name = "CONTAINS")]
    pub contains: Option<Reference>,
    /// Reagent must be a `BARREL` or `TOOL` which contains at least one item of type `LIQUID_MISC` made of `LYE`.
    /// Use of this token is discouraged, as it does not work with buckets (instead, use `[CONTAINS:lye]`
    /// and a corresponding lye reagent `[REAGENT:lye:150:LIQUID_MISC:NONE:LYE]`).
    #[df_token(token_name = "CONTAINS_LYE")]
    #[df_alias(token_name = "POTASHABLE", discouraged)]
    pub contains_lye: Option<Reference>,
    /// Reagent material must have `[ABSORPTION:0]`
    #[df_token(token_name = "DOES_NOT_ABSORB")]
    pub does_not_absorb: Option<()>,
    /// Performing a reaction with large stacks of inputs can allow multiple sets of outputs to be
    /// produced. Setting this flag causes the reagent to be ignored in this process -
    /// for example, with the reaction "1 plant + 1 barrel -> 5 alcohol (into barrel)",
    /// using this on the barrel allows the reaction to be performed as
    /// "5 plant + 1 barrel -> 25 alcohol" instead of "5 plant + 5 barrel -> 25 alcohol".
    #[df_token(token_name = "DOES_NOT_DETERMINE_PRODUCT_AMOUNT")]
    pub does_not_determine_product_amount: Option<()>,
    /// If the reagent is a container, it must be empty.
    #[df_token(token_name = "EMPTY")]
    pub empty: Option<()>,
    /// Reagent must be considered fire-safe (stable temperature below 11000 °U )
    /// - i.e. not wood, and not coal.
    #[df_token(token_name = "FIRE_BUILD_SAFE")]
    pub fire_build_safe: Option<()>,
    /// Reagent must be a barrel or any non-absorbing tool with `[TOOL_USE:FOOD_STORAGE]`
    #[df_token(token_name = "FOOD_STORAGE_CONTAINER")]
    pub food_storage_container: Option<()>,
    /// Reagent material has `[IS_GLASS]`.
    #[df_token(token_name = "GLASS_MATERIAL")]
    pub glass_material: Option<()>,
    /// Similar to `HAS_MATERIAL_REACTION_PRODUCT`, but requires the reagent's material to
    /// have a matching `ITEM_REACTION_PRODUCT` entry.
    #[df_token(token_name = "HAS_ITEM_REACTION_PRODUCT")]
    pub has_item_reaction_product: Option<Reference>, // TODO See REACTION_CLASS in MaterialDefinitionToken
    /// Similar to `REACTION_CLASS`, but requires the reagents material to have a matching
    /// `MATERIAL_REACTION_PRODUCT` entry. Intended for reactions which transform one class of
    /// material into another, such as skin->leather and fat->tallow.
    #[df_token(token_name = "HAS_MATERIAL_REACTION_PRODUCT")]
    pub has_material_reaction_product: Option<Reference>, // TODO See REACTION_CLASS in MaterialDefinitionToken
    /// Reagent must be a tool with the specific `TOOL_USE` value.
    /// The reagents item type must be `TOOL:NONE` for this to make any sense.
    #[df_token(token_name = "HAS_TOOL_USE")]
    /// Reference to `ITEM_TOOL` but the category like: `LIQUID_CONTAINER`.
    /// The item type must be `TOOL:NONE` for this to make any sense.
    pub has_tool_use: Option<Reference>, // TODO
    /// Reagent must contain writing.
    #[df_token(token_name = "HAS_WRITING_IMPROVEMENT")]
    pub has_writing_improvement: Option<()>,
    /// *Currently broken*
    ///
    /// Reagent must be considered magma-safe (stable temperature below 12000 °U ).
    #[df_token(token_name = "MAGMA_BUILD_SAFE")]
    pub magma_build_safe: Option<()>,
    /// Reagent material must be an ore of the specified metal.
    #[df_token(token_name = "METAL_ORE")]
    pub metal_ore: Option<Reference>, // TODO reference to Inorganic material
    /// Reagent's item dimension must be at least this large. The reagent's item type must be
    /// `BAR`, `POWDER_MISC`, `LIQUID_MISC`, `DRINK`, `THREAD`, `CLOTH`, or `GLOB` for this to work.
    #[df_token(token_name = "MIN_DIMENSION")]
    pub min_dimension: Option<u32>,
    /// Item must not have an edge, so must be blunt.
    /// Sharp stones (produced using knapping) and most types of weapon/ammo
    /// can not be used with this token.
    #[df_token(token_name = "NO_EDGE_ALLOWED")]
    pub no_edge_allowed: Option<()>,
    /// Reagent must be sharpened (used for carving).
    #[df_token(token_name = "HAS_EDGE")]
    pub has_edge: Option<()>,
    /// If the item is a container, it must not contain lye or milk.
    /// Not necessary if specifying `[EMPTY]`.
    #[df_token(token_name = "NOT_CONTAIN_BARREL_ITEM")]
    pub not_contain_barrel_item: Option<()>,
    /// Reagent can not be engraved. For example, a memorial slab can not be engraved.
    #[df_token(token_name = "NOT_ENGRAVED")]
    pub not_engraved: Option<()>,
    /// Reagent has not been decorated.
    #[df_token(token_name = "NOT_IMPROVED")]
    pub not_improved: Option<()>,
    /// Reagent must not be in the `SOLID_PRESSED` state.
    #[df_token(token_name = "NOT_PRESSED")]
    pub not_pressed: Option<()>,
    /// Reagent must be "collected" - used with `THREAD:NONE` to exclude webs.
    #[df_token(token_name = "NOT_WEB")]
    pub not_web: Option<()>,
    /// Reagent is not destroyed, which is the normal effect, at the completion of the reaction.
    /// Typically used for containers.
    #[df_token(token_name = "PRESERVE_REAGENT")]
    pub preserve_reagent: Option<()>,
    /// Requires the reagents material to have a matching `REACTION_CLASS` entry.
    /// Intended for reactions which accept a variety of materials but where the input material
    /// does not determine the output material, such as `FLUX` (for making pig iron and steel)
    /// and GYPSUM (for producing plaster powder).
    #[df_token(token_name = "REACTION_CLASS")]
    pub reaction_class: Option<Reference>, // TODO
    /// Reagent must not be rotten, mainly for organic materials.
    #[df_token(token_name = "UNROTTEN")]
    pub unrotten: Option<()>,
    /// Reagent material must come off a creature's body (`CORPSE` or `CORPSEPIECE`).
    #[df_token(token_name = "USE_BODY_COMPONENT")]
    pub use_body_component: Option<()>,
    /// Reagent must be "undisturbed" - used with `THREAD:NONE` to gather webs.
    #[df_token(token_name = "WEB_ONLY")]
    pub web_only: Option<()>,
    /// Reagent is made of an non-economic stone.
    #[df_token(token_name = "WORTHLESS_STONE_ONLY")]
    pub worthless_stone_only: Option<()>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct ProductToken {
    /// Defines a thing that comes out of the reaction. `GET_MATERIAL_FROM_REAGENT` and
    /// `GET_ITEM_DATA_FROM_REAGENT` can be used to defer the choice of material and/or item to
    /// the appropriate tag in a given reagent's material - the former comes in place of the
    /// material token, the latter replaces both the item and material tokens.
    /// Arguments are:
    ///    `<probability of success(0-100)>:<quantity>:<item token>:<item subtype>:<mat tokens>`
    /// `<mat tokens>` might consists of 3 arguments,
    /// for example: `GET_MATERIAL_FROM_REAGENT:A:TAN_MAT`
    /// Other times it has 2 extra arguments like: `INORGANIC:BRONZE_COATING`
    // Tarn comment:
    // > In the product, if you want to use the reagent's material itself,
    // > use NONE instead of a reaction product class (TAN_MAT in this example).
    // |---0---|-1-|2|-----------------3--------------------|---------------------4-------------------|
    // [PRODUCT:100:1:SKIN_TANNED:NONE                      :GET_MATERIAL_FROM_REAGENT:A     :TAN_MAT ]
    // [PRODUCT:100:1:BAR        :NONE                      :GET_MATERIAL_FROM_REAGENT:tallow:SOAP_MAT]
    // [PRODUCT:100:4:BAR        :NO_SUBTYPE                :METAL                    :STERLING_SILVER]
    // [PRODUCT:100:1:WEAPON     :ITEM_WEAPON_SPEAR_TRAINING:GET_MATERIAL_FROM_REAGENT:log   :NONE    ]
    #[df_token(token_name = "PRODUCT", on_duplicate_to_parent, primary_token)]
    pub reference: Option<(
        u8,  // probability_success
        u32, // quantity
        Choose<
            (ItemReferenceArg, MaterialTokenArgWithReagentMat), // item token, then material token
            (
                GetItemDataFromReagentEnum, // signals to use specific reagent's item and material
                ReferenceTo<ReagentToken>,  // signals which LOCALLY defined reagent you mean
                Choose<NoneEnum, Reference>, // TODO ref is to a MATERIAL_REACTION_PRODUCT reaction class
            ),
        >,
    )>,
    /// Product is given a sharp edge. Used for knapping.
    #[df_token(token_name = "FORCE_EDGE")]
    pub force_edge: Option<()>,
    /// Specifies the size of the product.
    /// A size of 150 is typical for `BAR`, `POWDER_MISC`, `LIQUID_MISC`, `DRINK`, and `GLOB`.
    /// A size of 15000 is typical for `THREAD`,
    /// and a size of 10000 is typical for `CLOTH`.
    #[df_token(token_name = "PRODUCT_DIMENSION")]
    pub product_dimension: Option<u32>,
    /// Product is created in the `SOLID_PASTE` state.
    #[df_token(token_name = "PRODUCT_PASTE")]
    pub product_paste: Option<()>,
    /// Product is created in the `SOLID_PRESSED` state.
    #[df_token(token_name = "PRODUCT_PRESSED")]
    pub product_pressed: Option<()>,
    /// Places the product in a container; `<id>` must be the name of a reagent with
    /// the `PRESERVE_REAGENT` token and a container item type.
    #[df_token(token_name = "PRODUCT_TO_CONTAINER")]
    pub product_to_container: Option<Reference>, // TODO refer to `[REAGENT:<This_value>:..]`
    /// Allows the product to be referred to by the given name, for the purpose of being passed
    /// down as argument in other tokens.
    #[df_token(token_name = "PRODUCT_TOKEN")]
    pub product_token: Option<Reference>, // TODO refer to `[REAGENT:<This_value>:..]`
    /// Transfers artifact status from the reagent to the product.
    #[df_token(token_name = "TRANSFER_ARTIFACT_STATUS")]
    pub transfer_artifact_status: Option<()>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct ReactionCategoryToken {
    /// Argument 1 of `[CATEGORY:...]`
    #[df_token(token_name = "CATEGORY", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// The name of the category as displayed in-game.
    #[df_token(token_name = "CATEGORY_NAME")]
    pub category_name: Option<String>,
    /// If present, when the category is highlighted in a building menu, this string will be
    /// displayed in the Helpful Hint box.
    #[df_token(token_name = "CATEGORY_DESCRIPTION")]
    pub category_description: Option<String>,
    /// If present, when the category is highlighted in a building menu, this string will be
    /// displayed in the Helpful Hint box.
    #[df_token(token_name = "CATEGORY_PARENT")]
    pub category_parent: Option<ReferenceTo<Self>>,
    /// If present, this category can be selected from its parent menu (whether a building or a
    /// parent category) using the given hotkey.
    #[df_token(token_name = "CATEGORY_KEY")]
    pub category_key: Option<Choose<KeyBindEnum, NoneEnum>>, // param is KeyBind
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum GetItemDataFromReagentEnum {
    #[df_token(token_name = "GET_ITEM_DATA_FROM_REAGENT")]
    GetItemDataFromReagent,
}
impl Default for GetItemDataFromReagentEnum {
    fn default() -> Self {
        Self::GetItemDataFromReagent
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum ImprovementTypeTokenArg {
    // TODO: token should be marked as deprecated/unused/ignored by the game, see #83
    // #[df_token(token_name = "ART_IMAGE")]
    // ArtImage,
    // #[df_token(token_name = "COVERED")]
    Covered,
    //#[df_token(token_name = "GLAZED")]
    Glazed,
    // #[df_token(token_name = "RINGS_HANGING")]
    RingsHanging,
    // #[df_token(token_name = "BANDS")]
    Bands,
    // #[df_token(token_name = "SPIKES")]
    Spikes,
    // #[df_token(token_name = "SPECIFIC")]
    // #[df_alias(token_name = "ITEMSPECIFIC", discouraged)]
    Specific(ItemSpecificEnum),
    // TODO: token should be marked as deprecated/unused/ignored by the game, see #83
    // #[df_token(token_name = "THREAD")]
    // Thread,
    // TODO: token should be marked as deprecated/unused/ignored by the game, see #83
    // #[df_token(token_name = "CLOTH")]
    // Cloth,
    // TODO: token should be marked as deprecated/unused/ignored by the game, see #83
    // #[df_token(token_name = "SEWN_IMAGE")]
    // SewnImage,
    // #[df_token(token_name = "PAGES")]
    Pages,
    // TODO: token should be marked as deprecated/unused/ignored by the game, see #83
    // #[df_token(token_name = "ILLUSTRATION")]
    // Illustration,
    // #[df_token(token_name = "INSTRUMENT_PIECE")]
    InstrumentPiece(Reference), // Ref to INSTRUMENT PIECE, distinct from ITEM_INSTRUMENT
    // #[df_token(token_name = "WRITING")]
    Writing,
    // TODO: token should be marked as deprecated/unused/ignored by the game, see #83
    // #[df_token(token_name = "IMAGE_SET")]
    // ImageSet,
}
impl Default for ImprovementTypeTokenArg {
    fn default() -> Self {
        Self::Covered
    }
}

// Deserialize a token with following pattern: `[REF:improvement_type_token_arg:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(ImprovementTypeTokenArg);

impl TryFromArgumentGroup for ImprovementTypeTokenArg {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Safe first argument for error case
        let arg0 = match token.get_current_arg() {
            Ok(arg) => Ok(arg.clone()),
            Err(err) => Err(err),
        };
        let reference_arg0 =
            Reference::try_from_argument_group(token, source, diagnostics, add_diagnostics_on_err)?;
        let bp_criteria = match reference_arg0.0.as_ref() {
            "COVERED" => ImprovementTypeTokenArg::Covered,
            "GLAZED" => ImprovementTypeTokenArg::Glazed,
            "RINGS_HANGING" => ImprovementTypeTokenArg::RingsHanging,
            "BANDS" => ImprovementTypeTokenArg::Bands,
            "SPIKES" => ImprovementTypeTokenArg::Spikes,
            // TODO: add alias warning
            "SPECIFIC" | "ITEMSPECIFIC" => {
                let specific = <ItemSpecificEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ImprovementTypeTokenArg::Specific(specific)
            }
            "PAGES" => ImprovementTypeTokenArg::Pages,
            "INSTRUMENT_PIECE" => {
                let instrument_piece = <Reference>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ImprovementTypeTokenArg::InstrumentPiece(instrument_piece)
            }
            "WRITING" => ImprovementTypeTokenArg::Writing,
            _ => {
                Self::diagnostics_wrong_enum_type(
                    &arg0?,
                    vec![
                        "COVERED",
                        "GLAZED",
                        "RINGS_HANGING",
                        "BANDS",
                        "SPIKES",
                        "SPECIFIC",
                        "ITEMSPECIFIC",
                        "PAGES",
                        "INSTRUMENT_PIECE",
                        "WRITING",
                    ],
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                );
                return Err(());
            }
        };
        Ok(bp_criteria)
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum ReactionDescriptionUseItemTokenArg {
    // TODO find out if all item types can actually be used
    UseAmmo(ReferenceTo<AmmoToken>),
    UseArmor(ReferenceTo<ArmorToken>),
    UseFood(ReferenceTo<FoodToken>),
    UseGloves(ReferenceTo<GlovesToken>),
    UseHelm(ReferenceTo<HelmToken>),
    UseInstrument(ReferenceTo<InstrumentToken>),
    UsePants(ReferenceTo<PantsToken>),
    UseShield(ReferenceTo<ShieldToken>),
    UseShoes(ReferenceTo<ShoesToken>),
    UseSiegeAmmo(ReferenceTo<SiegeAmmoToken>),
    UseTool(ReferenceTo<ToolToken>),
    UseToy(ReferenceTo<ToyToken>),
    UseTrapComp(ReferenceTo<TrapCompToken>),
    UseWeapon(ReferenceTo<WeaponToken>),
}
impl Default for ReactionDescriptionUseItemTokenArg {
    fn default() -> Self {
        Self::UseTool(ReferenceTo::new(String::default()))
    }
}

// Deserialize a token with following pattern: `[REF:reaction_description_use_item:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(ReactionDescriptionUseItemTokenArg);

impl TryFromArgumentGroup for ReactionDescriptionUseItemTokenArg {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Safe first argument for error case
        let arg0 = match token.get_current_arg() {
            Ok(arg) => Ok(arg.clone()),
            Err(err) => Err(err),
        };
        let reference_arg0 =
            Reference::try_from_argument_group(token, source, diagnostics, add_diagnostics_on_err)?;
        let bp_criteria = match reference_arg0.0.as_ref() {
            "USE_AMMO" => {
                let item_ref = <ReferenceTo<AmmoToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UseAmmo(item_ref)
            }
            "USE_ARMOR" => {
                let item_ref = <ReferenceTo<ArmorToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UseArmor(item_ref)
            }
            "USE_FOOD" => {
                let item_ref = <ReferenceTo<FoodToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UseFood(item_ref)
            }
            "USE_GLOVES" => {
                let item_ref = <ReferenceTo<GlovesToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UseGloves(item_ref)
            }
            "USE_HELM" => {
                let item_ref = <ReferenceTo<HelmToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UseHelm(item_ref)
            }
            "USE_INSTRUMENT" => {
                let item_ref = <ReferenceTo<InstrumentToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UseInstrument(item_ref)
            }
            "USE_PANTS" => {
                let item_ref = <ReferenceTo<PantsToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UsePants(item_ref)
            }
            "USE_SHIELD" => {
                let item_ref = <ReferenceTo<ShieldToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UseShield(item_ref)
            }
            "USE_SHOES" => {
                let item_ref = <ReferenceTo<ShoesToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UseShoes(item_ref)
            }
            "USE_SIEGEAMMO" => {
                let item_ref = <ReferenceTo<SiegeAmmoToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UseSiegeAmmo(item_ref)
            }
            "USE_TOOL" => {
                let item_ref = <ReferenceTo<ToolToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UseTool(item_ref)
            }
            "USE_TRAPCOMP" => {
                let item_ref = <ReferenceTo<TrapCompToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UseTrapComp(item_ref)
            }
            "USE_WEAPON" => {
                let item_ref = <ReferenceTo<WeaponToken>>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ReactionDescriptionUseItemTokenArg::UseWeapon(item_ref)
            }
            _ => {
                Self::diagnostics_wrong_enum_type(
                    &arg0?,
                    vec!["USE_INSTRUMENT", "USE_TOOL"],
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                );
                return Err(());
            }
        };
        Ok(bp_criteria)
    }
}
