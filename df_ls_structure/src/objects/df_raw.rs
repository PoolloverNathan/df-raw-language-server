use super::*;
use df_ls_core::DFRawFile;
use df_ls_diagnostics::hash_map;
use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

pub type DFRaw = DFRawFile<Vec<ObjectToken>>;

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
#[df_token(token_name = "OBJECT", second_par_check)]
pub struct ObjectToken {
    #[df_token(token_name = "BODY")]
    pub body_tokens: Vec<BodyObjectToken>,
    #[df_token(token_name = "BODY_DETAIL_PLAN")]
    pub body_detail_plan_tokens: Vec<BodyDetailPlanToken>,
    #[df_token(token_name = "BUILDING")]
    pub building_tokens: Vec<BuildingToken>,
    #[df_token(token_name = "CREATURE")]
    pub creature_tokens: Vec<CreatureToken>,
    #[df_token(token_name = "CREATURE_VARIATION")]
    pub creature_variation_tokens: Vec<CreatureVariationToken>,
    #[df_token(token_name = "DESCRIPTOR_COLOR")]
    pub color_tokens: Vec<ColorToken>,
    #[df_token(token_name = "DESCRIPTOR_PATTERN")]
    pub pattern_tokens: Vec<PatternToken>,
    #[df_token(token_name = "DESCRIPTOR_SHAPE")]
    pub shape_tokens: Vec<ShapeToken>,
    #[df_token(token_name = "ENTITY")]
    pub entity_tokens: Vec<EntityToken>,
    #[df_token(token_name = "GRAPHICS")]
    pub graphics_tokens: Vec<GraphicsToken>,
    #[df_token(token_name = "INTERACTION")]
    pub interaction_tokens: Vec<InteractionToken>,
    #[df_token(token_name = "INORGANIC")]
    pub inorganic_tokens: Vec<InorganicToken>,
    #[df_token(token_name = "ITEM")]
    pub item_tokens: Vec<ItemToken>,
    #[df_token(token_name = "LANGUAGE")]
    pub language_tokens: Vec<LanguageToken>,
    #[df_token(token_name = "MATERIAL_TEMPLATE")]
    pub material_tokens: Vec<MaterialToken>,
    #[df_token(token_name = "PLANT")]
    pub plant_tokens: Vec<PlantToken>,
    #[df_token(token_name = "REACTION")]
    pub reaction_tokens: Vec<ReactionToken>,
    #[df_token(token_name = "TISSUE_TEMPLATE")]
    pub tissue_template_tokens: Vec<TissueToken>,
}
