use df_ls_core::{Reference, ReferenceTo};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_syntax_analysis::{Token, TokenDeserialize, TryFromArgumentGroup};
use serde::{Deserialize, Serialize};

use crate::{CreatureToken, PlantToken};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum SingularOrPluralEnum {
    #[df_token(token_name = "SINGULAR")]
    Singular,
    #[df_token(token_name = "PLURAL")]
    Plural,
}
impl Default for SingularOrPluralEnum {
    fn default() -> Self {
        Self::Singular
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum AlertOrPeacefulIntermittentEnum {
    #[df_token(token_name = "ALERT")]
    Alert,
    #[df_token(token_name = "PEACEFUL_INTERMITTENT")]
    PeacefulIntermittent,
}
impl Default for AlertOrPeacefulIntermittentEnum {
    fn default() -> Self {
        Self::Alert
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum VocalizationEnum {
    #[df_token(token_name = "VOCALIZATION")]
    Vocalization,
}
impl Default for VocalizationEnum {
    fn default() -> Self {
        Self::Vocalization
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum TissueModifierEnum {
    #[df_token(token_name = "LENGTH")]
    Length,
    #[df_token(token_name = "DENSE")]
    Dense,
    #[df_token(token_name = "HIGH_POSITION")]
    HighPosition,
    #[df_token(token_name = "CURLY")]
    Curly,
    #[df_token(token_name = "GREASY")]
    Greasy,
    #[df_token(token_name = "WRINKLY")]
    Wrinkly,
}
impl Default for TissueModifierEnum {
    fn default() -> Self {
        Self::Length
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum AppGeneticModelEnum {
    #[df_token(token_name = "DOMINANT_MORE")]
    DominantMore,
    #[df_token(token_name = "DOMINANT_LESS")]
    DominantLess,
    #[df_token(token_name = "MIX")]
    Mix,
}
impl Default for AppGeneticModelEnum {
    fn default() -> Self {
        Self::DominantMore
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum SecretionTriggerEnum {
    /// Secretion occurs once every 40 ticks in fortress mode, and every tick in adventurer mode.
    #[df_token(token_name = "CONTINUOUS")]
    Continuous,
    /// Secretion occurs continuously (every 40 ticks in fortress mode, and every tick in adventurer
    /// mode) whilst the creature is at minimum `Tired` following physical exertion. Note that this
    /// cannot occur if the creature has `[NOEXERT]`.
    #[df_token(token_name = "EXERTION")]
    Exertion,
    /// Secretion occurs continuously (every 40 ticks in fortress mode, and every tick in adventurer
    /// mode) whilst the creature is distressed. Cannot occur in creatures with `[NOEMOTION]`.
    #[df_token(token_name = "EXTREME_EMOTION")]
    ExtremeEmotion,
}
impl Default for SecretionTriggerEnum {
    fn default() -> Self {
        Self::Continuous
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum LairCharacteristicEnum {
    #[df_token(token_name = "HAS_DOORS")]
    HasDoors,
}
impl Default for LairCharacteristicEnum {
    fn default() -> Self {
        Self::HasDoors
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum LairTypeEnum {
    #[df_token(token_name = "SIMPLE_BURROW")]
    SimpleBurrow,
    #[df_token(token_name = "SIMPLE_MOUND")]
    SimpleMound,
    #[df_token(token_name = "WILDERNESS_LOCATION")]
    WildernessLocation,
    #[df_token(token_name = "SHRINE")]
    Shrine,
    #[df_token(token_name = "LABYRINTH")]
    Labyrinth,
}
impl Default for LairTypeEnum {
    fn default() -> Self {
        Self::SimpleBurrow
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum TestAllEnum {
    #[df_token(token_name = "TEST_ALL")]
    TestAll,
}
impl Default for TestAllEnum {
    fn default() -> Self {
        Self::TestAll
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum HabitTypeEnum {
    #[df_token(token_name = "COLLECT_TROPHIES")]
    CollectTrophies,
    #[df_token(token_name = "COOK_PEOPLE")]
    CookPeople,
    #[df_token(token_name = "COOK_VERMIN")]
    CookVermin,
    #[df_token(token_name = "GRIND_VERMIN")]
    GrindVermin,
    #[df_token(token_name = "COOK_BLOOD")]
    CookBlood,
    #[df_token(token_name = "GRIND_BONE_MEAL")]
    GrindBoneMeal,
    #[df_token(token_name = "EAT_BONE_PORRIDGE")]
    EatBonePorridge,
    #[df_token(token_name = "USE_ANY_MELEE_WEAPON")]
    UseAnyMeleeWeapon,
    #[df_token(token_name = "GIANT_NEST")]
    GiantNest,
    #[df_token(token_name = "COLLECT_WEALTH")]
    CollectWealth,
}
impl Default for HabitTypeEnum {
    fn default() -> Self {
        Self::CollectTrophies
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum AnyHardStoneEnum {
    #[df_token(token_name = "ANY_HARD_STONE")]
    AnyHardStone,
}
impl Default for AnyHardStoneEnum {
    fn default() -> Self {
        Self::AnyHardStone
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum RootEnum {
    #[df_token(token_name = "ROOT")]
    Root,
}
impl Default for RootEnum {
    fn default() -> Self {
        Self::Root
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum TimescaleEnum {
    #[df_token(token_name = "DAILY")]
    Daily,
    #[df_token(token_name = "YEARLY")]
    Yearly,
}
impl Default for TimescaleEnum {
    fn default() -> Self {
        Self::Daily
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum NoEndEnum {
    #[df_token(token_name = "NO_END")]
    NoEnd,
}
impl Default for NoEndEnum {
    fn default() -> Self {
        Self::NoEnd
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum NormalEnum {
    /// _"Normal just skips the same was as none does. Could be an old compatibility thing? No idea."_
    ///
    /// -- [Toady](http://www.bay12forums.com/smf/index.php?topic=169696.msg8292042#msg8292042)
    #[df_token(token_name = "NORMAL")]
    Normal,
}
impl Default for NormalEnum {
    fn default() -> Self {
        Self::Normal
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum PlantOrCreatureTokenArg {
    Plant(ReferenceTo<PlantToken>),
    Creature(ReferenceTo<CreatureToken>),
}
impl Default for PlantOrCreatureTokenArg {
    fn default() -> Self {
        Self::Plant(ReferenceTo::new(String::default()))
    }
}

// Deserialize a token with following pattern: `[REF:token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(PlantOrCreatureTokenArg);

impl TryFromArgumentGroup for PlantOrCreatureTokenArg {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Safe first argument (is not token_name) for error case
        let arg0 = match token.get_current_arg() {
            Ok(arg) => Ok(arg.clone()),
            Err(err) => Err(err),
        };
        let reference_arg0 =
            Reference::try_from_argument_group(token, source, diagnostics, add_diagnostics_on_err)?;
        let plant_or_creature = match reference_arg0.0.as_ref() {
            "PLANT" => {
                let plant_reference = ReferenceTo::<PlantToken>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                PlantOrCreatureTokenArg::Plant(plant_reference)
            }
            "CREATURE" => {
                let creature_reference = ReferenceTo::<CreatureToken>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                PlantOrCreatureTokenArg::Creature(creature_reference)
            }
            _ => {
                Self::diagnostics_wrong_enum_type(
                    &arg0?,
                    vec!["PLANT", "CREATURE"],
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                );
                return Err(());
            }
        };
        Ok(plant_or_creature)
    }
}
