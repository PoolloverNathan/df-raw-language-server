use df_ls_core::{Choose, Clamp, DFChar, Reference, ReferenceTo, Referenceable};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_syntax_analysis::{Token, TokenDeserialize, TryFromArgumentGroup};
use serde::{Deserialize, Serialize};

use crate::SkillEnum;

#[allow(clippy::large_enum_variant)]
#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
pub enum ItemToken {
    #[df_token(token_name = "ITEM_AMMO")]
    AmmoToken(AmmoToken),
    #[df_token(token_name = "ITEM_ARMOR")]
    ArmorToken(ArmorToken),
    #[df_token(token_name = "ITEM_FOOD")]
    FoodToken(FoodToken),
    #[df_token(token_name = "ITEM_GLOVES")]
    GlovesToken(GlovesToken),
    #[df_token(token_name = "ITEM_HELM")]
    HelmToken(HelmToken),
    #[df_token(token_name = "ITEM_INSTRUMENT")]
    InstrumentToken(InstrumentToken),
    #[df_token(token_name = "ITEM_PANTS")]
    PantsToken(PantsToken),
    #[df_token(token_name = "ITEM_SHIELD")]
    ShieldToken(ShieldToken),
    #[df_token(token_name = "ITEM_SHOES")]
    ShoesToken(ShoesToken),
    #[df_token(token_name = "ITEM_SIEGEAMMO")]
    SiegeAmmoToken(SiegeAmmoToken),
    #[df_token(token_name = "ITEM_TOOL")]
    ToolToken(ToolToken),
    #[df_token(token_name = "ITEM_TOY")]
    ToyToken(ToyToken),
    #[df_token(token_name = "ITEM_TRAPCOMP")]
    TrapCompToken(TrapCompToken),
    #[df_token(token_name = "ITEM_WEAPON")]
    WeaponToken(WeaponToken),
}
impl Default for ItemToken {
    fn default() -> Self {
        Self::AmmoToken(AmmoToken::default())
    }
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct AmmoToken {
    /// Argument 1 of `[ITEM_AMMO:...]`
    #[df_token(token_name = "ITEM_AMMO", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// What this item will be called in-game.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>,
    /// This ammo can be fired from a weapon that is set to fire the same ammo type.
    /// Defaults to `BOLT`.
    #[df_token(token_name = "CLASS")]
    pub class: Option<Reference>,
    /// How large the ammunition is.
    #[df_token(token_name = "SIZE")] // Required token
    pub size: Option<u32>,
    /// The attack used by this ammo when used as a melee weapon.
    #[df_token(token_name = "ATTACK")]
    pub attack: Option<ItemAttack>, // TODO: test ingame if WeaponAttack could be used instead here
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct ArmorToken {
    /// Argument 1 of `[ITEM_ARMOR:...]`
    #[df_token(token_name = "ITEM_ARMOR", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// Length of the sleeves, counted in `[LIMB]` body parts towards the hands. A value of 0 only
    /// protects both halves of the torso, 1 extends over the upper arms and so on. Regardless of
    /// the value, body armor can never extend to cover the hands or head.
    #[df_token(token_name = "UBSTEP")]
    #[df_issue(
        since = "0.31.03",
        link = "http://www.bay12games.com/dwarves/mantisbt/view.php?id=1821",
        note = "High values of `UBSTEP` will result in the item protecting \
                facial features, fingers, and toes, while leaving those parts that it cannot protect \
                unprotected (but still counting them as steps).",
        severity = "WARN"
    )]
    pub ubstep: Option<Choose<u8, MaxEnum>>,
    // region: Shared by garments/shields/trapcomp/weapons; are all required ======================
    /// What this item will be called in-game.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>,
    /// How much material is needed to make the item. Most important with bars. The number of bars
    /// required to make the item is the value divided by three.
    #[df_token(token_name = "MATERIAL_SIZE")]
    pub material_size: Option<u32>,
    // endregion ==================================================================================
    // region: Shared by all garments =============================================================
    /// Adjective preceding the material name (e.g. "large copper dagger").
    #[df_token(token_name = "ADJECTIVE")]
    pub adjective: Option<String>,
    /// Metal versions of this item count as one `ARMORLEVEL` higher and thus won't be worn by
    /// random peasants. This tag will not work unless `ARMORLEVEL` is explicitly declared: if you
    /// leave out `ARMORLEVEL`, even metal armor will default to level 0.
    #[df_token(token_name = "METAL_ARMOR_LEVELS")]
    pub metal_armor_levels: Option<()>,
    /// Metal versions of this item will have "chain" added between the material and item name.
    #[df_token(token_name = "CHAIN_METAL_TEXT")]
    pub chain_metal_text: Option<()>,
    /// Clothiers can make this item from all kinds of cloth. If paired with `[LEATHER]`, the item
    /// has an equal chance of being either in randomly generated outfits. Further uses of this tag
    /// are unknown.
    #[df_token(token_name = "SOFT")]
    pub soft: Option<()>,
    /// Default state in the absence of a `[SOFT]` token. Actual effects unknown.
    #[df_token(token_name = "HARD")]
    pub hard: Option<()>,
    /// Item can be made from metal. Overrides `[SOFT]` and `[LEATHER]` in randomly generated
    /// outfits, if the `ARMORLEVEL` permits. Civilizations with `[WOOD_ARMOR]` will make this
    /// item out of wood instead.
    #[df_token(token_name = "METAL")]
    pub metal: Option<()>,
    /// Craftsmen can make this item from bones. Randomly generated outfits don't include bone
    /// armor.
    #[df_token(token_name = "BARRED")]
    pub barred: Option<()>,
    /// Craftsmen can make this item from shells. Randomly generated outfits don't include shell
    /// armor.
    #[df_token(token_name = "SCALED")]
    pub scaled: Option<()>,
    /// Leatherworkers can make this item from leather. If paired with `[SOFT]`, this item has an
    /// equal chance of being either in randomly generated outfits.
    #[df_token(token_name = "LEATHER")]
    pub leather: Option<()>,
    /// Only one shaped piece of clothing can be worn on a single body slot at a time.
    #[df_token(token_name = "SHAPED")]
    pub shaped: Option<()>,
    /// Increases the `*_STRAIN_AT_YIELD` properties of the armor's material to 50000, if lower.
    /// This makes the garment flex and give way instead of shattering under force. Strong materials
    /// that resist cutting will blunt edged attacks into bone-crushing hits instead.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_CHAIN_ALL")]
    pub structural_elasticity_chain_all: Option<()>,
    /// Increases the `*_STRAIN_AT_YIELD` properties of the armor's material to 50000, but only if
    /// the garment is made from metal.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_CHAIN_METAL")]
    pub structural_elasticity_chain_metal: Option<()>,
    /// Reduces the armor material's `SHEAR_YIELD` to 20000, `SHEAR_FRACTURE` to 30000 and increases
    /// the `*_STRAIN_AT_YIELD` properties to 50000, but only if the garment is made from cloth.
    /// This makes the item very weak against edged attacks, even if the thread material is
    /// normally very strong.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_WOVEN_THREAD")]
    pub structural_elasticity_woven_thread: Option<()>,
    /// The item's bulkiness when worn. Aside from the layer limitations, it's a big contributor to
    /// the thickness and weight (and therefore price) of the garment. See
    /// [Armor](https://dwarffortresswiki.org/index.php/Armor) for more on item sizes and
    /// layering. Defaults to 10.
    #[df_token(token_name = "LAYER_SIZE")]
    pub layer_size: Option<u32>,
    /// The maximum amount of garments that can fit underneath this garment. See
    /// [Armor](https://dwarffortresswiki.org/index.php/Armor) for more on item sizes and
    /// layering. Defaults to 10.
    #[df_token(token_name = "LAYER_PERMIT")]
    pub layer_permit: Option<u32>,
    /// Where the item goes in relation to other clothes. Socks cannot be worn on top of boots!
    ///
    /// The `LAYER_PERMIT` of the highest layer is used on a given section of the body - you can fit
    /// a lot of shirts and other undergarments underneath a robe, but not if you wear a leather
    /// jerkin on top of it, and you can still wear a cloak over the whole ensemble. Defaults to
    /// `UNDER`.
    #[df_token(token_name = "LAYER")]
    pub layer: Option<LayerEnum>,
    /// How often the garment gets in the way of a contaminant or an attack. Armor with a 5%
    /// coverage value, for example, will be near useless because 95% of attacks will bypass it
    /// completely. Temperature effects and armor thickness are also influenced. Defaults to 100.
    #[df_token(token_name = "COVERAGE")]
    pub coverage: Option<u8>,
    /// The garment's general purpose. Defaults to 1 for shields, 0 for everything else. Class 0
    /// items are claimed and used by civilians as ordinary clothing and are subject to wear.
    #[df_token(token_name = "ARMORLEVEL")]
    pub armorlevel: Option<u8>, // shared by all garments, and shields
    // endregion ==================================================================================
    // region: Shared by ARMOR and PANTS ==========================================================
    /// Changes the plural form of this item to "`phrase of` item". Primarily pertains to the stock
    /// screens.
    ///
    /// Example, "suits of" platemail, "pairs of" trousers, etc.
    #[df_token(token_name = "PREPLURAL")]
    pub preplural: Option<String>,
    /// If the item has no material associated with it (e.g. stockpile menus and trade
    /// negotiations), this will be displayed in its place. Used for leather armor in vanilla.
    #[df_token(token_name = "MATERIAL_PLACEHOLDER")]
    pub material_placeholder: Option<String>,
    /// Length of the legs/hem, counted in `[LIMB]` body parts towards the feet. A value of 0 only
    /// covers the lower body, 1 extends over the upper legs and so on. Regardless of the value,
    /// body armor or pants can never extend to cover the feet.
    #[df_token(token_name = "LBSTEP")]
    pub lbstep: Option<Choose<u8, MaxEnum>>,
    // endregion ==================================================================================
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct FoodToken {
    /// Argument 1 of `[ITEM_FOOD:...]`
    #[df_token(token_name = "ITEM_FOOD", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// What this item will be called in-game.
    #[df_token(token_name = "NAME")]
    pub name: Option<String>,
    /// Specifies the number of ingredients that are used in this type of prepared meal:
    /// - 2 for Easy. (default)
    /// - 3 for Fine.
    /// - 4 for Lavish.
    #[df_token(token_name = "LEVEL")]
    pub level: Option<Clamp<u8, 2, 4>>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct GlovesToken {
    /// Argument 1 of `[ITEM_GLOVES:...]`
    #[df_token(token_name = "ITEM_GLOVES", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// Length of gloves or footwear, counted in `[LIMB]` body parts towards the torso. A value of 1
    /// lets gloves cover the lower arms, a value of 2 stretches a boot all the way over the upper
    /// leg and so on.
    ///
    /// Regardless of the value, none of these items can ever extend to cover the upper or lower
    /// body. Shields also have this token, but it only seems to affect weight.
    #[df_token(token_name = "UPSTEP")]
    pub upstep: Option<Choose<u8, MaxEnum>>, // shared by glove, shield, shoes
    // region: Shared by garments/shields/trapcomp/weapons; are all required ======================
    /// What this item will be called in-game.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>,
    /// How much material is needed to make the item. Most important with bars. The number of bars
    /// required to make the item is the value divided by three.
    #[df_token(token_name = "MATERIAL_SIZE")]
    pub material_size: Option<u32>,
    // endregion ==================================================================================
    // region: Shared by all garments =============================================================
    /// Adjective preceding the material name (e.g. "large copper dagger").
    #[df_token(token_name = "ADJECTIVE")]
    pub adjective: Option<String>,
    /// Metal versions of this item count as one `ARMORLEVEL` higher and thus won't be worn by
    /// random peasants. This tag will not work unless `ARMORLEVEL` is explicitly declared: if you
    /// leave out `ARMORLEVEL`, even metal armor will default to level 0.
    #[df_token(token_name = "METAL_ARMOR_LEVELS")]
    pub metal_armor_levels: Option<()>,
    /// Metal versions of this item will have "chain" added between the material and item name.
    #[df_token(token_name = "CHAIN_METAL_TEXT")]
    pub chain_metal_text: Option<()>,
    /// Clothiers can make this item from all kinds of cloth. If paired with `[LEATHER]`, the item
    /// has an equal chance of being either in randomly generated outfits. Further uses of this tag
    /// are unknown.
    #[df_token(token_name = "SOFT")]
    pub soft: Option<()>,
    /// Default state in the absence of a `[SOFT]` token. Actual effects unknown.
    #[df_token(token_name = "HARD")]
    pub hard: Option<()>,
    /// Item can be made from metal. Overrides `[SOFT]` and `[LEATHER]` in randomly generated
    /// outfits, if the `ARMORLEVEL` permits. Civilizations with `[WOOD_ARMOR]` will make this
    /// item out of wood instead.
    #[df_token(token_name = "METAL")]
    pub metal: Option<()>,
    /// Craftsmen can make this item from bones. Randomly generated outfits don't include bone
    /// armor.
    #[df_token(token_name = "BARRED")]
    pub barred: Option<()>,
    /// Craftsmen can make this item from shells. Randomly generated outfits don't include shell
    /// armor.
    #[df_token(token_name = "SCALED")]
    pub scaled: Option<()>,
    /// Leatherworkers can make this item from leather. If paired with `[SOFT]`, this item has an
    /// equal chance of being either in randomly generated outfits.
    #[df_token(token_name = "LEATHER")]
    pub leather: Option<()>,
    /// Only one shaped piece of clothing can be worn on a single body slot at a time.
    #[df_token(token_name = "SHAPED")]
    pub shaped: Option<()>,
    /// Increases the `*_STRAIN_AT_YIELD` properties of the armor's material to 50000, if lower.
    /// This makes the garment flex and give way instead of shattering under force. Strong materials
    /// that resist cutting will blunt edged attacks into bone-crushing hits instead.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_CHAIN_ALL")]
    pub structural_elasticity_chain_all: Option<()>,
    /// Increases the `*_STRAIN_AT_YIELD` properties of the armor's material to 50000, but only if
    /// the garment is made from metal.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_CHAIN_METAL")]
    pub structural_elasticity_chain_metal: Option<()>,
    /// Reduces the armor material's `SHEAR_YIELD` to 20000, `SHEAR_FRACTURE` to 30000 and increases
    /// the `*_STRAIN_AT_YIELD` properties to 50000, but only if the garment is made from cloth.
    /// This makes the item very weak against edged attacks, even if the thread material is
    /// normally very strong.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_WOVEN_THREAD")]
    pub structural_elasticity_woven_thread: Option<()>,
    /// The item's bulkiness when worn. Aside from the layer limitations, it's a big contributor to
    /// the thickness and weight (and therefore price) of the garment. See
    /// [Armor](https://dwarffortresswiki.org/index.php/Armor) for more on item sizes and
    /// layering. Defaults to 10.
    #[df_token(token_name = "LAYER_SIZE")]
    pub layer_size: Option<u32>,
    /// The maximum amount of garments that can fit underneath this garment. See
    /// [Armor](https://dwarffortresswiki.org/index.php/Armor) for more on item sizes and
    /// layering. Defaults to 10.
    #[df_token(token_name = "LAYER_PERMIT")]
    pub layer_permit: Option<u32>,
    /// Where the item goes in relation to other clothes. Socks cannot be worn on top of boots!
    ///
    /// The `LAYER_PERMIT` of the highest layer is used on a given section of the body - you can fit
    /// a lot of shirts and other undergarments underneath a robe, but not if you wear a leather
    /// jerkin on top of it, and you can still wear a cloak over the whole ensemble. Defaults to
    /// `UNDER`.
    #[df_token(token_name = "LAYER")]
    pub layer: Option<LayerEnum>,
    /// How often the garment gets in the way of a contaminant or an attack. Armor with a 5%
    /// coverage value, for example, will be near useless because 95% of attacks will bypass it
    /// completely. Temperature effects and armor thickness are also influenced. Defaults to 100.
    #[df_token(token_name = "COVERAGE")]
    pub coverage: Option<u8>,
    /// The garment's general purpose. Defaults to 1 for shields, 0 for everything else. Class 0
    /// items are claimed and used by civilians as ordinary clothing and are subject to wear.
    #[df_token(token_name = "ARMORLEVEL")] // shared by all garments, and shields
    pub armorlevel: Option<u8>,
    // endregion ==================================================================================
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct HelmToken {
    /// Argument 1 of `[ITEM_HELM:...]`
    #[df_token(token_name = "ITEM_HELM", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    // region: Shared by garments/shields/trapcomp/weapons; are all required ======================
    /// What this item will be called in-game.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>,
    /// How much material is needed to make the item. Most important with bars. The number of bars
    /// required to make the item is the value divided by three.
    #[df_token(token_name = "MATERIAL_SIZE")]
    pub material_size: Option<u32>,
    // endregion ==================================================================================
    // region: Shared by all garments =============================================================
    /// Adjective preceding the material name (e.g. "large copper dagger").
    #[df_token(token_name = "ADJECTIVE")]
    pub adjective: Option<String>,
    /// Metal versions of this item count as one `ARMORLEVEL` higher and thus won't be worn by
    /// random peasants. This tag will not work unless `ARMORLEVEL` is explicitly declared: if you
    /// leave out `ARMORLEVEL`, even metal armor will default to level 0.
    #[df_token(token_name = "METAL_ARMOR_LEVELS")]
    pub metal_armor_levels: Option<()>,
    /// Metal versions of this item will have "chain" added between the material and item name.
    #[df_token(token_name = "CHAIN_METAL_TEXT")]
    pub chain_metal_text: Option<()>,
    /// Clothiers can make this item from all kinds of cloth. If paired with `[LEATHER]`, the item
    /// has an equal chance of being either in randomly generated outfits. Further uses of this tag
    /// are unknown.
    #[df_token(token_name = "SOFT")]
    pub soft: Option<()>,
    /// Default state in the absence of a `[SOFT]` token. Actual effects unknown.
    #[df_token(token_name = "HARD")]
    pub hard: Option<()>,
    /// Item can be made from metal. Overrides `[SOFT]` and `[LEATHER]` in randomly generated
    /// outfits, if the `ARMORLEVEL` permits. Civilizations with `[WOOD_ARMOR]` will make this
    /// item out of wood instead.
    #[df_token(token_name = "METAL")]
    pub metal: Option<()>,
    /// Craftsmen can make this item from bones. Randomly generated outfits don't include bone
    /// armor.
    #[df_token(token_name = "BARRED")]
    pub barred: Option<()>,
    /// Craftsmen can make this item from shells. Randomly generated outfits don't include shell
    /// armor.
    #[df_token(token_name = "SCALED")]
    pub scaled: Option<()>,
    /// Leatherworkers can make this item from leather. If paired with `[SOFT]`, this item has an
    /// equal chance of being either in randomly generated outfits.
    #[df_token(token_name = "LEATHER")]
    pub leather: Option<()>,
    /// Only one shaped piece of clothing can be worn on a single body slot at a time.
    #[df_token(token_name = "SHAPED")]
    pub shaped: Option<()>,
    /// Increases the `*_STRAIN_AT_YIELD` properties of the armor's material to 50000, if lower.
    /// This makes the garment flex and give way instead of shattering under force. Strong materials
    /// that resist cutting will blunt edged attacks into bone-crushing hits instead.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_CHAIN_ALL")]
    pub structural_elasticity_chain_all: Option<()>,
    /// Increases the `*_STRAIN_AT_YIELD` properties of the armor's material to 50000, but only if
    /// the garment is made from metal.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_CHAIN_METAL")]
    pub structural_elasticity_chain_metal: Option<()>,
    /// Reduces the armor material's `SHEAR_YIELD` to 20000, `SHEAR_FRACTURE` to 30000 and increases
    /// the `*_STRAIN_AT_YIELD` properties to 50000, but only if the garment is made from cloth.
    /// This makes the item very weak against edged attacks, even if the thread material is
    /// normally very strong.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_WOVEN_THREAD")]
    pub structural_elasticity_woven_thread: Option<()>,
    /// The item's bulkiness when worn. Aside from the layer limitations, it's a big contributor to
    /// the thickness and weight (and therefore price) of the garment. See
    /// [Armor](https://dwarffortresswiki.org/index.php/Armor) for more on item sizes and
    /// layering. Defaults to 10.
    #[df_token(token_name = "LAYER_SIZE")]
    pub layer_size: Option<u32>,
    /// The maximum amount of garments that can fit underneath this garment. See
    /// [Armor](https://dwarffortresswiki.org/index.php/Armor) for more on item sizes and
    /// layering. Defaults to 10.
    #[df_token(token_name = "LAYER_PERMIT")]
    pub layer_permit: Option<u32>,
    /// Where the item goes in relation to other clothes. Socks cannot be worn on top of boots!
    ///
    /// The `LAYER_PERMIT` of the highest layer is used on a given section of the body - you can fit
    /// a lot of shirts and other undergarments underneath a robe, but not if you wear a leather
    /// jerkin on top of it, and you can still wear a cloak over the whole ensemble. Defaults to
    /// `UNDER`.
    #[df_token(token_name = "LAYER")]
    pub layer: Option<LayerEnum>,
    /// How often the garment gets in the way of a contaminant or an attack. Armor with a 5%
    /// coverage value, for example, will be near useless because 95% of attacks will bypass it
    /// completely. Temperature effects and armor thickness are also influenced. Defaults to 100.
    #[df_token(token_name = "COVERAGE")]
    pub coverage: Option<u8>,
    /// The garment's general purpose. Defaults to 1 for shields, 0 for everything else. Class 0
    /// items are claimed and used by civilians as ordinary clothing and are subject to wear.
    #[df_token(token_name = "ARMORLEVEL")] // shared by all garments, and shields
    pub armorlevel: Option<u8>,
    // endregion ==================================================================================
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct InstrumentToken {
    /// Argument 1 of `[ITEM_INSTRUMENT:...]`
    #[df_token(token_name = "ITEM_INSTRUMENT", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    // region: Unique to instruments ==============================================================
    /// Makes the instrument stationary.
    #[df_token(token_name = "PLACED_AS_BUILDING")]
    pub placed_as_building: Option<()>,
    /// Sets a piece as the central part of the instrument.
    #[df_token(token_name = "DOMINANT_MATERIAL_PIECE")]
    pub dominant_material_piece: Option<Reference>,
    /// Defines an instrument piece. The 1st argument is the identifier that can be used in other
    /// raw tags to refer to this instrument piece. The 2nd is the tool which is required
    /// (and consumed) during the construction process to create this instrument piece.
    ///
    /// If an instrument does not have any pieces, `SELF` can be used for any argument which needs
    /// to be an instrument piece.
    #[df_token(token_name = "INSTRUMENT_PIECE")]
    pub instrument_piece: Vec<(
        Reference,
        ReferenceTo<ToolToken>,
        String,
        String,
        NameTypeEnum,
    )>,
    /// The instrument's volume range, in millibels (100 mB = 1 dB).
    #[df_token(token_name = "VOLUME_mB")]
    pub volume_mb: Option<(u32, u32)>,
    /// Defines how a musician can produce sound when using this instrument. Can be used multiple
    /// times.
    #[df_token(token_name = "SOUND_PRODUCTION")]
    pub sound_production: Vec<(SoundProductionEnum, Reference, Option<Reference>)>,
    /// Defines how the pitch can be varied by the musician. Can be used multiple times.
    #[df_token(token_name = "PITCH_CHOICE")]
    pub pitch_choice: Vec<(PitchMethodEnum, Reference, Option<Reference>)>,
    /// Defines how the instrument may be tuned. Can be used multiple times.
    #[df_token(token_name = "TUNING")]
    pub tuning: Vec<(TuningMethodEnum, Reference)>,
    /// Pitch is `min`:`max` in cents with middle C at zero. There are 1200 cents in an octave.
    ///
    /// The game verbally differentiates values from -4200 to 4200, but you can go outside
    /// that if you like.  The in-game generated instruments will range from roughly C0 to C8
    /// (-4800 to 4800), sometimes beyond for really unusual ones.
    ///
    /// You can also use `[INDEFINITE_PITCH]` instead.
    #[df_token(token_name = "PITCH_RANGE")]
    pub pitch_range: Option<(i32, i32)>,
    /// You can add as many timbre words as you want. The generated timbres have a series of
    /// checks for conflicts, but they don't apply to the raws, so how you use them is up to you.
    #[df_token(token_name = "TIMBRE")]
    pub timbre: Option<(TimbreEnum, Vec<TimbreEnum>)>,
    /// The pitch range overrides the global pitch for a register, but the register timbres are
    /// added to the global ones. You can add as many timbre words as you want.
    ///
    /// Pitch is `min`:`max` in cents with middle C at zero. There are 1200 cents in an octave.
    ///
    /// The game verbally differentiates values from -4200 to 4200, but you can go outside
    /// that if you like.  The in-game generated instruments will range from roughly C0 to C8
    /// (-4800 to 4800), sometimes beyond for really unusual ones.
    ///
    /// You can also use `[INDEFINITE_PITCH]` instead.
    #[df_token(token_name = "REGISTER")]
    pub register: Vec<(i32, i32, TimbreEnum, Vec<TimbreEnum>)>,
    /// The skill used for playing this instrument. Despite the name, any skill type will work
    /// (for example, INTIMIDATION).
    #[df_token(token_name = "MUSIC_SKILL")]
    pub music_skill: Option<SkillEnum>,
    /// Can be used instead of either `REGISTER` or `PITCH_RANGE`.
    #[df_token(token_name = "INDEFINITE_PITCH")]
    pub indefinite_pitch: Option<()>,
    // endregion ==================================================================================

    // TODO: prune the following list/region of shared tokens, these aren't all actually shared.
    // The following 3 links may be useful for figuring out which are truly shared:
    // - https://github.com/DFHack/df-structures/blob/master/df.items.xml
    // - https://github.com/DFHack/df-structures/blob/master/df.item-raws.xml
    // - https://github.com/DFHack/df-structures/blob/master/df.item-vectors.xml
    // Also, the descriptions for most of them are almost definitely inapplicable for instruments.
    // region: Shared by tools and instruments ====================================================
    /// Volume of tool in mL or cubic centimeters. Required.
    #[df_token(token_name = "SIZE")]
    pub size: Option<u32>,
    /// Name of the tool. Required.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>,
    /// Defines the item value of the tool. Required.
    #[df_token(token_name = "VALUE")]
    pub value: Option<u32>,
    /// Defines the tile used to represent the tool. Required.
    #[df_token(token_name = "TILE")]
    pub tile: Option<DFChar>,
    /// Permits the tool to be made from any bone.
    #[df_token(token_name = "BONE_MAT")]
    pub bone_mat: Option<()>,
    /// Permits the tool to be made from any ceramic material.
    #[df_token(token_name = "CERAMIC_MAT")]
    pub ceramic_mat: Option<()>,
    /// Allows a string to describe the tool when viewed. The text box can accommodate up to 325
    /// characters until it cuts off, but the spacing of actual sentences puts the realistic limit
    /// closer to 300.
    #[df_token(token_name = "DESCRIPTION")]
    pub description: Option<String>,
    /// Permits the tool to be made from any glass.
    #[df_token(token_name = "GLASS_MAT")]
    pub glass_mat: Option<()>,
    /// Permits the tool to be made from anything with the `[ITEMS_HARD]` token, such as wood, stone
    /// or metal.
    #[df_token(token_name = "HARD_MAT")]
    pub hard_mat: Option<()>,
    /// Permits the tool to be made from any leather.
    #[df_token(token_name = "LEATHER_MAT")]
    pub leather_mat: Option<()>,
    /// Permits the tool to be made from anything with the `[IS_METAL]` token.
    #[df_token(token_name = "METAL_MAT")]
    pub metal_mat: Option<()>,
    /// Permits the tool to be made from any metal with the `[ITEMS_WEAPON]` token.
    #[df_token(token_name = "METAL_WEAPON_MAT")]
    pub metal_weapon_mat: Option<()>,
    /// Permits the tool to be made from any "sheet" material, such as papyrus, paper, and
    /// parchment. May be connected to the `PAPER_SLURRY`/`PAPER_PLANT` reaction classes,
    /// but this is not verified.
    #[df_token(token_name = "SHEET_MAT")]
    pub sheet_mat: Option<()>,
    /// Permits the tool to be made from any shell.
    #[df_token(token_name = "SHELL_MAT")]
    pub shell_mat: Option<()>,
    /// Permits the tool to be made from any silk.
    #[df_token(token_name = "SILK_MAT")]
    pub silk_mat: Option<()>,
    /// Permits the tool to be made from any material with the `[ITEMS_SOFT]` token, such as leather
    /// or textiles.
    #[df_token(token_name = "SOFT_MAT")]
    pub soft_mat: Option<()>,
    /// Permits the tool to be made from any stone. Presumably connected to the `[IS_STONE]` token.
    #[df_token(token_name = "STONE_MAT")]
    pub stone_mat: Option<()>,
    /// Permits the tool to be made from any plant fiber, such as pig tails.
    #[df_token(token_name = "THREAD_PLANT_MAT")]
    pub thread_plant_mat: Option<()>,
    /// Permits the tool to be made from any wood.
    #[df_token(token_name = "WOOD_MAT")]
    pub wood_mat: Option<()>,
    /// According to Toady, "Won't be used in world gen libraries (to differentiate scrolls from
    /// quires). Also put it on bindings, rollers, instr. pieces for completeness/future use".
    /// Used on scroll rollers, book bindings, and quires.
    #[df_token(token_name = "INCOMPLETE_ITEM")]
    pub incomplete_item: Option<()>,
    /// Items that appear in the wild come standard with this kind of improvement. Used on scrolls:
    /// `[DEFAULT_IMPROVEMENT:SPECIFIC:ROLLERS:HARD_MAT]`
    ///
    /// Currently bugged, the effect is also applied to everything made in-game. This causes
    /// scrolls to have two sets of rollers, for example.
    #[df_token(token_name = "DEFAULT_IMPROVEMENT")]
    pub default_improvement: Option<ImprovementTypeWithMatFlagTokenArg>,
    /// Prevents the tool from being improved. Used on honeycombs, scroll rollers, book bindings,
    /// and quires.
    #[df_token(token_name = "UNIMPROVABLE")]
    pub unimprovable: Option<()>,
    /// **This token's purpose is unknown, and it may be an alias of another token; if you know
    /// what it does, please open an issue on the issue tracker.**
    #[df_token(token_name = "NO_DEFAULT_IMPROVEMENTS")]
    pub no_default_improvements: Option<()>,
    /// The background of the tile will be colored, instead of the foreground.
    #[df_token(token_name = "INVERTED_TILE")]
    pub inverted_tile: Option<()>,
    /// According to Toady, "only custom reactions are used to make this item". Found on scrolls and
    /// quires.
    #[df_token(token_name = "NO_DEFAULT_JOB")]
    pub no_default_job: Option<()>,
    /// Defines the task performed using the tool.
    #[df_token(token_name = "TOOL_USE")]
    pub tool_use: Vec<ToolUseEnum>,
    /// Allows item to be stored in a furniture stockpile.
    #[df_token(token_name = "FURNITURE")]
    pub furniture: Option<()>,
    // TODO: ref is shape category
    #[df_token(token_name = "SHAPE_CATEGORY")]
    pub shape_category: Option<Reference>,
    /// Used on dice.
    #[df_token(token_name = "USES_FACE_IMAGE_SET")]
    pub uses_face_image_set: Option<()>,
    /// Adjective preceding the material name (e.g. "large copper dagger")
    #[df_token(token_name = "ADJECTIVE")]
    pub adjective: Option<String>,
    /// How much the item can contain. Defaults to 0.
    #[df_token(token_name = "CONTAINER_CAPACITY")]
    pub container_capacity: Option<u32>,
    /// Required for weapons.
    #[df_token(token_name = "SHOOT_FORCE")]
    pub shoot_force: Option<u32>,
    /// Required for weapons.
    #[df_token(token_name = "SHOOT_MAXVEL")]
    pub shoot_maxvel: Option<u32>,
    /// The skill to determine effectiveness in melee with this tool. Required for weapons.
    #[df_token(token_name = "SKILL")]
    pub skill: Option<SkillEnum>,
    /// Makes this tool a ranged weapon that uses the specified ammo. The specified skill
    /// determines accuracy in ranged combat.
    #[df_token(token_name = "RANGED")]
    pub ranged: Option<(SkillEnum, ReferenceTo<AmmoToken>)>,
    /// Creatures under this size (in cm^3) must use the tool two-handed. Required for weapons.
    #[df_token(token_name = "TWO_HANDED")]
    pub two_handed: Option<u32>,
    /// Minimum body size (in cm^3) to use the tool at all (multigrasp required until `TWO_HANDED`
    /// value). Required for weapons.
    #[df_token(token_name = "MINIMUM_SIZE")]
    pub minimum_size: Option<u32>,
    /// Number of bar units needed for forging, as well as the amount gained from melting. Required
    /// for weapons.
    #[df_token(token_name = "MATERIAL_SIZE")]
    pub material_size: Option<u32>,
    /// You can have many `ATTACK` tags and one will be randomly selected for each attack, with
    /// `EDGE` attacks 100 times more common than `BLUNT` attacks. Required for weapons.
    #[df_token(token_name = "ATTACK")]
    pub attack: Vec<WeaponAttack>,
    // endregion ==================================================================================
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum NameTypeEnum {
    #[df_token(token_name = "STANDARD")]
    Standard,
    #[df_token(token_name = "ALWAYS_PLURAL")]
    AlwaysPlural,
    #[df_token(token_name = "ALWAYS_SINGULAR")]
    AlwaysSingular,
}
impl Default for NameTypeEnum {
    fn default() -> Self {
        Self::Standard
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum PitchMethodEnum {
    #[df_token(token_name = "MEMBRANE_POSITION")]
    MembranePosition,
    #[df_token(token_name = "SUBPART_CHOICE")]
    SubpartChoice,
    #[df_token(token_name = "KEYBOARD")]
    Keyboard,
    /// Requires two `INSTRUMENT_PIECE` tokens, first for "string" second for "neck"
    /// -- or whatever is being pressed against what.
    #[df_token(token_name = "STOPPING_FRET")]
    StoppingFret,
    /// Requires two `INSTRUMENT_PIECE` tokens.
    #[df_token(token_name = "STOPPING_AGAINST_BODY")]
    StoppingAgainstBody,
    #[df_token(token_name = "STOPPING_HOLE")]
    StoppingHole,
    #[df_token(token_name = "STOPPING_HOLE_KEY")]
    StoppingHoleKey,
    #[df_token(token_name = "SLIDE")]
    Slide,
    #[df_token(token_name = "HARMONIC_SERIES")]
    HarmonicSeries,
    #[df_token(token_name = "VALVE_ROUTES_AIR")]
    ValveRoutesAir,
    #[df_token(token_name = "BP_IN_BELL")]
    BpInBell,
    /// Requires two `INSTRUMENT_PIECE` tokens, first is what is being changed e.g. "strings",
    /// second is "body" which has the pedalboard -- or whatever piece is being stepped on.
    #[df_token(token_name = "FOOT_PEDALS")]
    FootPedals,
}
impl Default for PitchMethodEnum {
    fn default() -> Self {
        Self::MembranePosition
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum TuningMethodEnum {
    #[df_token(token_name = "PEGS")]
    Pegs,
    #[df_token(token_name = "ADJUSTABLE_BRIDGES")]
    AdjustableBridhes,
    #[df_token(token_name = "CROOKS")]
    Crooks,
    #[df_token(token_name = "TIGHTENING")]
    Tightening,
    #[df_token(token_name = "LEVERS")]
    Levers,
}
impl Default for TuningMethodEnum {
    fn default() -> Self {
        Self::Pegs
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum SoundProductionEnum {
    #[df_token(token_name = "PLUCKED_BY_BP")]
    PluckedByBp,
    /// Requires two `INSTRUMENT_PIECE` tokens: actor, then target.
    #[df_token(token_name = "PLUCKED")]
    Plucked,
    /// Requires two `INSTRUMENT_PIECE` tokens: actor, then target.
    #[df_token(token_name = "BOWED")]
    Bowed,
    #[df_token(token_name = "STRUCK_BY_BP")]
    StruckByBp,
    /// Requires two `INSTRUMENT_PIECE` tokens: actor, then target.
    #[df_token(token_name = "STRUCK")]
    Struck,
    #[df_token(token_name = "VIBRATE_BP_AGAINST_OPENING")]
    VibrateBpAgainstOpening,
    #[df_token(token_name = "BLOW_AGAINST_FIPPLE")]
    BlowAgainstFipple,
    #[df_token(token_name = "BLOW_OVER_OPENING_SIDE")]
    BlowOverOpeningSide,
    #[df_token(token_name = "BLOW_OVER_OPENING_END")]
    BlowOverOpeningEnd,
    #[df_token(token_name = "BLOW_OVER_SINGLE_REED")]
    BlowOverSingleReed,
    #[df_token(token_name = "BLOW_OVER_DOUBLE_REED")]
    BlowOverDoubleReed,
    #[df_token(token_name = "BLOW_OVER_FREE_REED")]
    BlowOverFreeReed,
    #[df_token(token_name = "STRUCK_TOGETHER")]
    StruckTogether,
    #[df_token(token_name = "SHAKEN")]
    Shaken,
    /// Requires two `INSTRUMENT_PIECE` tokens: actor, then target.
    #[df_token(token_name = "SCRAPED")]
    Scraped,
    /// Requires two `INSTRUMENT_PIECE` tokens: actor, then target.
    #[df_token(token_name = "FRICTION")]
    Friction,
    #[df_token(token_name = "RESONATOR")]
    Resonator,
    /// Requires two `INSTRUMENT_PIECE` tokens: actor, then target.
    #[df_token(token_name = "BAG_OVER_REED")]
    BagOverReed,
    /// Requires two `INSTRUMENT_PIECE` tokens: actor, then target.
    #[df_token(token_name = "AIR_OVER_REED")]
    AirOverReed,
    /// Requires two `INSTRUMENT_PIECE` tokens: actor, then target.
    #[df_token(token_name = "AIR_OVER_FREE_REED")]
    AirOverFreeReed,
    /// Requires two `INSTRUMENT_PIECE` tokens: actor, then target.
    #[df_token(token_name = "AIR_AGAINST_FIPPLE")]
    AirAgainstFipple,
}
impl Default for SoundProductionEnum {
    fn default() -> Self {
        Self::PluckedByBp
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum TimbreEnum {
    #[df_token(token_name = "CLEAR")]
    Clear,
    #[df_token(token_name = "NOISY")]
    Noisy,
    #[df_token(token_name = "FULL")]
    Full,
    #[df_token(token_name = "THIN")]
    Thin,
    #[df_token(token_name = "ROUND")]
    Round,
    #[df_token(token_name = "SHARP")]
    Sharp,
    #[df_token(token_name = "SMOOTH")]
    Smooth,
    #[df_token(token_name = "CHOPPY")]
    Choppy,
    #[df_token(token_name = "STEADY")]
    Steady,
    #[df_token(token_name = "EVOLVING")]
    Evolving,
    #[df_token(token_name = "STRONG")]
    Strong,
    #[df_token(token_name = "DELICATE")]
    Delicate,
    #[df_token(token_name = "BRIGHT")]
    Bright,
    #[df_token(token_name = "GRACEFUL")]
    Graceful,
    #[df_token(token_name = "SPARSE")]
    Sparse,
    #[df_token(token_name = "BREATHY")]
    Breathy,
    #[df_token(token_name = "STRAINED")]
    Strained,
    #[df_token(token_name = "BROAD")]
    Broad,
    #[df_token(token_name = "LIGHT")]
    Light,
    #[df_token(token_name = "MELLOW")]
    Mellow,
    #[df_token(token_name = "WOBBLING")]
    Wobbling,
    #[df_token(token_name = "FOCUSED")]
    Focused,
    #[df_token(token_name = "EVEN")]
    Even,
    #[df_token(token_name = "FLUID")]
    Fluid,
    #[df_token(token_name = "VIBRATING")]
    Vibrating,
    #[df_token(token_name = "QUAVERING")]
    Quavering,
    #[df_token(token_name = "EERIE")]
    Eerie,
    #[df_token(token_name = "FRAGILE")]
    Fragile,
    #[df_token(token_name = "BRITTLE")]
    Brittle,
    #[df_token(token_name = "PURE")]
    Pure,
    #[df_token(token_name = "PIERCING")]
    Piercing,
    #[df_token(token_name = "STRIDENT")]
    Strident,
    #[df_token(token_name = "WAVERING")]
    Wavering,
    #[df_token(token_name = "HARSH")]
    Harsh,
    #[df_token(token_name = "REEDY")]
    Reedy,
    #[df_token(token_name = "NASAL")]
    Nasal,
    #[df_token(token_name = "BUZZY")]
    Buzzy,
    #[df_token(token_name = "ROUGH")]
    Rough,
    #[df_token(token_name = "WARM")]
    Warm,
    #[df_token(token_name = "RUGGED")]
    Rugged,
    #[df_token(token_name = "HEAVY")]
    Heavy,
    #[df_token(token_name = "FLAT")]
    Flat,
    #[df_token(token_name = "DARK")]
    Dark,
    #[df_token(token_name = "CRISP")]
    Crisp,
    #[df_token(token_name = "SONOROUS")]
    Sonorous,
    #[df_token(token_name = "WATERY")]
    Watery,
    #[df_token(token_name = "GENTLE")]
    Gentle,
    #[df_token(token_name = "SLICING")]
    Slicing,
    #[df_token(token_name = "LIQUID")]
    Liquid,
    #[df_token(token_name = "RAUCOUS")]
    Raucous,
    #[df_token(token_name = "BREEZY")]
    Breezy,
    #[df_token(token_name = "RASPY")]
    Raspy,
    #[df_token(token_name = "WISPY")]
    Wispy,
    #[df_token(token_name = "SHRILL")]
    Shrill,
    #[df_token(token_name = "MUDDY")]
    Muddy,
    #[df_token(token_name = "RICH")]
    Rich,
    #[df_token(token_name = "DULL")]
    Dull,
    #[df_token(token_name = "FLOATING")]
    Floating,
    #[df_token(token_name = "RINGING")]
    Ringing,
    #[df_token(token_name = "RESONANT")]
    Resonant,
    #[df_token(token_name = "SWEET")]
    Sweet,
    #[df_token(token_name = "RIPPLING")]
    Rippling,
    #[df_token(token_name = "SPARKLING")]
    Sparkling,
}
impl Default for TimbreEnum {
    fn default() -> Self {
        Self::Clear
    }
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct PantsToken {
    /// Argument 1 of `[ITEM_PANTS:...]`
    #[df_token(token_name = "ITEM_PANTS", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    // region: Shared by garments/shields/trapcomp/weapons; are all required ======================
    /// What this item will be called in-game.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>,
    /// How much material is needed to make the item. Most important with bars. The number of bars
    /// required to make the item is the value divided by three.
    #[df_token(token_name = "MATERIAL_SIZE")]
    pub material_size: Option<u32>,
    // endregion ==================================================================================
    // region: Shared by all garments =============================================================
    /// Adjective preceding the material name (e.g. "large copper dagger").
    #[df_token(token_name = "ADJECTIVE")]
    pub adjective: Option<String>,
    /// Metal versions of this item count as one `ARMORLEVEL` higher and thus won't be worn by
    /// random peasants. This tag will not work unless `ARMORLEVEL` is explicitly declared: if you
    /// leave out `ARMORLEVEL`, even metal armor will default to level 0.
    #[df_token(token_name = "METAL_ARMOR_LEVELS")]
    pub metal_armor_levels: Option<()>,
    /// Metal versions of this item will have "chain" added between the material and item name.
    #[df_token(token_name = "CHAIN_METAL_TEXT")]
    pub chain_metal_text: Option<()>,
    /// Clothiers can make this item from all kinds of cloth. If paired with `[LEATHER]`, the item
    /// has an equal chance of being either in randomly generated outfits. Further uses of this tag
    /// are unknown.
    #[df_token(token_name = "SOFT")]
    pub soft: Option<()>,
    /// Default state in the absence of a `[SOFT]` token. Actual effects unknown.
    #[df_token(token_name = "HARD")]
    pub hard: Option<()>,
    /// Item can be made from metal. Overrides `[SOFT]` and `[LEATHER]` in randomly generated
    /// outfits, if the `ARMORLEVEL` permits. Civilizations with `[WOOD_ARMOR]` will make this
    /// item out of wood instead.
    #[df_token(token_name = "METAL")]
    pub metal: Option<()>,
    /// Craftsmen can make this item from bones. Randomly generated outfits don't include bone
    /// armor.
    #[df_token(token_name = "BARRED")]
    pub barred: Option<()>,
    /// Craftsmen can make this item from shells. Randomly generated outfits don't include shell
    /// armor.
    #[df_token(token_name = "SCALED")]
    pub scaled: Option<()>,
    /// Leatherworkers can make this item from leather. If paired with `[SOFT]`, this item has an
    /// equal chance of being either in randomly generated outfits.
    #[df_token(token_name = "LEATHER")]
    pub leather: Option<()>,
    /// Only one shaped piece of clothing can be worn on a single body slot at a time.
    #[df_token(token_name = "SHAPED")]
    pub shaped: Option<()>,
    /// Increases the `*_STRAIN_AT_YIELD` properties of the armor's material to 50000, if lower.
    /// This makes the garment flex and give way instead of shattering under force. Strong materials
    /// that resist cutting will blunt edged attacks into bone-crushing hits instead.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_CHAIN_ALL")]
    pub structural_elasticity_chain_all: Option<()>,
    /// Increases the `*_STRAIN_AT_YIELD` properties of the armor's material to 50000, but only if
    /// the garment is made from metal.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_CHAIN_METAL")]
    pub structural_elasticity_chain_metal: Option<()>,
    /// Reduces the armor material's `SHEAR_YIELD` to 20000, `SHEAR_FRACTURE` to 30000 and increases
    /// the `*_STRAIN_AT_YIELD` properties to 50000, but only if the garment is made from cloth.
    /// This makes the item very weak against edged attacks, even if the thread material is
    /// normally very strong.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_WOVEN_THREAD")]
    pub structural_elasticity_woven_thread: Option<()>,
    /// The item's bulkiness when worn. Aside from the layer limitations, it's a big contributor to
    /// the thickness and weight (and therefore price) of the garment. See
    /// [Armor](https://dwarffortresswiki.org/index.php/Armor) for more on item sizes and
    /// layering. Defaults to 10.
    #[df_token(token_name = "LAYER_SIZE")]
    pub layer_size: Option<u32>,
    /// The maximum amount of garments that can fit underneath this garment. See
    /// [Armor](https://dwarffortresswiki.org/index.php/Armor) for more on item sizes and
    /// layering. Defaults to 10.
    #[df_token(token_name = "LAYER_PERMIT")]
    pub layer_permit: Option<u32>,
    /// Where the item goes in relation to other clothes. Socks cannot be worn on top of boots!
    ///
    /// The `LAYER_PERMIT` of the highest layer is used on a given section of the body - you can fit
    /// a lot of shirts and other undergarments underneath a robe, but not if you wear a leather
    /// jerkin on top of it, and you can still wear a cloak over the whole ensemble. Defaults to
    /// `UNDER`.
    #[df_token(token_name = "LAYER")]
    pub layer: Option<LayerEnum>,
    /// How often the garment gets in the way of a contaminant or an attack. Armor with a 5%
    /// coverage value, for example, will be near useless because 95% of attacks will bypass it
    /// completely. Temperature effects and armor thickness are also influenced. Defaults to 100.
    #[df_token(token_name = "COVERAGE")]
    pub coverage: Option<u8>,
    /// The garment's general purpose. Defaults to 1 for shields, 0 for everything else. Class 0
    /// items are claimed and used by civilians as ordinary clothing and are subject to wear.
    #[df_token(token_name = "ARMORLEVEL")] // shared by all garments, and shields
    pub armorlevel: Option<u8>,
    // endregion ==================================================================================
    // region: Shared by ARMOR and PANTS ==========================================================
    /// Changes the plural form of this item to "`phrase of` item". Primarily pertains to the stock
    /// screens.
    ///
    /// Example, "suits of" platemail, "pairs of" trousers, etc.
    #[df_token(token_name = "PREPLURAL")]
    pub preplural: Option<String>,
    /// If the item has no material associated with it (e.g. stockpile menus and trade
    /// negotiations), this will be displayed in its place. Used for leather armor in vanilla.
    #[df_token(token_name = "MATERIAL_PLACEHOLDER")]
    pub material_placeholder: Option<String>,
    /// Length of the legs/hem, counted in `[LIMB]` body parts towards the feet. A value of 0 only
    /// covers the lower body, 1 extends over the upper legs and so on. Regardless of the value,
    /// body armor or pants can never extend to cover the feet.
    #[df_token(token_name = "LBSTEP")]
    pub lbstep: Option<Choose<u8, MaxEnum>>,
    // endregion ==================================================================================
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct ShieldToken {
    /// Argument 1 of `[ITEM_SHIELD:...]`
    #[df_token(token_name = "ITEM_SHIELD", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// Affects the block chance of the shield. Defaults to 10.
    #[df_token(token_name = "BLOCKCHANCE")]
    pub blockchance: Option<Clamp<u8, 0, 100>>,
    /// Length of gloves or footwear, counted in `[LIMB]` body parts towards the torso. A value of 1
    /// lets gloves cover the lower arms, a value of 2 stretches a boot all the way over the upper
    /// leg and so on.
    ///
    /// Regardless of the value, none of these items can ever extend to cover the upper or lower
    /// body. Shields also have this token, but it only seems to affect weight.
    #[df_token(token_name = "UPSTEP")]
    pub upstep: Option<Choose<u8, MaxEnum>>, // shared by glove, shield, shoes
    /// The garment's general purpose. Defaults to 1 for shields, 0 for everything else. Class 0
    /// items are claimed and used by civilians as ordinary clothing and are subject to wear.
    #[df_token(token_name = "ARMORLEVEL")]
    pub armorlevel: Option<u8>, // shared by all garments, and shields
    // region: Shared by garments/shields/trapcomp/weapons; are all required ======================
    /// What this item will be called in-game.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>,
    /// How much material is needed to make the item. Most important with bars. The number of bars
    /// required to make the item is the value divided by three.
    #[df_token(token_name = "MATERIAL_SIZE")]
    pub material_size: Option<u32>,
    // endregion ==================================================================================
    /// Adjective preceding the material name (e.g. "large copper dagger").
    #[df_token(token_name = "ADJECTIVE")]
    pub adjective: Option<String>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct ShoesToken {
    /// Argument 1 of `[ITEM_SHOES:...]`
    #[df_token(token_name = "ITEM_SHOES", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// Length of gloves or footwear, counted in `[LIMB]` body parts towards the torso. A value of 1
    /// lets gloves cover the lower arms, a value of 2 stretches a boot all the way over the upper
    /// leg and so on.
    ///
    /// Regardless of the value, none of these items can ever extend to cover the upper or lower
    /// body. Shields also have this token, but it only seems to affect weight.
    #[df_token(token_name = "UPSTEP")]
    pub upstep: Option<Choose<u8, MaxEnum>>, // shared by glove, shield, shoes
    // region: Shared by garments/shields/trapcomp/weapons; are all required ======================
    /// What this item will be called in-game.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>,
    /// How much material is needed to make the item. Most important with bars. The number of bars
    /// required to make the item is the value divided by three.
    #[df_token(token_name = "MATERIAL_SIZE")]
    pub material_size: Option<u32>,
    // endregion ==================================================================================
    // region: Shared by all garments =============================================================
    /// Adjective preceding the material name (e.g. "large copper dagger").
    #[df_token(token_name = "ADJECTIVE")]
    pub adjective: Option<String>,
    /// Metal versions of this item count as one `ARMORLEVEL` higher and thus won't be worn by
    /// random peasants. This tag will not work unless `ARMORLEVEL` is explicitly declared: if you
    /// leave out `ARMORLEVEL`, even metal armor will default to level 0.
    #[df_token(token_name = "METAL_ARMOR_LEVELS")]
    pub metal_armor_levels: Option<()>,
    /// Metal versions of this item will have "chain" added between the material and item name.
    #[df_token(token_name = "CHAIN_METAL_TEXT")]
    pub chain_metal_text: Option<()>,
    /// Clothiers can make this item from all kinds of cloth. If paired with `[LEATHER]`, the item
    /// has an equal chance of being either in randomly generated outfits. Further uses of this tag
    /// are unknown.
    #[df_token(token_name = "SOFT")]
    pub soft: Option<()>,
    /// Default state in the absence of a `[SOFT]` token. Actual effects unknown.
    #[df_token(token_name = "HARD")]
    pub hard: Option<()>,
    /// Item can be made from metal. Overrides `[SOFT]` and `[LEATHER]` in randomly generated
    /// outfits, if the `ARMORLEVEL` permits. Civilizations with `[WOOD_ARMOR]` will make this
    /// item out of wood instead.
    #[df_token(token_name = "METAL")]
    pub metal: Option<()>,
    /// Craftsmen can make this item from bones. Randomly generated outfits don't include bone
    /// armor.
    #[df_token(token_name = "BARRED")]
    pub barred: Option<()>,
    /// Craftsmen can make this item from shells. Randomly generated outfits don't include shell
    /// armor.
    #[df_token(token_name = "SCALED")]
    pub scaled: Option<()>,
    /// Leatherworkers can make this item from leather. If paired with `[SOFT]`, this item has an
    /// equal chance of being either in randomly generated outfits.
    #[df_token(token_name = "LEATHER")]
    pub leather: Option<()>,
    /// Only one shaped piece of clothing can be worn on a single body slot at a time.
    #[df_token(token_name = "SHAPED")]
    pub shaped: Option<()>,
    /// Increases the `*_STRAIN_AT_YIELD` properties of the armor's material to 50000, if lower.
    /// This makes the garment flex and give way instead of shattering under force. Strong materials
    /// that resist cutting will blunt edged attacks into bone-crushing hits instead.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_CHAIN_ALL")]
    pub structural_elasticity_chain_all: Option<()>,
    /// Increases the `*_STRAIN_AT_YIELD` properties of the armor's material to 50000, but only if
    /// the garment is made from metal.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_CHAIN_METAL")]
    pub structural_elasticity_chain_metal: Option<()>,
    /// Reduces the armor material's `SHEAR_YIELD` to 20000, `SHEAR_FRACTURE` to 30000 and increases
    /// the `*_STRAIN_AT_YIELD` properties to 50000, but only if the garment is made from cloth.
    /// This makes the item very weak against edged attacks, even if the thread material is
    /// normally very strong.
    #[df_token(token_name = "STRUCTURAL_ELASTICITY_WOVEN_THREAD")]
    pub structural_elasticity_woven_thread: Option<()>,
    /// The item's bulkiness when worn. Aside from the layer limitations, it's a big contributor to
    /// the thickness and weight (and therefore price) of the garment. See
    /// [Armor](https://dwarffortresswiki.org/index.php/Armor) for more on item sizes and
    /// layering. Defaults to 10.
    #[df_token(token_name = "LAYER_SIZE")]
    pub layer_size: Option<u32>,
    /// The maximum amount of garments that can fit underneath this garment. See
    /// [Armor](https://dwarffortresswiki.org/index.php/Armor) for more on item sizes and
    /// layering. Defaults to 10.
    #[df_token(token_name = "LAYER_PERMIT")]
    pub layer_permit: Option<u32>,
    /// Where the item goes in relation to other clothes. Socks cannot be worn on top of boots!
    ///
    /// The `LAYER_PERMIT` of the highest layer is used on a given section of the body - you can fit
    /// a lot of shirts and other undergarments underneath a robe, but not if you wear a leather
    /// jerkin on top of it, and you can still wear a cloak over the whole ensemble. Defaults to
    /// `UNDER`.
    #[df_token(token_name = "LAYER")]
    pub layer: Option<LayerEnum>,
    /// How often the garment gets in the way of a contaminant or an attack. Armor with a 5%
    /// coverage value, for example, will be near useless because 95% of attacks will bypass it
    /// completely. Temperature effects and armor thickness are also influenced. Defaults to 100.
    #[df_token(token_name = "COVERAGE")]
    pub coverage: Option<u8>,
    /// The garment's general purpose. Defaults to 1 for shields, 0 for everything else. Class 0
    /// items are claimed and used by civilians as ordinary clothing and are subject to wear.
    #[df_token(token_name = "ARMORLEVEL")] // shared by all garments, and shields
    pub armorlevel: Option<u8>,
    // endregion ==================================================================================
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum LayerEnum {
    #[df_token(token_name = "UNDER")]
    Under,
    #[df_token(token_name = "OVER")]
    Over,
    #[df_token(token_name = "ARMOR")]
    Armor,
    #[df_token(token_name = "COVER")]
    Cover,
}
impl Default for LayerEnum {
    fn default() -> Self {
        Self::Under
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum MaxEnum {
    #[df_token(token_name = "MAX")]
    Max,
}
impl Default for MaxEnum {
    fn default() -> Self {
        Self::Max
    }
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct SiegeAmmoToken {
    /// Argument 1 of `[ITEM_SIEGEAMMO:...]`
    #[df_token(token_name = "ITEM_SIEGEAMMO", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// What this item will be called in-game.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>,
    /// Specifies what type of siege engine uses this ammunition. Currently, only `BALLISTA` is
    /// permitted.
    #[df_token(token_name = "CLASS")]
    pub class: Option<SiegeAmmoClassEnum>,
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum SiegeAmmoClassEnum {
    #[df_token(token_name = "BALLISTA")]
    Ballista,
}
impl Default for SiegeAmmoClassEnum {
    fn default() -> Self {
        Self::Ballista
    }
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct ToolToken {
    /// Argument 1 of `[ITEM_TOOL:...]`
    #[df_token(token_name = "ITEM_TOOL", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// Volume of tool in mL or cubic centimeters. Required.
    #[df_token(token_name = "SIZE")]
    pub size: Option<u32>,
    /// Name of the tool. Required.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>,
    /// Defines the item value of the tool. Required.
    #[df_token(token_name = "VALUE")]
    pub value: Option<u32>,
    /// Defines the tile used to represent the tool. Required.
    #[df_token(token_name = "TILE")]
    pub tile: Option<DFChar>,
    /// Permits the tool to be made from any bone.
    #[df_token(token_name = "BONE_MAT")]
    pub bone_mat: Option<()>,
    /// Permits the tool to be made from any ceramic material.
    #[df_token(token_name = "CERAMIC_MAT")]
    pub ceramic_mat: Option<()>,
    /// Allows a string to describe the tool when viewed. The text box can accommodate up to 325
    /// characters until it cuts off, but the spacing of actual sentences puts the realistic limit
    /// closer to 300.
    #[df_token(token_name = "DESCRIPTION")]
    pub description: Option<String>,
    /// Permits the tool to be made from any glass.
    #[df_token(token_name = "GLASS_MAT")]
    pub glass_mat: Option<()>,
    /// Permits the tool to be made from anything with the `[ITEMS_HARD]` token, such as wood, stone
    /// or metal.
    #[df_token(token_name = "HARD_MAT")]
    pub hard_mat: Option<()>,
    /// Permits the tool to be made from any leather.
    #[df_token(token_name = "LEATHER_MAT")]
    pub leather_mat: Option<()>,
    /// Permits the tool to be made from anything with the `[IS_METAL]` token.
    #[df_token(token_name = "METAL_MAT")]
    pub metal_mat: Option<()>,
    /// Permits the tool to be made from any metal with the `[ITEMS_WEAPON]` token.
    #[df_token(token_name = "METAL_WEAPON_MAT")]
    pub metal_weapon_mat: Option<()>,
    /// Permits the tool to be made from any "sheet" material, such as papyrus, paper, and
    /// parchment. May be connected to the `PAPER_SLURRY`/`PAPER_PLANT` reaction classes,
    /// but this is not verified.
    #[df_token(token_name = "SHEET_MAT")]
    pub sheet_mat: Option<()>,
    /// Permits the tool to be made from any shell.
    #[df_token(token_name = "SHELL_MAT")]
    pub shell_mat: Option<()>,
    /// Permits the tool to be made from any silk.
    #[df_token(token_name = "SILK_MAT")]
    pub silk_mat: Option<()>,
    /// Permits the tool to be made from any material with the `[ITEMS_SOFT]` token, such as leather
    /// or textiles.
    #[df_token(token_name = "SOFT_MAT")]
    pub soft_mat: Option<()>,
    /// Permits the tool to be made from any stone. Presumably connected to the `[IS_STONE]` token.
    #[df_token(token_name = "STONE_MAT")]
    pub stone_mat: Option<()>,
    /// Permits the tool to be made from any plant fiber, such as pig tails.
    #[df_token(token_name = "THREAD_PLANT_MAT")]
    pub thread_plant_mat: Option<()>,
    /// Permits the tool to be made from any wood.
    #[df_token(token_name = "WOOD_MAT")]
    pub wood_mat: Option<()>,
    /// According to Toady, "Won't be used in world gen libraries (to differentiate scrolls from
    /// quires). Also put it on bindings, rollers, instr. pieces for completeness/future use".
    /// Used on scroll rollers, book bindings, and quires.
    #[df_token(token_name = "INCOMPLETE_ITEM")]
    pub incomplete_item: Option<()>,
    /// Items that appear in the wild come standard with this kind of improvement. Used on scrolls:
    /// `[DEFAULT_IMPROVEMENT:SPECIFIC:ROLLERS:HARD_MAT]`
    ///
    /// Currently bugged, the effect is also applied to everything made in-game. This causes
    /// scrolls to have two sets of rollers, for example.
    #[df_token(token_name = "DEFAULT_IMPROVEMENT")]
    pub default_improvement: Option<ImprovementTypeWithMatFlagTokenArg>,
    /// Prevents the tool from being improved. Used on honeycombs, scroll rollers, book bindings,
    /// and quires.
    #[df_token(token_name = "UNIMPROVABLE")]
    pub unimprovable: Option<()>,
    /// **This token's purpose is unknown, and it may be an alias of another token; if you know
    /// what it does, please open an issue on the issue tracker.**
    #[df_token(token_name = "NO_DEFAULT_IMPROVEMENTS")]
    pub no_default_improvements: Option<()>,
    /// The background of the tile will be colored, instead of the foreground.
    #[df_token(token_name = "INVERTED_TILE")]
    pub inverted_tile: Option<()>,
    /// According to Toady, "only custom reactions are used to make this item". Found on scrolls and
    /// quires.
    #[df_token(token_name = "NO_DEFAULT_JOB")]
    pub no_default_job: Option<()>,
    /// Defines the task performed using the tool.
    #[df_token(token_name = "TOOL_USE")]
    pub tool_use: Vec<ToolUseEnum>,
    /// Allows item to be stored in a furniture stockpile.
    #[df_token(token_name = "FURNITURE")]
    pub furniture: Option<()>,
    // TODO: ref is shape category
    #[df_token(token_name = "SHAPE_CATEGORY")]
    pub shape_category: Option<Reference>,
    /// Used on dice.
    #[df_token(token_name = "USES_FACE_IMAGE_SET")]
    pub uses_face_image_set: Option<()>,
    /// Adjective preceding the material name (e.g. "large copper dagger")
    #[df_token(token_name = "ADJECTIVE")]
    pub adjective: Option<String>,
    /// How much the item can contain. Defaults to 0.
    #[df_token(token_name = "CONTAINER_CAPACITY")]
    pub container_capacity: Option<u32>,
    /// Required for weapons.
    #[df_token(token_name = "SHOOT_FORCE")]
    pub shoot_force: Option<u32>,
    /// Required for weapons.
    #[df_token(token_name = "SHOOT_MAXVEL")]
    pub shoot_maxvel: Option<u32>,
    /// The skill to determine effectiveness in melee with this tool. Required for weapons.
    #[df_token(token_name = "SKILL")]
    pub skill: Option<SkillEnum>,
    /// Makes this tool a ranged weapon that uses the specified ammo. The specified skill
    /// determines accuracy in ranged combat.
    #[df_token(token_name = "RANGED")]
    pub ranged: Option<(SkillEnum, ReferenceTo<AmmoToken>)>,
    /// Creatures under this size (in cm^3) must use the tool two-handed. Required for weapons.
    #[df_token(token_name = "TWO_HANDED")]
    pub two_handed: Option<u32>,
    /// Minimum body size (in cm^3) to use the tool at all (multigrasp required until `TWO_HANDED`
    /// value). Required for weapons.
    #[df_token(token_name = "MINIMUM_SIZE")]
    pub minimum_size: Option<u32>,
    /// Number of bar units needed for forging, as well as the amount gained from melting. Required
    /// for weapons.
    #[df_token(token_name = "MATERIAL_SIZE")]
    pub material_size: Option<u32>,
    /// You can have many `ATTACK` tags and one will be randomly selected for each attack, with
    /// `EDGE` attacks 100 times more common than `BLUNT` attacks. Required for weapons.
    #[df_token(token_name = "ATTACK")]
    pub attack: Vec<WeaponAttack>,
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum ToolUseEnum {
    /// Cauldron adventure mode decoration / weapon
    #[df_token(token_name = "LIQUID_COOKING")]
    LiquidCooking,
    /// Ladle, an adventure mode decoration/weapon.
    #[df_token(token_name = "LIQUID_SCOOP")]
    LiquidScoop,
    /// Mortar, an adventure mode decoration/weapon.
    #[df_token(token_name = "GRIND_POWDER_RECEPTACLE")]
    GrindPowderReceptacle,
    /// Pestle, an adventure mode decoration/weapon.
    #[df_token(token_name = "GRIND_POWDER_GRINDER")]
    GrindPowderGrinder,
    /// Carving knife, an adventure mode decoration/weapon.
    #[df_token(token_name = "MEAT_CARVING")]
    MeatCarving,
    /// Boning knife, an adventure mode decoration/weapon.
    #[df_token(token_name = "MEAT_BONING")]
    MeatBoning,
    /// Slicing knife, an adventure mode decoration/weapon.
    #[df_token(token_name = "MEAT_SLICING")]
    MeatSlicing,
    /// Meat cleaver, an adventure mode decoration/weapon.
    #[df_token(token_name = "MEAT_CLEAVING")]
    MeatCleaving,
    /// Carving fork, an adventure mode decoration/weapon.
    #[df_token(token_name = "HOLD_MEAT_FOR_CARVING")]
    HoldMeatForCarving,
    /// Bowl, an adventure mode decoration/weapon.
    #[df_token(token_name = "MEAL_CONTAINER")]
    MealContainer,
    /// Nest box for your birds to lay eggs.
    #[df_token(token_name = "NEST_BOX")]
    NestBox,
    /// Jug; can store honey or oil.
    #[df_token(token_name = "LIQUID_CONTAINER")]
    LiquidContainer,
    /// Large pot; can store beer.
    #[df_token(token_name = "FOOD_STORAGE")]
    FoodStorage,
    /// Hive; can make honey.
    #[df_token(token_name = "HIVE")]
    Hive,
    /// Pouch, an adventure mode coin purse.
    #[df_token(token_name = "SMALL_OBJECT_STORAGE")]
    SmallObjectStorage,
    /// Minecart; item hauling/weapon.
    #[df_token(token_name = "TRACK_CART")]
    TrackCart,
    /// Wheelbarrow; allows hauling items faster.
    #[df_token(token_name = "HEAVY_OBJECT_HAULING")]
    HeavyObjectHauling,
    /// Stepladder, allows gathering fruit from trees.
    #[df_token(token_name = "STAND_AND_WORK_ABOVE")]
    StandAndWorkAbove,
    /// Scroll rollers.
    #[df_token(token_name = "ROLL_UP_SHEET")]
    RollUpSheet,
    /// Book binding.
    #[df_token(token_name = "PROTECT_FOLDED_SHEETS")]
    ProtectFoldedSheets,
    /// Scroll & quire.
    #[df_token(token_name = "CONTAIN_WRITING")]
    ContainWriting,
    /// Bookcase.
    #[df_token(token_name = "BOOKCASE")]
    Bookcase,
    /// Pedestal & display case for museums.
    #[df_token(token_name = "DISPLAY_OBJECT")]
    DisplayObject,
    /// Altar for (eventually) offering sacrifices.
    #[df_token(token_name = "PLACE_OFFERING")]
    PlaceOffering,
    /// Dice.
    #[df_token(token_name = "DIVINATION")]
    Divination,
    /// Dice.
    #[df_token(token_name = "GAMES_OF_CHANCE")]
    GamesOfChance,
}
impl Default for ToolUseEnum {
    fn default() -> Self {
        Self::LiquidCooking
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum ImprovementTypeWithMatFlagTokenArg {
    // TODO: token should be marked as deprecated/unused/ignored by the game, see #83
    // #[df_token(token_name = "ART_IMAGE")]
    // ArtImage,
    // #[df_token(token_name = "COVERED")]
    Covered(ImprovementMaterialFlagEnum),
    // #[df_token(token_name = "RINGS_HANGING")]
    RingsHanging(ImprovementMaterialFlagEnum),
    // #[df_token(token_name = "BANDS")]
    Bands(ImprovementMaterialFlagEnum),
    // #[df_token(token_name = "SPIKES")]
    Spikes(ImprovementMaterialFlagEnum),
    // #[df_token(token_name = "SPECIFIC")]
    // #[df_alias(token_name = "ITEMSPECIFIC", discouraged)]
    Specific((ItemSpecificEnum, ImprovementMaterialFlagEnum)),
    // TODO: token should be marked as deprecated/unused/ignored by the game, see #83
    // #[df_token(token_name = "THREAD")]
    // Thread,
    // TODO: token should be marked as deprecated/unused/ignored by the game, see #83
    // #[df_token(token_name = "CLOTH")]
    // Cloth,
    // TODO: token should be marked as deprecated/unused/ignored by the game, see #83
    // #[df_token(token_name = "SEWN_IMAGE")]
    // SewnImage,
    // #[df_token(token_name = "PAGES")]
    Pages(ImprovementMaterialFlagEnum),
    // TODO: token should be marked as deprecated/unused/ignored by the game, see #83
    // #[df_token(token_name = "ILLUSTRATION")]
    // Illustration,
    // #[df_token(token_name = "INSTRUMENT_PIECE")]
    InstrumentPiece((Reference, ImprovementMaterialFlagEnum)), // Ref to INSTRUMENT PIECE, distinct from ITEM_INSTRUMENT
    // #[df_token(token_name = "WRITING")]
    Writing(ImprovementMaterialFlagEnum),
    // TODO: token should be marked as deprecated/unused/ignored by the game, see #83
    // #[df_token(token_name = "IMAGE_SET")]
    // ImageSet,
}
impl Default for ImprovementTypeWithMatFlagTokenArg {
    fn default() -> Self {
        Self::Covered(ImprovementMaterialFlagEnum::default())
    }
}

// Deserialize a token with following pattern: `[REF:improvement_type_token_arg:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(ImprovementTypeWithMatFlagTokenArg);

impl TryFromArgumentGroup for ImprovementTypeWithMatFlagTokenArg {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Safe first argument (is not token_name) for error case
        let arg0 = match token.get_current_arg() {
            Ok(arg) => Ok(arg.clone()),
            Err(err) => Err(err),
        };
        let reference_arg0 =
            Reference::try_from_argument_group(token, source, diagnostics, add_diagnostics_on_err)?;
        let bp_criteria = match reference_arg0.0.as_ref() {
            "COVERED" => {
                let covered = ImprovementMaterialFlagEnum::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ImprovementTypeWithMatFlagTokenArg::Covered(covered)
            }
            "RINGS_HANGING" => {
                let rings_hanging = ImprovementMaterialFlagEnum::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ImprovementTypeWithMatFlagTokenArg::RingsHanging(rings_hanging)
            }
            "BANDS" => {
                let bands = ImprovementMaterialFlagEnum::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ImprovementTypeWithMatFlagTokenArg::Bands(bands)
            }
            "SPIKES" => {
                let spikes = ImprovementMaterialFlagEnum::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ImprovementTypeWithMatFlagTokenArg::Spikes(spikes)
            }
            // TODO: add alias warning
            "SPECIFIC" | "ITEMSPECIFIC" => {
                let specific =
                    <(ItemSpecificEnum, ImprovementMaterialFlagEnum)>::try_from_argument_group(
                        token,
                        source,
                        diagnostics,
                        add_diagnostics_on_err,
                    )?;
                ImprovementTypeWithMatFlagTokenArg::Specific(specific)
            }
            "PAGES" => {
                let pages = ImprovementMaterialFlagEnum::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ImprovementTypeWithMatFlagTokenArg::Pages(pages)
            }
            "INSTRUMENT_PIECE" => {
                let instrument_piece =
                    <(Reference, ImprovementMaterialFlagEnum)>::try_from_argument_group(
                        token,
                        source,
                        diagnostics,
                        add_diagnostics_on_err,
                    )?;
                ImprovementTypeWithMatFlagTokenArg::InstrumentPiece(instrument_piece)
            }
            "WRITING" => {
                let writing = ImprovementMaterialFlagEnum::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                ImprovementTypeWithMatFlagTokenArg::Writing(writing)
            }
            _ => {
                Self::diagnostics_wrong_enum_type(
                    &arg0?,
                    vec![
                        "COVERED",
                        "RINGS_HANGING",
                        "BANDS",
                        "SPIKES",
                        "SPECIFIC",
                        "ITEMSPECIFIC",
                        "PAGES",
                        "INSTRUMENT_PIECE",
                        "WRITING",
                    ],
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                );
                return Err(());
            }
        };
        Ok(bp_criteria)
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum ItemSpecificEnum {
    #[df_token(token_name = "ROLLERS")]
    Rollers,
    #[df_token(token_name = "HANDLE")]
    Handle,
}
impl Default for ItemSpecificEnum {
    fn default() -> Self {
        Self::Rollers
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum ImprovementMaterialFlagEnum {
    /// Permits the improvement to be made from any bone.
    #[df_token(token_name = "BONE_MAT")]
    BoneMat,
    /// Permits the improvement to be made from any ceramic material.
    #[df_token(token_name = "CERAMIC_MAT")]
    CeramicMat,
    /// Permits the improvement to be made from any glass.
    #[df_token(token_name = "GLASS_MAT")]
    GlassMat,
    /// Permits the improvement to be made from anything with the `[ITEMS_HARD]` token, such as wood, stone
    /// or metal.
    #[df_token(token_name = "HARD_MAT")]
    HardMat,
    /// Permits the improvement to be made from any leather.
    #[df_token(token_name = "LEATHER_MAT")]
    LeatherMat,
    /// Permits the improvement to be made from anything with the `[IS_METAL]` token.
    #[df_token(token_name = "METAL_MAT")]
    MetalMat,
    /// Permits the improvement to be made from any metal with the `[ITEMS_WEAPON]` token.
    #[df_token(token_name = "METAL_WEAPON_MAT")]
    MetalWeaponMat,
    /// Permits the improvement to be made from any "sheet" material, such as papyrus, paper, and
    /// parchment. May be connected to the `PAPER_SLURRY`/`PAPER_PLANT` reaction classes,
    /// but this is not verified.
    #[df_token(token_name = "SHEET_MAT")]
    SheetMat,
    /// Permits the improvement to be made from any shell.
    #[df_token(token_name = "SHELL_MAT")]
    ShellMat,
    /// Permits the improvement to be made from any silk.
    #[df_token(token_name = "SILK_MAT")]
    SilkMat,
    /// Permits the improvement to be made from any material with the `[ITEMS_SOFT]` token, such as leather
    /// or textiles.
    #[df_token(token_name = "SOFT_MAT")]
    SoftMat,
    /// Permits the improvement to be made from any stone. Presumably connected to the `[IS_STONE]` token.
    #[df_token(token_name = "STONE_MAT")]
    StoneMat,
    /// Permits the improvement to be made from any plant fiber, such as pig tails.
    #[df_token(token_name = "THREAD_PLANT_MAT")]
    ThreadPlantMat,
    /// Permits the improvement to be made from any wood.
    #[df_token(token_name = "WOOD_MAT")]
    WoodMat,
}
impl Default for ImprovementMaterialFlagEnum {
    fn default() -> Self {
        Self::BoneMat
    }
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct ToyToken {
    /// Argument 1 of `[ITEM_TOY:...]`
    #[df_token(token_name = "ITEM_TOY", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// What this item will be called in-game.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>,
    /// Presumably prevents the item from being made from cloth, silk, or leather, present on
    /// everything but puzzleboxes and drums. Appears to work backwards for strange moods.
    #[df_token(token_name = "HARD_MAT")]
    pub hard_mat: Option<()>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct TrapCompToken {
    /// Argument 1 of `[ITEM_TRAPCOMP:...]`
    #[df_token(token_name = "ITEM_TRAPCOMP", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// What this item will be called in-game.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>,
    /// How much material is needed to make the item. Is most important with bars.
    /// The number of bars needed is the value divided by three.
    #[df_token(token_name = "MATERIAL_SIZE")]
    pub material_size: Option<u32>, // TODO: required
    /// Appears before the name of the weapon's material. For example: "menacing steel spike".
    #[df_token(token_name = "ADJECTIVE")]
    pub adjective: Option<String>,
    /// How large the item is. Defaults to 100.
    #[df_token(token_name = "SIZE")]
    pub size: Option<u32>,
    /// Number of times it hits. Defaults to 1
    #[df_token(token_name = "HITS")]
    pub hits: Option<u32>,
    /// Weapon may be installed in a screw pump.
    #[df_token(token_name = "IS_SCREW")]
    pub is_screw: Option<()>,
    /// Weapon may be installed in a spike trap.
    #[df_token(token_name = "IS_SPIKE")]
    pub is_spike: Option<()>,
    /// Weapon may be made out of wood.
    #[df_token(token_name = "WOOD")]
    pub wood: Option<()>,
    /// Weapon may be made out of metal.
    #[df_token(token_name = "METAL")]
    pub metal: Option<()>,
    /// Sets the attack characteristics of the weapon
    #[df_token(token_name = "ATTACK")]
    pub attack: Option<ItemAttack>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct WeaponToken {
    /// Argument 1 of `[ITEM_WEAPON:...]`
    #[df_token(token_name = "ITEM_WEAPON", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// Name of the weapon.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String)>, // TODO: required
    /// Number of bar units needed for forging, as well as the amount gained from melting.
    #[df_token(token_name = "MATERIAL_SIZE")]
    pub material_size: Option<u32>, // TODO: Required
    /// Adjective of the weapon, e.g. the "large" in "large copper dagger".
    #[df_token(token_name = "ADJECTIVE")]
    pub adjective: Option<String>,
    /// Volume of weapon in mL or cubic cm. Defaults to 100.
    #[df_token(token_name = "SIZE")]
    pub size: Option<u32>,
    /// The amount of force used when firing projectiles - velocity is presumably determined by the
    /// projectile's mass. Defaults to 0.
    #[df_token(token_name = "SHOOT_FORCE")]
    pub shoot_force: Option<u32>,
    /// The maximum speed a fired projectile can have.
    #[df_token(token_name = "SHOOT_MAXVEL")]
    pub shoot_maxvel: Option<u32>,
    /// The skill to determine effectiveness in melee with this weapon. Defaults to `MACE`.
    ///
    /// Skill of `AXE` will allow it to be used to chop down trees. Skill of `MINER` will allow
    /// it to be used for mining.
    ///
    /// Outsider adventurers (or regular ones with no points in offensive skills) will receive a
    /// weapon with the `SPEAR` skill, selected at random if multiple ones have been modded in.
    /// All adventurers will also start with a weapon using the `DAGGER` skill, again selected at
    /// random if multiple such weapons exist.
    #[df_token(token_name = "SKILL")]
    pub skill: Option<SkillEnum>,
    /// Makes this a ranged weapon that uses the specified ammo. The specified skill determines
    /// accuracy in ranged combat.
    #[df_token(token_name = "RANGED")]
    pub ranged: Option<(SkillEnum, Reference)>, // `TODO` reference is ammo class
    /// Creatures under this size (in cm^3) must use the weapon two-handed. Defaults to 50000.
    #[df_token(token_name = "TWO_HANDED")]
    pub two_handed: Option<u32>,
    /// Minimum body size (in cm^3) to use the weapon at all (multigrasp required until `TWO_HANDED`
    /// value). Defaults to 40000.
    #[df_token(token_name = "MINIMUM_SIZE")]
    pub minimum_size: Option<u32>,
    /// Allows the weapon to be made at a craftsdwarf's workshop from a sharp (`[MAX_EDGE:10000]` or
    /// higher) stone (i.e. obsidian) plus a wood log.
    #[df_token(token_name = "CAN_STONE")]
    pub can_stone: Option<()>,
    /// Restricts this weapon to being made of wood.
    #[df_token(token_name = "TRAINING")]
    pub training: Option<()>,
    /// List of attacks this weapon can have.
    #[df_token(token_name = "ATTACK")]
    pub attack: Vec<WeaponAttack>,
}

// region: Attack definitions

/// Specifies the attack characteristics of this item.
/// - attack type: `BLUNT` or `EDGE`
/// - contact_area: value
/// - penetration_size: value
/// - verb2nd: string
/// - verb3rd: string
/// - noun: string or `NO_SUB`
/// - velocity_multiplier: value
#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct ItemAttack {
    /// Arguments of the `ATTACK` token
    #[df_token(token_name = "ATTACK")]
    pub attack: Option<(
        AttackTypeEnum,
        u32,
        u32,
        String,
        String,
        Choose<NoSubEnum, String>,
        u32,
    )>,
    /// Determines the length of time to prepare this attack and until one can perform this attack
    /// again. Values appear to be calculated in adventure mode ticks.
    #[df_token(token_name = "ATTACK_PREPARE_AND_RECOVER")]
    pub attack_prepare_and_recover: Option<(u32, u32)>,
}

/// Defines an attack on this weapon.
/// You can have many `ATTACK` tags and one will be randomly selected for each attack, with `EDGE`
/// attacks used 100 times more often than `BLUNT` attacks.
///
/// Contact area is usually high for slashing and low for bludgeoning, piercing, and poking.
/// Penetration value tends to be low for slashing and high for stabbing. Penetration is ignored if
/// attack is `BLUNT`.
/// - attack type: `BLUNT` or `EDGE`
/// - contact_area: value
/// - penetration_size: value
/// - verb2nd: string
/// - verb3rd: string
/// - noun: string or `NO_SUB`
/// - velocity_multiplier: value
#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct WeaponAttack {
    /// Arguments of the `ATTACK` token
    #[df_token(token_name = "ATTACK", on_duplicate_to_parent, primary_token)]
    pub attack: Option<(
        AttackTypeEnum,
        u32,
        u32,
        String,
        String,
        Choose<NoSubEnum, String>,
        u32,
    )>,
    /// Determines the length of time to prepare this attack and until one can perform this attack
    /// again. Values appear to be calculated in adventure mode ticks.
    #[df_token(token_name = "ATTACK_PREPARE_AND_RECOVER")]
    pub attack_prepare_and_recover: Option<(u32, u32)>,
    /// Multiple strikes with this attack cannot be performed effectively.
    #[df_token(token_name = "ATTACK_FLAG_BAD_MULTIATTACK")]
    pub attack_flag_bad_multiattack: Option<()>,
    /// Multiple strikes with this attack can be performed with no penalty.
    #[df_token(token_name = "ATTACK_FLAG_INDEPENDENT_MULTIATTACK")]
    pub attack_flag_independent_multiattack: Option<()>,
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum AttackTypeEnum {
    #[df_token(token_name = "EDGE")]
    Edge,
    #[df_token(token_name = "BLUNT")]
    Blunt,
}
impl Default for AttackTypeEnum {
    fn default() -> Self {
        Self::Edge
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum NoSubEnum {
    #[df_token(token_name = "NO_SUB")]
    NoSub,
}
impl Default for NoSubEnum {
    fn default() -> Self {
        Self::NoSub
    }
}
// endregion
