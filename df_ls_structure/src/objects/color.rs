use df_ls_core::{ReferenceTo, Referenceable};
use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct ColorToken {
    /// Argument 1 of `COLOR`
    /// The reference for the Color in other RAW files and tokens
    #[df_token(token_name = "COLOR", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// The name of the color.
    #[df_token(token_name = "NAME")]
    pub name: Option<String>,
    /// The word for the color, see the `WORD` token in `languages_X.txt`.
    #[df_token(token_name = "WORD")]
    pub word: Vec<ReferenceTo<crate::WordToken>>,
    /// The RGB (Red,Green,Blue) color value of the color.
    #[df_token(token_name = "RGB")]
    pub rgb: Option<(u8, u8, u8)>,
}
