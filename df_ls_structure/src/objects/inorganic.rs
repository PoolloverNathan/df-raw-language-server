use crate::{
    AllOrAllSolidEnum, DietInfoEnum, MaterialStateEnum, NoneEnum, OverwriteSolidEnum, SphereEnum,
    StandardPluralEnum,
};
use crate::{
    ColorToken, ItemReferenceArg, MaterialToken, MaterialTokenArg, ReactionToken, SyndromeToken,
};
use df_ls_core::{Choose, Clamp, DFChar, Reference, ReferenceTo, Referenceable};
use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct InorganicToken {
    /// Argument 1 of `[INORGANIC:...]`
    #[df_token(token_name = "INORGANIC", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    // region: Inorganic specific tokens ==========================================================
    /// Specify a material template to base this inorganic material off of.
    #[df_token(token_name = "USE_MATERIAL_TEMPLATE")]
    // TODO semantic; I think this must be first token used in the object if it's used
    pub use_material_template: Option<ReferenceTo<MaterialToken>>,
    /// Specifies what types of layers will contain this mineral.
    #[df_token(token_name = "ENVIRONMENT")]
    pub environment: Vec<(EnvClassEnum, InclusionTypeEnum, Clamp<u8, 0, 100>)>,
    /// Specifies which specific minerals will contain this mineral.
    #[df_token(token_name = "ENVIRONMENT_SPEC")]
    // TODO not sure if ReferenceTo<InorganicToken> is allowed as well or instead
    pub environment_spec: Vec<(
        ReferenceTo<MaterialToken>,
        InclusionTypeEnum,
        Clamp<u8, 0, 100>,
    )>,
    /// Used on metals, causes the metal to be made into wafers instead of
    /// [bars](https://dwarffortresswiki.org/index.php/Bar).
    #[df_token(token_name = "WAFERS")]
    pub wafers: Option<()>,
    /// Allows the ore to be [smelted](https://dwarffortresswiki.org/index.php/Smelter) into metal
    /// in the smelter. Each token with a non-zero chance causes the game to roll d100 four times,
    /// each time creating one bar of the type requested on success.
    #[df_token(token_name = "METAL_ORE")]
    pub metal_ore: Vec<(ReferenceTo<MaterialToken>, Clamp<u8, 0, 100>)>,
    /// Allows strands to be extracted from the metal at a
    /// [craftsdwarf's workshop](https://dwarffortresswiki.org/index.php/Craftsdwarf%27s_workshop).
    #[df_token(token_name = "THREAD_METAL")]
    pub thread_metal: Option<(ReferenceTo<MaterialToken>, Clamp<u8, 0, 100>)>,
    /// Found on divine materials. Presumably links the material to a god of the same sphere.
    #[df_token(token_name = "SPHERE")]
    pub sphere: Option<SphereEnum>,
    /// Used for [ceramics](https://dwarffortresswiki.org/index.php/Ceramic).
    ///
    /// Allows the material to be used in the
    /// [ceramic industry](https://dwarffortresswiki.org/index.php/Ceramic_industry).
    #[df_token(token_name = "IS_CERAMIC")]
    pub is_ceramic: Option<()>,
    /// Causes the stone to form hollow tubes leading to the
    /// [Underworld](https://dwarffortresswiki.org/index.php/Underworld). Used for
    /// [raw adamantine](https://dwarffortresswiki.org/index.php/Raw_adamantine). When mined, stone
    /// has a 100% yield. If no material with this token exists, hollow veins will instead be made
    /// of the first available inorganic, usually [iron](https://dwarffortresswiki.org/index.php/Iron).
    #[df_token(token_name = "DEEP_SPECIAL")]
    pub deep_special: Option<()>,
    /// Causes the stone to line the landscape of the
    /// [Underworld](https://dwarffortresswiki.org/index.php/Underworld). Used for
    /// [slade](https://dwarffortresswiki.org/index.php/Slade). When mined (if it's mineable), stone
    /// has a 100% yield. If no material with this token exists, other materials will be used in
    /// place of slade. Underworld spires will still be referred to as a "spire of slade" in the
    /// world's history.
    #[df_token(token_name = "DEEP_SURFACE")]
    pub deep_surface: Option<()>,
    /// Allows the stone to support an [aquifer](https://dwarffortresswiki.org/index.php/Aquifer).
    #[df_token(token_name = "AQUIFER")]
    pub aquifer: Option<()>,
    /// Causes the material to form
    /// [metamorphic layers](https://dwarffortresswiki.org/index.php/Metamorphic_layer).
    #[df_token(token_name = "METAMORPHIC")]
    pub metamorphic: Option<()>,
    /// Causes the material to form [sedimentary layers](https://dwarffortresswiki.org/index.php/Sedimentary_layer).
    #[df_token(token_name = "SEDIMENTARY")]
    pub sedimentary: Option<()>,
    /// Causes the material to form [soil](https://dwarffortresswiki.org/index.php/Soil) layers,
    /// allowing it to appear in (almost) any biome. Mining is faster and produces no stones.
    #[df_token(token_name = "SOIL")]
    pub soil: Option<()>,
    /// Causes the material to form
    /// [pelagic sediment](https://dwarffortresswiki.org/index.php/DF2014:Soil) layers beneath
    /// deep oceans. Mining is faster and produces no stones.
    #[df_token(token_name = "SOIL_OCEAN")]
    pub soil_ocean: Option<()>,
    /// Causes the material to form [sand](https://dwarffortresswiki.org/index.php/Sand) layers,
    /// allowing it to appear in [sand deserts](https://dwarffortresswiki.org/index.php/Sand_desert)
    /// and shallow [oceans](https://dwarffortresswiki.org/index.php/Ocean). Mining is faster and
    /// produces no stones. Sand layers can also be used for making
    /// [glass](https://dwarffortresswiki.org/index.php/Glass). Can be combined with `[SOIL]`.
    #[df_token(token_name = "SOIL_SAND")]
    pub soil_sand: Option<()>,
    /// Permits an already `[SEDIMENTARY]` stone layer to appear underneath shallow ocean regions.
    #[df_token(token_name = "SEDIMENTARY_OCEAN_SHALLOW")]
    pub sedimentary_ocean_shallow: Option<()>,
    /// Permits an already `[SEDIMENTARY]` stone layer to appear underneath deep ocean regions.
    #[df_token(token_name = "SEDIMENTARY_OCEAN_DEEP")]
    pub sedimentary_ocean_deep: Option<()>,
    /// Causes the material to form
    /// [igneous intrusive layers](https://dwarffortresswiki.org/index.php/Igneous_intrusive_layer).
    #[df_token(token_name = "IGNEOUS_INTRUSIVE")]
    pub igneous_intrusive: Option<()>,
    /// Causes the material to form
    /// [igneous extrusive layers](https://dwarffortresswiki.org/index.php/Igneous_extrusive_layer).
    #[df_token(token_name = "IGNEOUS_EXTRUSIVE")]
    pub igneous_extrusive: Option<()>,
    /// Specifies that the stone is created when combining
    /// [water](https://dwarffortresswiki.org/index.php/Water) and
    /// [magma](https://dwarffortresswiki.org/index.php/Magma), also causing it to line the edges of
    /// [magma](https://dwarffortresswiki.org/index.php/Magma) pools and volcanoes. If multiple
    /// minerals are marked as lava stones, a different one will be used in each biome or geological
    /// region.
    #[df_token(token_name = "LAVA")]
    pub lava: Option<()>,
    /// Prevents the material from showing up in certain places. AI-controlled entities won't use
    /// the material to make items and don't bring it in caravans, though the player can use it as
    /// normal. Also, inorganic generated creatures (forgotten beasts, titans, demons) will never be
    /// composed of this material. Explicitly set by all
    /// [evil weather](https://dwarffortresswiki.org/index.php/Evil_weather) materials and implied by
    /// `[DEEP_SURFACE]` and `[DEEP_SPECIAL]`.
    #[df_token(token_name = "SPECIAL")]
    pub special: Option<()>,
    /// Indicates that this is a generated material. Cannot be specified in user-defined raws.
    #[df_token(token_name = "GENERATED")]
    pub generated: Option<()>,
    /// Found on random-generated metals and cloth. Marks this material as usable by
    /// [Deity](https://dwarffortresswiki.org/index.php/Deity)-created generated entities.
    #[df_token(token_name = "DIVINE")]
    pub divine: Option<()>,
    // endregion ==================================================================================
    // region: Not Permitted in MatDef ============================================================
    /// Applies a prefix to all items made from the material. For `PLANT` and `CREATURE` materials,
    /// this defaults to the plant/creature name. Not permitted in material template definitions.
    #[df_token(token_name = "PREFIX")]
    pub prefix: Option<Choose<NoneEnum, String>>,
    /// Multiplies the value of the material. Not permitted in material template definitions.
    #[df_token(token_name = "MULTIPLY_VALUE")]
    pub multiply_value: Option<u32>,
    /// Changes a material's `HEATDAM_POINT`, but only if it was not set to `NONE`. Not permitted in
    /// material template definitions.
    #[df_token(token_name = "IF_EXISTS_SET_HEATDAM_POINT")]
    pub if_exists_set_heatdam_point: Option<Choose<u32, NoneEnum>>,
    /// Changes a material's `COLDDAM_POINT`, but only if it was not set to `NONE`. Not permitted in
    /// material template definitions.
    #[df_token(token_name = "IF_EXISTS_SET_COLDDAM_POINT")]
    pub if_exists_set_colddam_point: Option<Choose<u32, NoneEnum>>,
    /// Changes a material's `IGNITE_POINT`, but only if it was not set to `NONE`. Not permitted in
    /// material template definitions.
    #[df_token(token_name = "IF_EXISTS_SET_IGNITE_POINT")]
    pub if_exists_set_ignite_point: Option<Choose<u32, NoneEnum>>,
    /// Changes a material's `MELTING_POINT`, but only if it was not set to `NONE`. Not permitted in
    /// material template definitions.
    #[df_token(token_name = "IF_EXISTS_SET_MELTING_POINT")]
    pub if_exists_set_melting_point: Option<Choose<u32, NoneEnum>>,
    /// Changes a material's `BOILING_POINT`, but only if it was not set to `NONE`. Not permitted in
    /// material template definitions.
    #[df_token(token_name = "IF_EXISTS_SET_BOILING_POINT")]
    pub if_exists_set_boiling_point: Option<Choose<u32, NoneEnum>>,
    /// Changes a material's `MAT_FIXED_TEMP`, but only if it was not set to `NONE`. Not permitted
    /// in material template definitions.
    #[df_token(token_name = "IF_EXISTS_SET_MAT_FIXED_TEMP")]
    pub if_exists_set_mat_fixed_temp: Option<Choose<u32, NoneEnum>>,
    // endregion ==================================================================================
    // region: Material definition tokens =========================================================
    /// List of syndromes tied to the material.
    #[df_token(token_name = "SYNDROME")]
    pub syndrome: Vec<SyndromeToken>,
    /// Overrides the name of `BOULDER` items (i.e. mined-out stones) made of the material (used for
    /// native copper/silver/gold/platinum to make them be called "nuggets" instead of "boulders").
    #[df_token(token_name = "STONE_NAME")]
    pub stone_name: Option<String>,
    /// Used to indicate that said material is a gemstone - when tiles are mined out, rough gems
    /// will be yielded instead of boulders. Plural can be "STP" to automatically append an "s" to
    /// the singular form, and `OVERWRITE_SOLID` will override the relevant `STATE_NAME` and
    /// `STATE_ADJ` values.
    #[df_token(token_name = "IS_GEM")]
    pub is_gem: Option<(
        String,
        Choose<StandardPluralEnum, String>,
        Option<OverwriteSolidEnum>,
    )>,
    /// Specifies what the material should be treated as when drinking water contaminated by it, for
    /// generating unhappy thoughts.
    #[df_token(token_name = "TEMP_DIET_INFO")]
    pub temp_diet_info: Option<DietInfoEnum>,
    /// Allows the material to be used as dye, and defines color of dyed items.
    #[df_token(token_name = "POWDER_DYE")]
    pub powder_dye: Option<ReferenceTo<ColorToken>>,
    /// Specifies the tile that will be used to represent unmined tiles made of this material.
    /// Generally only used with stones. Defaults to 219 ('█').
    #[df_token(token_name = "TILE")]
    pub tile: Option<DFChar>,
    /// Specifies the tile that will be used to represent `BOULDER` items made of this material.
    /// Generally only used with stones. Defaults to 7 ('•').
    #[df_token(token_name = "ITEM_SYMBOL")]
    pub item_symbol: Option<DFChar>,
    /// The on-screen color of the material. Uses a standard 3-digit color token. Equivalent to
    /// `[TILE_COLOR:a:b:c]`, `[BUILD_COLOR:b:a:X]` (X = 1 if 'a' equals 'b', 0 otherwise), and
    /// `[BASIC_COLOR:a:c]`.
    #[df_token(token_name = "DISPLAY_COLOR")]
    pub display_color: Option<(u8, u8, u8)>,
    /// The color of objects made of this material which use both the foreground and background
    /// color: doors, floodgates, hatch covers, bins, barrels, and cages. Defaults to 7:7:1 (white).
    #[df_token(token_name = "BUILD_COLOR")]
    pub build_color: Option<(u8, u8, u8)>,
    /// The color of unmined tiles containing this material (for stone and soil), as well as
    /// engravings in this material. Defaults to 7:7:1 (white).
    #[df_token(token_name = "TILE_COLOR")]
    pub tile_color: Option<(u8, u8, u8)>,
    /// The color of objects made of this material which use only the foreground color, including
    /// workshops, floors and boulders, and smoothed walls. Defaults to 7:1 (white).
    #[df_token(token_name = "BASIC_COLOR")]
    pub basic_color: Option<(u8, u8)>,
    /// Determines the color of the material at the specified state. Colors come from
    /// `descriptor_color_standard.txt`. The nearest color value is used to display contaminants
    /// and body parts made of this material.
    #[df_token(token_name = "STATE_COLOR")]
    pub state_color: Vec<(
        Choose<MaterialStateEnum, AllOrAllSolidEnum>,
        ReferenceTo<ColorToken>,
    )>,
    /// Determines the name of the material at the specified state, as displayed in-game.
    #[df_token(token_name = "STATE_NAME")]
    pub state_name: Vec<(Choose<MaterialStateEnum, AllOrAllSolidEnum>, String)>,
    /// Like `STATE_NAME`, but used in different situations. Equipment made from the material uses
    /// the state adjective and not the state name.
    #[df_token(token_name = "STATE_ADJ")]
    pub state_adj: Vec<(Choose<MaterialStateEnum, AllOrAllSolidEnum>, String)>,
    /// Sets both `STATE_NAME` and `STATE_ADJ` at the same time.
    #[df_token(token_name = "STATE_NAME_ADJ")]
    pub state_name_adj: Vec<(Choose<MaterialStateEnum, AllOrAllSolidEnum>, String)>,
    /// The material's tendency to absorb liquids. Containers made of materials with nonzero
    /// absorption cannot hold liquids unless they have been glazed. Defaults to 0.
    #[df_token(token_name = "ABSORPTION")]
    pub absorption: Option<u32>,
    /// Specifies how hard of an impact (in kilopascals) the material can withstand before it will
    /// start deforming permanently. Used for blunt-force combat. Defaults to 10000.
    #[df_token(token_name = "IMPACT_YIELD")]
    pub impact_yield: Option<u32>,
    /// Specifies how hard of an impact the material can withstand before it will fail entirely.
    /// Used for blunt-force combat. Defaults to 10000.
    #[df_token(token_name = "IMPACT_FRACTURE")]
    pub impact_fracture: Option<u32>,
    /// Specifies how much the material will have given (in parts-per-100000) when the yield point
    /// is reached. Used for blunt-force combat. Defaults to 0.
    ///
    /// Apparently affects in combat whether the corresponding tissue is bruised (value >= 50000),
    /// torn (value between 25000 and 49999), or fractured (value <= 24999).
    #[df_token(token_name = "IMPACT_STRAIN_AT_YIELD")]
    #[df_alias(token_name = "IMPACT_ELASTICITY")]
    pub impact_strain_at_yield: Option<u32>,
    /// Specifies how hard the material can be compressed before it will start deforming
    /// permanently. Determines a tissue's resistance to pinching and response to strangulation.
    /// Defaults to 10000.
    #[df_token(token_name = "COMPRESSIVE_YIELD")]
    pub compressive_yield: Option<u32>,
    /// Specifies how hard the material can be compressed before it will fail entirely. Determines a
    /// tissue's resistance to pinching and response to strangulation. Defaults to 10000.
    #[df_token(token_name = "COMPRESSIVE_FRACTURE")]
    pub compressive_fracture: Option<u32>,
    /// Specifies how much the material will have given when it has been compressed to its yield
    /// point. Determines a tissue's resistance to pinching and response to strangulation. Defaults
    /// to 0.
    #[df_token(token_name = "COMPRESSIVE_STRAIN_AT_YIELD")]
    #[df_alias(token_name = "COMPRESSIVE_ELASTICITY")]
    pub compressive_strain_at_yield: Option<u32>,
    /// Specifies how hard the material can be stretched before it will start deforming permanently.
    /// Determines a tissue's resistance to a latching and tearing bite. Defaults to 10000.
    #[df_token(token_name = "TENSILE_YIELD")]
    pub tensile_yield: Option<u32>,
    /// Specifies how hard the material can be stretched before it will fail entirely. Determines a
    /// tissue's resistance to a latching and tearing bite. Defaults to 10000.
    #[df_token(token_name = "TENSILE_FRACTURE")]
    pub tensile_fracture: Option<u32>,
    /// Specifies how much the material will have given when it is stretched to its yield point.
    /// Determines a tissue's resistance to a latching and tearing bite. Defaults to 0.
    #[df_token(token_name = "TENSILE_STRAIN_AT_YIELD")]
    #[df_alias(token_name = "TENSILE_ELASTICITY")]
    pub tensile_strain_at_yield: Option<u32>,
    /// Specifies how hard the material can be twisted before it will start deforming permanently.
    /// Used for latching and shaking with a blunt attack (no default creature has such an attack,
    /// but they can be modded in). Defaults to 10000.
    #[df_token(token_name = "TORSION_YIELD")]
    pub torsion_yield: Option<u32>,
    /// Specifies how hard the material can be twisted before it will fail entirely. Used for
    /// latching and shaking with a blunt attack (no default creature has such an attack, but they
    /// can be modded in). Defaults to 10000.
    #[df_token(token_name = "TORSION_FRACTURE")]
    pub torsion_fracture: Option<u32>,
    /// Specifies how much the material will have given when it is twisted to its yield point. Used
    /// for latching and shaking with a blunt attack (no default creature has such an attack, but
    /// they can be modded in). Defaults to 0.
    #[df_token(token_name = "TORSION_STRAIN_AT_YIELD")]
    #[df_alias(token_name = "TORSION_ELASTICITY")]
    pub torsion_strain_at_yield: Option<u32>,
    /// Specifies how hard the material can be sheared before it will start deforming permanently.
    /// Used for cutting calculations. Defaults to 10000.
    #[df_token(token_name = "SHEAR_YIELD")]
    pub shear_yield: Option<u32>,
    /// Specifies how hard the material can be sheared before it will fail entirely. Used for
    /// cutting calculations. Defaults to 10000.
    #[df_token(token_name = "SHEAR_FRACTURE")]
    pub shear_fracture: Option<u32>,
    /// Specifies how much the material will have given when sheared to its yield point. Used for
    /// cutting calculations. Defaults to 0.
    #[df_token(token_name = "SHEAR_STRAIN_AT_YIELD")]
    #[df_alias(token_name = "SHEAR_ELASTICITY")]
    pub shear_strain_at_yield: Option<u32>,
    /// Specifies how hard the material can be bent before it will start deforming permanently.
    /// Determines a tissue's resistance to being mangled with a joint lock. Defaults to 10000.
    #[df_token(token_name = "BENDING_YIELD")]
    pub bending_yield: Option<u32>,
    /// Specifies how hard the material can be bent before it will fail entirely. Determines a
    /// tissue's resistance to being mangled with a joint lock. Defaults to 10000.
    #[df_token(token_name = "BENDING_FRACTURE")]
    pub bending_fracture: Option<u32>,
    /// Specifies how much the material will have given when bent to its yield point. Determines a
    /// tissue's resistance to being mangled with a joint lock. Defaults to 0.
    #[df_token(token_name = "BENDING_STRAIN_AT_YIELD")]
    #[df_alias(token_name = "BENDING_ELASTICITY")]
    pub bending_strain_at_yield: Option<u32>,
    /// How sharp the material is. Used in cutting calculations. Does not allow an inferior metal to
    /// penetrate superior armor. Applying a value of at least 10000 to a stone will allow weapons
    /// to be made from that stone. Defaults to 10000.
    #[df_token(token_name = "MAX_EDGE")]
    pub max_edge: Option<u32>,
    /// Value modifier for the material. Defaults to 1. This number can be made negative by placing
    /// a "-" in front, resulting in things that you are paid to buy and must pay to sell.
    #[df_token(token_name = "MATERIAL_VALUE")]
    pub material_value: Option<i32>,
    /// Rate at which the material heats up or cools down (in joules/kilokelvin). If set to `NONE`,
    /// the temperature will be fixed at its initial value.
    /// See [Temperature](https://dwarffortresswiki.org/index.php/Temperature) for more information.
    /// Defaults to `NONE`.
    #[df_token(token_name = "SPEC_HEAT")]
    pub spec_heat: Option<Choose<u32, NoneEnum>>,
    /// Temperature above which the material takes damage from heat. May be set to `NONE`. If the
    /// material has an ignite point but no heatdam point, it will burn for a very long time (9
    /// months and 16.8 days). Defaults to `NONE`.
    #[df_token(token_name = "HEATDAM_POINT")]
    pub heatdam_point: Option<Choose<u32, NoneEnum>>,
    /// Temperature below which the material takes damage from cold. Defaults to `NONE`.
    #[df_token(token_name = "COLDDAM_POINT")]
    pub colddam_point: Option<Choose<u32, NoneEnum>>,
    /// Temperature at which the material will catch fire. Defaults to `NONE`.
    #[df_token(token_name = "IGNITE_POINT")]
    pub ignite_point: Option<Choose<u32, NoneEnum>>,
    /// Temperature at which the material melts. Defaults to `NONE`.
    #[df_token(token_name = "MELTING_POINT")]
    pub melting_point: Option<Choose<u32, NoneEnum>>,
    /// Temperature at which the material boils. Defaults to `NONE`.
    #[df_token(token_name = "BOILING_POINT")]
    pub boiling_point: Option<Choose<u32, NoneEnum>>,
    /// Items composed of this material will initially have this temperature. Used in conjunction
    /// with `[SPEC_HEAT:NONE]` to make material's temperature fixed at the specified value.
    /// Defaults to `NONE`.
    #[df_token(token_name = "MAT_FIXED_TEMP")]
    pub mat_fixed_temp: Option<Choose<u32, NoneEnum>>,
    /// Specifies the density (in kilograms per cubic meter) of the material when in solid form.
    /// Also affects combat calculations; affects blunt-force damage and ability of edged weapons to
    /// pierce tissue layers. Defaults to `NONE`.
    #[df_token(token_name = "SOLID_DENSITY")]
    pub solid_density: Option<Choose<u32, NoneEnum>>,
    /// Specifies the density of the material when in liquid form. Defaults to `NONE`.
    #[df_token(token_name = "LIQUID_DENSITY")]
    pub liquid_density: Option<Choose<u32, NoneEnum>>,
    /// Supposedly not used. Theoretically, should determine density (at given pressure) in
    /// gas state, on which in turn would depend (together with weight of vaporized material) on the
    /// volume covered by spreading vapors. Defaults to `NONE`.
    #[df_token(token_name = "MOLAR_MASS")]
    pub molar_mass: Option<Choose<u32, NoneEnum>>,
    /// Specifies the type of container used to store the material. Used in conjunction with the
    /// `[EXTRACT_BARREL]`, `[EXTRACT_VIAL]`, or `[EXTRACT_STILL_VIAL]` plant tokens. Defaults to
    /// `BARREL`.
    #[df_token(token_name = "EXTRACT_STORAGE")]
    pub extract_storage: Option<Reference>, // TODO: ref is container type
    /// Specifies the item type used for butchering results made of this material. Stock raws use
    /// `GLOB:NONE` for fat and `MEAT:NONE` for other meat materials.
    #[df_token(token_name = "BUTCHER_SPECIAL")]
    pub butcher_special: Option<ItemReferenceArg>,
    /// When a creature is butchered, meat yielded from organs made from this material will be named
    /// via this token.
    #[df_token(token_name = "MEAT_NAME")]
    pub meat_name: Option<(Choose<NoneEnum, String>, String, String)>,
    /// Specifies the name of blocks made from this material.
    #[df_token(token_name = "BLOCK_NAME")]
    pub block_name: Option<(String, Choose<StandardPluralEnum, String>)>,
    /// Used with reaction raws to associate a reagent material with a product material. The first
    /// argument is used by `HAS_MATERIAL_REACTION_PRODUCT` and `GET_MATERIAL_FROM_REAGENT` in
    /// reaction raws. The remainder is a material reference, generally `LOCAL_CREATURE_MAT:SUBTYPE`
    /// or `LOCAL_PLANT_MAT:SUBTYPE` or `INORGANIC:STONETYPE`.
    #[df_token(token_name = "MATERIAL_REACTION_PRODUCT")]
    pub material_reaction_product: Vec<(ReferenceTo<ReactionToken>, MaterialTokenArg)>,
    /// Used with reaction raws to associate a reagent material with a complete item. The first
    /// argument is used by `HAS_ITEM_REACTION_PRODUCT` and `GET_ITEM_DATA_FROM_REAGENT` in reaction
    /// raws. The rest refers to the type of item, then its material.
    #[df_token(token_name = "ITEM_REACTION_PRODUCT")]
    pub item_reaction_product: Vec<(Reference, ItemReferenceArg, MaterialTokenArg)>,
    /// Used to classify all items made of the material, so that reactions can use them as generic
    /// reagents.
    ///
    /// In default raws, the following classes are used:
    /// - `FAT`, `TALLOW`, `SOAP`, `PARCHMENT`, `PAPER_PLANT`, `PAPER_SLURRY`, `MILK`, `CHEESE`, `WAX`
    /// - `CAN_GLAZE` - items made from this material can be glazed.
    /// - `FLUX` - can be used as flux in pig iron and steel making.
    /// - `GYPSUM` - can be processed into gypsum plaster.
    /// - `CALCIUM_CARBONATE` - can be used in production of quicklime.
    #[df_token(token_name = "REACTION_CLASS")]
    pub reaction_class: Vec<Reference>,
    /// Allows the material to be used to make casts.
    #[df_token(token_name = "HARDENS_WITH_WATER")]
    pub hardens_with_water: Option<MaterialTokenArg>,
    /// Soap has `[SOAP_LEVEL:2]`. Effects unknown. Defaults to 0.
    #[df_token(token_name = "SOAP_LEVEL")]
    pub soap_level: Option<u32>,
    // region: Material usage tokens (no args) ====================================================
    /// Lets the game know that an animal was likely killed in the production of this item. Entities
    /// opposed to killing animals (which currently does not include Elves) will refuse to accept
    /// these items in trade.
    #[df_token(token_name = "IMPLIES_ANIMAL_KILL")]
    pub implies_animal_kill: Option<()>,
    /// Classifies the material as plant-based alcohol, allowing its storage in food stockpiles
    /// under "Drink (Plant)".
    #[df_token(token_name = "ALCOHOL_PLANT")]
    pub alcohol_plant: Option<()>,
    /// Classifies the material as animal-based alcohol, allowing its storage in food stockpiles
    /// under "Drink (Animal)".
    #[df_token(token_name = "ALCOHOL_CREATURE")]
    pub alcohol_creature: Option<()>,
    /// Classifies the material as generic alcohol. Implied by both `ALCOHOL_PLANT` and
    /// `ALCOHOL_CREATURE`. Exact behavior unknown, possibly vestigial.
    #[df_token(token_name = "ALCOHOL")]
    pub alcohol: Option<()>,
    /// Classifies the material as plant-based cheese, allowing its storage in food stockpiles
    /// under "Cheese (Plant)".
    #[df_token(token_name = "CHEESE_PLANT")]
    pub cheese_plant: Option<()>,
    /// Classifies the material as animal-based cheese, allowing its storage in food stockpiles
    /// under "Cheese (Animal)".
    #[df_token(token_name = "CHEESE_CREATURE")]
    pub cheese_creature: Option<()>,
    /// Classifies the material as generic cheese. Implied by both `CHEESE_PLANT` and
    /// `CHEESE_CREATURE`. Exact behavior unknown, possibly vestigial.
    #[df_token(token_name = "CHEESE")]
    pub cheese: Option<()>,
    /// Classifies the material as plant powder, allowing its storage in food stockpiles under
    /// "Milled Plant".
    #[df_token(token_name = "POWDER_MISC_PLANT")]
    pub powder_misc_plant: Option<()>,
    /// Classifies the material as creature powder, allowing its storage in food stockpiles under
    /// "Bone Meal".
    #[df_token(token_name = "POWDER_MISC_CREATURE")]
    pub powder_misc_creature: Option<()>,
    /// Classifies the material as generic powder. Implied by both `POWDER_MISC_PLANT` and
    /// `POWDER_MISC_CREATURE`. Exact behavior unknown, possibly vestigial.
    #[df_token(token_name = "POWDER_MISC")]
    pub powder_misc: Option<()>,
    /// Permits globs of the material in solid form to be stored in food stockpiles under "Fat" -
    /// without it, dwarves will come by and "clean" the items, destroying them (unless
    /// `[DO_NOT_CLEAN_GLOB]` is also included).
    #[df_token(token_name = "STOCKPILE_GLOB")]
    #[df_alias(token_name = "STOCKPILE_GLOB_SOLID", discouraged)]
    pub stockpile_glob: Option<()>,
    /// Classifies the material as milled paste, allowing its storage in food stockpiles under
    /// "Paste".
    #[df_token(token_name = "STOCKPILE_GLOB_PASTE")]
    pub stockpile_glob_paste: Option<()>,
    /// Classifies the material as pressed goods, allowing its storage in food stockpiles under
    /// "Pressed Material".
    #[df_token(token_name = "STOCKPILE_GLOB_PRESSED")]
    pub stockpile_glob_pressed: Option<()>,
    /// Classifies the material as a plant growth (e.g. fruits, leaves), allowing its storage in
    /// food stockpiles under Plant Growth/Fruit.
    #[df_token(token_name = "STOCKPILE_PLANT_GROWTH")]
    pub stockpile_plant_growth: Option<()>,
    /// Classifies the material as a plant extract, allowing its storage in food stockpiles under
    /// "Extract (Plant)".
    #[df_token(token_name = "LIQUID_MISC_PLANT")]
    pub liquid_misc_plant: Option<()>,
    /// Classifies the material as a creature extract, allowing its storage in food stockpiles under
    /// "Extract (Animal)".
    #[df_token(token_name = "LIQUID_MISC_CREATURE")]
    pub liquid_misc_creature: Option<()>,
    /// Classifies the material as a miscellaneous liquid, allowing its storage in food stockpiles
    /// under "Misc. Liquid" along with lye.
    #[df_token(token_name = "LIQUID_MISC_OTHER")]
    pub liquid_misc_other: Option<()>,
    /// Classifies the material as a generic liquid. Implied by `LIQUID_MISC_PLANT`,
    /// `LIQUID_MISC_CREATURE`, and `LIQUID_MISC_OTHER`. Exact behavior unknown, possibly vestigial.
    #[df_token(token_name = "LIQUID_MISC")]
    pub liquid_misc: Option<()>,
    /// Classifies the material as a plant, allowing its storage in food stockpiles under "Plants".
    #[df_token(token_name = "STRUCTURAL_PLANT_MAT")]
    pub structural_plant_mat: Option<()>,
    /// Classifies the material as a plant seed, allowing its storage in food stockpiles under
    /// "Seeds".
    #[df_token(token_name = "SEED_MAT")]
    pub seed_mat: Option<()>,
    /// Classifies the material as bone, allowing its use for bone carvers and restriction from
    /// stockpiles by material.
    #[df_token(token_name = "BONE")]
    pub bone: Option<()>,
    /// Classifies the material as wood, allowing its use for carpenters and storage in wood
    /// stockpiles. Entities opposed to killing plants (i.e. Elves) will refuse to accept these
    /// items in trade.
    #[df_token(token_name = "WOOD")]
    pub wood: Option<()>,
    /// Classifies the material as plant fiber, allowing its use for clothiers and storage in cloth
    /// stockpiles under "Thread (Plant)" and "Cloth (Plant)".
    #[df_token(token_name = "THREAD_PLANT")]
    pub thread_plant: Option<()>,
    /// Classifies the material as tooth, allowing its use for bone carvers and restriction from
    /// stockpiles by material.
    #[df_token(token_name = "TOOTH")]
    pub tooth: Option<()>,
    /// Classifies the material as horn, allowing its use for bone carvers and restriction from
    /// stockpiles by material.
    #[df_token(token_name = "HORN")]
    pub horn: Option<()>,
    /// Classifies the material as pearl, allowing its use for bone carvers and restriction from
    /// stockpiles by material.
    #[df_token(token_name = "PEARL")]
    pub pearl: Option<()>,
    /// Classifies the material as shell, allowing its use for bone carvers and restriction from
    /// stockpiles by material.
    #[df_token(token_name = "SHELL")]
    pub shell: Option<()>,
    /// Classifies the material as leather, allowing its use for leatherworkers and storage in
    /// leather stockpiles.
    #[df_token(token_name = "LEATHER")]
    pub leather: Option<()>,
    /// Classifies the material as silk, allowing its use for clothiers and storage in cloth
    /// stockpiles under "Thread (Silk)" and "Cloth (Silk)".
    #[df_token(token_name = "SILK")]
    pub silk: Option<()>,
    /// Classifies the material as soap, allowing it to be used as a bath detergent and stored in
    /// bar/block stockpiles under "Bars: Other Materials".
    #[df_token(token_name = "SOAP")]
    pub soap: Option<()>,
    /// Material generates miasma when it rots.
    #[df_token(token_name = "GENERATES_MIASMA")]
    pub generates_miasma: Option<()>,
    /// Classifies the material as edible meat.
    #[df_token(token_name = "MEAT")]
    pub meat: Option<()>,
    /// Material will rot if not stockpiled appropriately. Currently only affects food and refuse,
    /// other items made of this material will not rot.
    #[df_token(token_name = "ROTS")]
    pub rots: Option<()>,
    /// Tells the game to classify contaminants of this material as being "blood" in Adventurer mode
    /// tile descriptions ("Here we have a Dwarf in a slurry of blood.").
    #[df_token(token_name = "BLOOD_MAP_DESCRIPTOR")]
    pub blood_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "ichor".
    #[df_token(token_name = "ICHOR_MAP_DESCRIPTOR")]
    pub ichor_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "goo".
    #[df_token(token_name = "GOO_MAP_DESCRIPTOR")]
    pub goo_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "slime".
    #[df_token(token_name = "SLIME_MAP_DESCRIPTOR")]
    pub slime_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "pus".
    #[df_token(token_name = "PUS_MAP_DESCRIPTOR")]
    pub pus_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "sweat".
    #[df_token(token_name = "SWEAT_MAP_DESCRIPTOR")]
    pub sweat_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "tears".
    #[df_token(token_name = "TEARS_MAP_DESCRIPTOR")]
    pub tears_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "spit".
    #[df_token(token_name = "SPIT_MAP_DESCRIPTOR")]
    pub spit_map_descriptor: Option<()>,
    /// Contaminants composed of this material evaporate over time, slowly disappearing from the
    /// map. Used internally by water.
    #[df_token(token_name = "EVAPORATES")]
    pub evaporates: Option<()>,
    /// Used for materials which cause syndromes, causes it to enter the creature's blood instead of
    /// simply spattering on the surface.
    #[df_token(token_name = "ENTERS_BLOOD")]
    pub enters_blood: Option<()>,
    /// Can be eaten by vermin.
    #[df_token(token_name = "EDIBLE_VERMIN")]
    pub edible_vermin: Option<()>,
    /// Can be eaten raw.
    #[df_token(token_name = "EDIBLE_RAW")]
    pub edible_raw: Option<()>,
    /// Can be cooked and then eaten.
    #[df_token(token_name = "EDIBLE_COOKED")]
    pub edible_cooked: Option<()>,
    /// Prevents globs made of this material from being cleaned up and destroyed.
    #[df_token(token_name = "DO_NOT_CLEAN_GLOB")]
    pub do_not_clean_glob: Option<()>,
    /// Prevents the material from showing up in Stone stockpile settings.
    #[df_token(token_name = "NO_STONE_STOCKPILE")]
    pub no_stone_stockpile: Option<()>,
    /// Allows the creation of metal furniture at the metalsmith's forge.
    #[df_token(token_name = "ITEMS_METAL")]
    pub items_metal: Option<()>,
    /// Equivalent to `ITEMS_HARD`. Given to bone.
    #[df_token(token_name = "ITEMS_BARRED")]
    pub items_barred: Option<()>,
    /// Equivalent to `ITEMS_HARD`. Given to shell.
    #[df_token(token_name = "ITEMS_SCALED")]
    pub items_scaled: Option<()>,
    /// Equivalent to `ITEMS_SOFT`. Given to leather.
    #[df_token(token_name = "ITEMS_LEATHER")]
    pub items_leather: Option<()>,
    /// Random crafts made from this material cannot be made into rings, crowns, scepters or
    /// figurines. Given to plant fiber, silk and wool.
    #[df_token(token_name = "ITEMS_SOFT")]
    pub items_soft: Option<()>,
    /// Random crafts made from this material include all seven items. Given to stone, wood, bone,
    /// shell, chitin, claws, teeth, horns, hooves and beeswax. Hair, pearls and eggshells also have
    /// the tag.
    #[df_token(token_name = "ITEMS_HARD")]
    pub items_hard: Option<()>,
    /// Used to define that the material is a stone. Allows its usage in masonry and stonecrafting
    /// and storage in stone stockpiles, among other effects.
    #[df_token(token_name = "IS_STONE")]
    pub is_stone: Option<()>,
    /// Used for a stone that cannot be dug into.
    #[df_token(token_name = "UNDIGGABLE")]
    pub undiggable: Option<()>,
    /// Causes containers made of this material to be prefixed with "unglazed" if they have not yet
    /// been glazed.
    #[df_token(token_name = "DISPLAY_UNGLAZED")]
    pub display_unglazed: Option<()>,
    /// Classifies the material as yarn, allowing its use for clothiers and its storage in cloth
    /// stockpiles under "Thread (Yarn)" and "Cloth (Yarn)".
    #[df_token(token_name = "YARN")]
    pub yarn: Option<()>,
    /// Classifies the material as metal thread, permitting thread and cloth to be stored in cloth
    /// stockpiles under "Thread (Metal)" and "Cloth (Metal)".
    #[df_token(token_name = "STOCKPILE_THREAD_METAL")]
    pub stockpile_thread_metal: Option<()>,
    /// Defines the material as being metal, allowing it to be used at forges.
    #[df_token(token_name = "IS_METAL")]
    pub is_metal: Option<()>,
    /// Used internally by green glass, clear glass, and crystal glass.
    #[df_token(token_name = "IS_GLASS")]
    pub is_glass: Option<()>,
    /// Can be used in the production of crystal glass.
    #[df_token(token_name = "CRYSTAL_GLASSABLE")]
    pub crystal_glassable: Option<()>,
    /// Melee weapons can be made out of this material.
    #[df_token(token_name = "ITEMS_WEAPON")]
    pub items_weapon: Option<()>,
    /// Ranged weapons can be made out of this material.
    #[df_token(token_name = "ITEMS_WEAPON_RANGED")]
    pub items_weapon_ranged: Option<()>,
    /// Anvils can be made out of this material.
    #[df_token(token_name = "ITEMS_ANVIL")]
    pub items_anvil: Option<()>,
    /// Ammunition can be made out of this material.
    #[df_token(token_name = "ITEMS_AMMO")]
    pub items_ammo: Option<()>,
    /// Picks can be made out of this material.
    #[df_token(token_name = "ITEMS_DIGGER")]
    pub items_digger: Option<()>,
    /// Armor can be made out of this material.
    #[df_token(token_name = "ITEMS_ARMOR")]
    pub items_armor: Option<()>,
    /// Used internally by amber and coral. Functionally equivalent to `ITEMS_HARD`.
    #[df_token(token_name = "ITEMS_DELICATE")]
    pub items_delicate: Option<()>,
    /// Siege engine parts can be made out of this material. Does not appear to work.
    #[df_token(token_name = "ITEMS_SIEGE_ENGINE")]
    pub items_siege_engine: Option<()>,
    /// Querns and millstones can be made out of this material.
    #[df_token(token_name = "ITEMS_QUERN")]
    pub items_quern: Option<()>,
    // endregion ==================================================================================
    // endregion ==================================================================================
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum EnvClassEnum {
    /// Will appear in every stone.
    #[df_token(token_name = "ALL_STONE")]
    AllStone,
    /// Will appear in igneous layers, both
    /// [intrusive](https://dwarffortresswiki.org/index.php/Igneous_intrusive_layer) and
    /// [extrusive](https://dwarffortresswiki.org/index.php/Igneous_extrusive_layer).
    #[df_token(token_name = "IGNEOUS_ALL")]
    IgneousAll,
    /// Will appear in
    /// [igneous intrusive](https://dwarffortresswiki.org/index.php/Igneous_intrusive_layer) layers.
    #[df_token(token_name = "IGNEOUS_INTRUSIVE")]
    IgneousIntrusive,
    /// Will appear in
    /// [igneous extrusive](https://dwarffortresswiki.org/index.php/Igneous_extrusive_layer) layers.
    #[df_token(token_name = "IGNEOUS_EXTRUSIVE")]
    IgneousExtrusive,
    /// Will appear in [soil](https://dwarffortresswiki.org/index.php/Soil).
    #[df_token(token_name = "SOIL")]
    Soil,
    /// Will appear in [soil](https://dwarffortresswiki.org/index.php/Soil) in oceans specifically.
    #[df_token(token_name = "SOIL_OCEAN")]
    SoilOcean,
    /// Will appear in sand type [soils](https://dwarffortresswiki.org/index.php/Soil).
    #[df_token(token_name = "SOIL_SAND")]
    SoilSand,
    /// Will appear in [metamorphic](https://dwarffortresswiki.org/index.php/Metamorphic) layers.
    #[df_token(token_name = "METAMORPHIC")]
    Metamorphic,
    /// Will appear in [sedimentary](https://dwarffortresswiki.org/index.php/Sedimentary) layers.
    #[df_token(token_name = "SEDIMENTARY")]
    Sedimentary,
    /// Will appear in [alluvial](https://dwarffortresswiki.org/index.php/Alluvial) layers.
    #[df_token(token_name = "ALLUVIAL")]
    Alluvial,
}
impl Default for EnvClassEnum {
    fn default() -> Self {
        Self::AllStone
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum InclusionTypeEnum {
    /// Large ovoids that occupy their entire 48x48 embark tile.
    /// [Microcline](https://dwarffortresswiki.org/index.php/Microcline) is an example from vanilla.
    ///
    /// When mined, stone has a 25% yield (as with layer stones).
    #[df_token(token_name = "CLUSTER")]
    Cluster,
    /// Blobs of 3-9 tiles. Will always be successfully mined.
    /// [Red pyropes](https://dwarffortresswiki.org/index.php/Red_pyrope) are an example from vanilla.
    ///
    /// When mined, stone has a 100% yield.
    #[df_token(token_name = "CLUSTER_SMALL")]
    ClusterSmall,
    /// Single tiles. Will always be successfully mined.
    /// [Clear diamonds](https://dwarffortresswiki.org/index.php/Clear_diamond) are an example from vanilla.
    ///
    /// When mined, stone has a 100% yield.
    #[df_token(token_name = "CLUSTER_ONE")]
    ClusterOne,
    /// Large streaks of stone. [Native gold](https://dwarffortresswiki.org/index.php/Native_gold)
    /// is an example from vanilla.
    ///
    /// When mined, stone has a 33% yield instead of the usual 25%.
    #[df_token(token_name = "VEIN")]
    Vein,
}
impl Default for InclusionTypeEnum {
    fn default() -> Self {
        Self::Cluster
    }
}
