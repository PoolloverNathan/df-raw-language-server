use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

// TODO: maybe add descriptions for these where possible/practical;
// copy from applicable creature tokens.
#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum CreatureFlagEnum {
    #[df_token(token_name = "ALL_CASTES_ALIVE")]
    AllCastesAlive,
    #[df_token(token_name = "ARTIFICIAL_HIVEABLE")]
    ArtificialHiveable,
    #[df_token(token_name = "BIOME_DESERT_BADLAND")]
    BiomeDesertBadland,
    #[df_token(token_name = "BIOME_DESERT_ROCK")]
    BiomeDesertRock,
    #[df_token(token_name = "BIOME_DESERT_SAND")]
    BiomeDesertSand,
    #[df_token(token_name = "BIOME_FOREST_TAIGA")]
    BiomeForestTaiga,
    #[df_token(token_name = "BIOME_FOREST_TEMPERATE_BROADLEAF")]
    BiomeForestTemperateBroadleaf,
    #[df_token(token_name = "BIOME_FOREST_TEMPERATE_CONIFER")]
    BiomeForestTemperateConifer,
    #[df_token(token_name = "BIOME_FOREST_TROPICAL_CONIFER")]
    BiomeForestTropicalConifer,
    #[df_token(token_name = "BIOME_FOREST_TROPICAL_DRY_BROADLEAF")]
    BiomeForestTropicalDryBroadleaf,
    #[df_token(token_name = "BIOME_FOREST_TROPICAL_MOIST_BROADLEAF")]
    BiomeForestTropicalMoistBroadleaf,
    #[df_token(token_name = "BIOME_GLACIER")]
    BiomeGlacier,
    #[df_token(token_name = "BIOME_GRASSLAND_TEMPERATE")]
    BiomeGrasslandTemperate,
    #[df_token(token_name = "BIOME_GRASSLAND_TROPICAL")]
    BiomeGrasslandTropical,
    #[df_token(token_name = "BIOME_LAKE_TEMPERATE_BRACKISHWATER")]
    BiomeLakeTemperateBrackishwater,
    #[df_token(token_name = "BIOME_LAKE_TEMPERATE_FRESHWATER")]
    BiomeLakeTemperateFreshwater,
    #[df_token(token_name = "BIOME_LAKE_TEMPERATE_SALTWATER")]
    BiomeLakeTemperateSaltwater,
    #[df_token(token_name = "BIOME_LAKE_TROPICAL_BRACKISHWATER")]
    BiomeLakeTropicalBrackishwater,
    #[df_token(token_name = "BIOME_LAKE_TROPICAL_FRESHWATER")]
    BiomeLakeTropicalFreshwater,
    #[df_token(token_name = "BIOME_LAKE_TROPICAL_SALTWATER")]
    BiomeLakeTropicalSaltwater,
    #[df_token(token_name = "BIOME_MARSH_TEMPERATE_FRESHWATER")]
    BiomeMarshTemperateFreshwater,
    #[df_token(token_name = "BIOME_MARSH_TEMPERATE_SALTWATER")]
    BiomeMarshTemperateSaltwater,
    #[df_token(token_name = "BIOME_MARSH_TROPICAL_FRESHWATER")]
    BiomeMarshTropicalFreshwater,
    #[df_token(token_name = "BIOME_MARSH_TROPICAL_SALTWATER")]
    BiomeMarshTropicalSaltwater,
    #[df_token(token_name = "BIOME_MOUNTAIN")]
    BiomeMountain,
    #[df_token(token_name = "BIOME_OCEAN_ARCTIC")]
    BiomeOceanArctic,
    #[df_token(token_name = "BIOME_OCEAN_TEMPERATE")]
    BiomeOceanTemperate,
    #[df_token(token_name = "BIOME_OCEAN_TROPICAL")]
    BiomeOceanTropical,
    #[df_token(token_name = "BIOME_POOL_TEMPERATE_BRACKISHWATER")]
    BiomePoolTemperateBrackishwater,
    #[df_token(token_name = "BIOME_POOL_TEMPERATE_FRESHWATER")]
    BiomePoolTemperateFreshwater,
    #[df_token(token_name = "BIOME_POOL_TEMPERATE_SALTWATER")]
    BiomePoolTemperateSaltwater,
    #[df_token(token_name = "BIOME_POOL_TROPICAL_BRACKISHWATER")]
    BiomePoolTropicalBrackishwater,
    #[df_token(token_name = "BIOME_POOL_TROPICAL_FRESHWATER")]
    BiomePoolTropicalFreshwater,
    #[df_token(token_name = "BIOME_POOL_TROPICAL_SALTWATER")]
    BiomePoolTropicalSaltwater,
    #[df_token(token_name = "BIOME_RIVER_TEMPERATE_BRACKISHWATER")]
    BiomeRiverTemperateBrackishwater,
    #[df_token(token_name = "BIOME_RIVER_TEMPERATE_FRESHWATER")]
    BiomeRiverTemperateFreshwater,
    #[df_token(token_name = "BIOME_RIVER_TEMPERATE_SALTWATER")]
    BiomeRiverTemperateSaltwater,
    #[df_token(token_name = "BIOME_RIVER_TROPICAL_BRACKISHWATER")]
    BiomeRiverTropicalBrackishwater,
    #[df_token(token_name = "BIOME_RIVER_TROPICAL_FRESHWATER")]
    BiomeRiverTropicalFreshwater,
    #[df_token(token_name = "BIOME_RIVER_TROPICAL_SALTWATER")]
    BiomeRiverTropicalSaltwater,
    #[df_token(token_name = "BIOME_SAVANNA_TEMPERATE")]
    BiomeSavannaTemperate,
    #[df_token(token_name = "BIOME_SAVANNA_TROPICAL")]
    BiomeSavannaTropical,
    #[df_token(token_name = "BIOME_SHRUBLAND_TEMPERATE")]
    BiomeShrublandTemperate,
    #[df_token(token_name = "BIOME_SHRUBLAND_TROPICAL")]
    BiomeShrublandTropical,
    #[df_token(token_name = "BIOME_SUBTERRANEAN_CHASM")]
    BiomeSubterraneanChasm,
    #[df_token(token_name = "BIOME_SUBTERRANEAN_LAVA")]
    BiomeSubterraneanLava,
    #[df_token(token_name = "BIOME_SUBTERRANEAN_WATER")]
    BiomeSubterraneanWater,
    #[df_token(token_name = "BIOME_SWAMP_MANGROVE")]
    BiomeSwampMangrove,
    #[df_token(token_name = "BIOME_SWAMP_TEMPERATE_FRESHWATER")]
    BiomeSwampTemperateFreshwater,
    #[df_token(token_name = "BIOME_SWAMP_TEMPERATE_SALTWATER")]
    BiomeSwampTemperateSaltwater,
    #[df_token(token_name = "BIOME_SWAMP_TROPICAL_FRESHWATER")]
    BiomeSwampTropicalFreshwater,
    #[df_token(token_name = "BIOME_SWAMP_TROPICAL_SALTWATER")]
    BiomeSwampTropicalSaltwater,
    #[df_token(token_name = "BIOME_TUNDRA")]
    BiomeTundra,
    #[df_token(token_name = "DOES_NOT_EXIST")]
    DoesNotExist,
    #[df_token(token_name = "EQUIPMENT")]
    Equipment,
    #[df_token(token_name = "EQUIPMENT_WAGON")]
    EquipmentWagon,
    #[df_token(token_name = "EVIL")]
    Evil,
    #[df_token(token_name = "FANCIFUL")]
    Fanciful,
    #[df_token(token_name = "GENERATED")]
    Generated,
    #[df_token(token_name = "GOOD")]
    Good,
    #[df_token(token_name = "HAS_ANY_BENIGN")]
    HasAnyBenign,
    #[df_token(token_name = "HAS_ANY_CANNOT_BREATHE_AIR")]
    HasAnyCannotBreatheAir,
    #[df_token(token_name = "HAS_ANY_CANNOT_BREATHE_WATER")]
    HasAnyCannotBreatheWater,
    #[df_token(token_name = "HAS_ANY_CAN_SWIM")]
    HasAnyCanSwim,
    #[df_token(token_name = "HAS_ANY_CARNIVORE")]
    HasAnyCarnivore,
    #[df_token(token_name = "HAS_ANY_COMMON_DOMESTIC")]
    HasAnyCommonDomestic,
    #[df_token(token_name = "HAS_ANY_CURIOUS_BEAST")]
    HasAnyCuriousBeast,
    #[df_token(token_name = "HAS_ANY_DEMON")]
    HasAnyDemon,
    #[df_token(token_name = "HAS_ANY_FEATURE_BEAST")]
    HasAnyFeatureBeast,
    #[df_token(token_name = "HAS_ANY_FLIER")]
    HasAnyFlier,
    #[df_token(token_name = "HAS_ANY_FLY_RACE_GAIT")]
    HasAnyFlyRaceGait,
    #[df_token(token_name = "HAS_ANY_GRASP")]
    HasAnyGrasp,
    #[df_token(token_name = "HAS_ANY_GRAZER")]
    HasAnyGrazer,
    #[df_token(token_name = "HAS_ANY_HAS_BLOOD")]
    HasAnyHasBlood,
    #[df_token(token_name = "HAS_ANY_IMMOBILE")]
    HasAnyImmobile,
    #[df_token(token_name = "HAS_ANY_INTELLIGENT_LEARNS")]
    HasAnyIntelligentLearns,
    #[df_token(token_name = "HAS_ANY_INTELLIGENT_SPEAKS")]
    HasAnyIntelligentSpeaks,
    #[df_token(token_name = "HAS_ANY_LARGE_PREDATOR")]
    HasAnyLargePredator,
    #[df_token(token_name = "HAS_ANY_LOCAL_POPS_CONTROLLABLE")]
    HasAnyLocalPopsControllable,
    #[df_token(token_name = "HAS_ANY_LOCAL_POPS_PRODUCE_HEROES")]
    HasAnyLocalPopsProduceHeroes,
    #[df_token(token_name = "HAS_ANY_MEGABEAST")]
    HasAnyMegabeast,
    #[df_token(token_name = "HAS_ANY_MISCHIEVIOUS")]
    HasAnyMischievious,
    #[df_token(token_name = "HAS_ANY_NATURAL_ANIMAL")]
    HasAnyNaturalAnimal,
    #[df_token(token_name = "HAS_ANY_NIGHT_CREATURE")]
    HasAnyNightCreature,
    #[df_token(token_name = "HAS_ANY_NIGHT_CREATURE_BOGEYMAN")]
    HasAnyNightCreatureBogeyman,
    #[df_token(token_name = "HAS_ANY_NIGHT_CREATURE_EXPERIMENTER")]
    HasAnyNightCreatureExperimenter,
    #[df_token(token_name = "HAS_ANY_NIGHT_CREATURE_HUNTER")]
    HasAnyNightCreatureHunter,
    #[df_token(token_name = "HAS_ANY_NIGHT_CREATURE_NIGHTMARE")]
    HasAnyNightCreatureNightmare,
    #[df_token(token_name = "HAS_ANY_NOT_FIREIMMUNE")]
    HasAnyNotFireimmune,
    #[df_token(token_name = "HAS_ANY_NOT_FLIER")]
    HasAnyNotFlier,
    #[df_token(token_name = "HAS_ANY_NOT_LIVING")]
    HasAnyNotLiving,
    #[df_token(token_name = "HAS_ANY_OUTSIDER_CONTROLLABLE")]
    HasAnyOutsiderControllable,
    #[df_token(token_name = "HAS_ANY_POWER")]
    HasAnyPower,
    #[df_token(token_name = "HAS_ANY_RACE_GAIT")]
    HasAnyRaceGait,
    #[df_token(token_name = "HAS_ANY_SEMIMEGABEAST")]
    HasAnySemimegabeast,
    #[df_token(token_name = "HAS_ANY_SLOW_LEARNER")]
    HasAnySlowLearner,
    #[df_token(token_name = "HAS_ANY_SUPERNATURAL")]
    HasAnySupernatural,
    #[df_token(token_name = "HAS_ANY_TITAN")]
    HasAnyTitan,
    #[df_token(token_name = "HAS_ANY_UNIQUE_DEMON")]
    HasAnyUniqueDemon,
    #[df_token(token_name = "HAS_ANY_UTTERANCES")]
    HasAnyUtterances,
    #[df_token(token_name = "HAS_ANY_VERMIN_HATEABLE")]
    HasAnyVerminHateable,
    #[df_token(token_name = "HAS_ANY_VERMIN_MICRO")]
    HasAnyVerminMicro,
    #[df_token(token_name = "HAS_FEMALE")]
    HasFemale,
    #[df_token(token_name = "HAS_MALE")]
    HasMale,
    #[df_token(token_name = "LARGE_ROAMING")]
    LargeRoaming,
    #[df_token(token_name = "LOOSE_CLUSTERS")]
    LooseClusters,
    #[df_token(token_name = "MATES_TO_BREED")]
    MatesToBreed,
    #[df_token(token_name = "MUNDANE")]
    Mundane,
    #[df_token(token_name = "OCCURS_AS_ENTITY_RACE")]
    OccursAsEntityRace,
    #[df_token(token_name = "SAVAGE")]
    Savage,
    /// Applies to any vermin creature.
    #[df_token(token_name = "SMALL_RACE")]
    SmallRace,
    #[df_token(token_name = "TWO_GENDERS")]
    TwoGenders,
    #[df_token(token_name = "UBIQUITOUS")]
    Ubiquitous,
    #[df_token(token_name = "VERMIN_EATER")]
    VerminEater,
    #[df_token(token_name = "VERMIN_FISH")]
    VerminFish,
    #[df_token(token_name = "VERMIN_GROUNDER")]
    VerminGrounder,
    #[df_token(token_name = "VERMIN_ROTTER")]
    VerminRotter,
    #[df_token(token_name = "VERMIN_SOIL")]
    VerminSoil,
    #[df_token(token_name = "VERMIN_SOIL_COLONY")]
    VerminSoilColony,
}
impl Default for CreatureFlagEnum {
    fn default() -> Self {
        Self::AllCastesAlive
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum CasteFlagEnum {
    #[df_token(token_name = "ADOPTS_OWNER")]
    AdoptsOwner,
    #[df_token(token_name = "ALCOHOL_DEPENDENT")]
    AlcoholDependent,
    #[df_token(token_name = "ALL_ACTIVE")]
    AllActive,
    #[df_token(token_name = "AMBUSHPREDATOR")]
    Ambushpredator,
    #[df_token(token_name = "AQUATIC_UNDERSWIM")]
    AquaticUnderswim,
    #[df_token(token_name = "ARENA_RESTRICTED")]
    ArenaRestricted,
    #[df_token(token_name = "AT_PEACE_WITH_WILDLIFE")]
    AtPeaceWithWildlife,
    #[df_token(token_name = "BENIGN")]
    Benign,
    #[df_token(token_name = "BLOODSUCKER")]
    Bloodsucker,
    #[df_token(token_name = "BONECARN")]
    BoneCarn,
    #[df_token(token_name = "CANNOT_BREATHE_AIR")]
    CannotBreatheAir,
    #[df_token(token_name = "CANNOT_CLIMB")]
    CannotClimb,
    #[df_token(token_name = "CANNOT_JUMP")]
    CannotJump,
    #[df_token(token_name = "CANOPENDOORS")]
    CanOpenDoors,
    #[df_token(token_name = "CAN_BREATHE_WATER")]
    CanBreatheWater,
    #[df_token(token_name = "CAN_LEARN")]
    #[df_alias(token_name = "INTELLIGENT_LEARNS", discouraged)]
    CanLearn,
    #[df_token(token_name = "CAN_SPEAK")]
    #[df_alias(token_name = "INTELLIGENT_SPEAKS", discouraged)]
    CanSpeak,
    #[df_token(token_name = "CAN_SWIM")]
    CanSwim,
    #[df_token(token_name = "CAN_SWIM_INNATE")]
    CanSwimInnate,
    #[df_token(token_name = "CARNIVORE")]
    Carnivore,
    #[df_token(token_name = "CAVE_ADAPT")]
    CaveAdapt,
    #[df_token(token_name = "COLONY_EXTERNAL")]
    ColonyExternal,
    #[df_token(token_name = "COMMON_DOMESTIC")]
    CommonDomestic,
    #[df_token(token_name = "CONVERTED_SPOUSE")]
    ConvertedSpouse,
    #[df_token(token_name = "COOKABLE_LIVE")]
    CookableLive,
    #[df_token(token_name = "CRAZED")]
    Crazed,
    #[df_token(token_name = "CREPUSCULAR")]
    Crepuscular,
    #[df_token(token_name = "CURIOUS_BEAST")]
    CuriousBeast,
    #[df_token(token_name = "CURIOUS_BEAST_EATER")]
    CuriousBeastEater,
    #[df_token(token_name = "CURIOUS_BEAST_GUZZLER")]
    CuriousBeastGuzzler,
    #[df_token(token_name = "CURIOUS_BEAST_ITEM")]
    CuriousBeastItem,
    #[df_token(token_name = "DEMON")]
    Demon,
    #[df_token(token_name = "DIE_WHEN_VERMIN_BITE")]
    DieWhenVerminBite,
    #[df_token(token_name = "DIURNAL")]
    Diurnal,
    #[df_token(token_name = "DIVE_HUNTS_VERMIN")]
    DiveHuntsVermin,
    #[df_token(token_name = "EQUIPS")]
    Equips,
    #[df_token(token_name = "EXTRAVISION")]
    Extravision,
    #[df_token(token_name = "FEATURE_ATTACK_GROUP")]
    FeatureAttackGroup,
    #[df_token(token_name = "FEATURE_BEAST")]
    FeatureBeast,
    #[df_token(token_name = "FIREIMMUNE")]
    Fireimmune,
    #[df_token(token_name = "FIREIMMUNE_SUPER")]
    FireimmuneSuper,
    #[df_token(token_name = "FISHITEM")]
    FishItem,
    #[df_token(token_name = "FLEEQUICK")]
    FleeQuick,
    #[df_token(token_name = "FLIER")]
    Flier,
    #[df_token(token_name = "GELDABLE")]
    Geldable,
    #[df_token(token_name = "GETS_INFECTIONS_FROM_ROT")]
    GetsInfectionsFromRot,
    #[df_token(token_name = "GETS_WOUND_INFECTIONS")]
    GetsWoundInfections,
    #[df_token(token_name = "GNAWER")]
    Gnawer,
    #[df_token(token_name = "GRAZER")]
    Grazer,
    #[df_token(token_name = "HASSHELL")]
    HasShell,
    #[df_token(token_name = "HAS_BABYSTATE")]
    HasBabystate,
    #[df_token(token_name = "HAS_BLOOD")]
    HasBlood,
    #[df_token(token_name = "HAS_CHILDSTATE")]
    HasChildstate,
    #[df_token(token_name = "HAS_COLOR")]
    HasColor,
    #[df_token(token_name = "HAS_FLY_RACE_GAIT")]
    HasFlyRaceGait,
    #[df_token(token_name = "HAS_GLOW_COLOR")]
    HasGlowColor,
    #[df_token(token_name = "HAS_GLOW_TILE")]
    HasGlowTile,
    #[df_token(token_name = "HAS_GRASP")]
    HasGrasp,
    #[df_token(token_name = "HAS_NERVES")]
    HasNerves,
    #[df_token(token_name = "HAS_PUS")]
    HasPus,
    #[df_token(token_name = "HAS_RACE_GAIT")]
    HasRaceGait,
    #[df_token(token_name = "HAS_ROTTABLE")]
    HasRottable,
    #[df_token(token_name = "HAS_SECRETION")]
    HasSecretion,
    #[df_token(token_name = "HAS_SOLDIER_TILE")]
    HasSoldierTile,
    #[df_token(token_name = "HAS_SOUND_ALERT")]
    HasSoundAlert,
    #[df_token(token_name = "HAS_SOUND_PEACEFUL_INTERMITTENT")]
    HasSoundPeacefulIntermittent,
    #[df_token(token_name = "HAS_TILE")]
    HasTile,
    #[df_token(token_name = "HUNTS_VERMIN")]
    HuntsVermin,
    #[df_token(token_name = "IMMOBILE")]
    Immobile,
    #[df_token(token_name = "IMMOBILE_LAND")]
    ImmobileLand,
    #[df_token(token_name = "IMMOLATE")]
    Immolate,
    #[df_token(token_name = "ITEMCORPSE")]
    ItemCorpse,
    #[df_token(token_name = "LAIR_HUNTER")]
    LairHunter,
    #[df_token(token_name = "LARGE_PREDATOR")]
    LargePredator,
    #[df_token(token_name = "LAYS_EGGS")]
    LaysEggs,
    #[df_token(token_name = "LAYS_UNUSUAL_EGGS")]
    LaysUnusualEggs,
    #[df_token(token_name = "LIGAMENTS")]
    Ligaments,
    #[df_token(token_name = "LIGHT_GEN")]
    LightGen,
    #[df_token(token_name = "LISP")]
    Lisp,
    #[df_token(token_name = "LOCAL_POPS_CONTROLLABLE")]
    LocalPopsControllable,
    #[df_token(token_name = "LOCAL_POPS_PRODUCE_HEROES")]
    LocalPopsProduceHeroes,
    #[df_token(token_name = "LOCKPICKER")]
    Lockpicker,
    #[df_token(token_name = "MAGICAL")]
    Magical,
    #[df_token(token_name = "MAGMA_VISION")]
    MagmaVision,
    #[df_token(token_name = "MANNERISM_BREATH")]
    MannerismBreath,
    #[df_token(token_name = "MANNERISM_EYELIDS")]
    MannerismEyelids,
    #[df_token(token_name = "MANNERISM_LAUGH")]
    MannerismLaugh,
    #[df_token(token_name = "MANNERISM_POSTURE")]
    MannerismPosture,
    #[df_token(token_name = "MANNERISM_SIT")]
    MannerismSit,
    #[df_token(token_name = "MANNERISM_SMILE")]
    MannerismSmile,
    #[df_token(token_name = "MANNERISM_STRETCH")]
    MannerismStretch,
    #[df_token(token_name = "MANNERISM_WALK")]
    MannerismWalk,
    #[df_token(token_name = "MATUTINAL")]
    Matutinal,
    #[df_token(token_name = "MEANDERER")]
    Meanderer,
    #[df_token(token_name = "MEGABEAST")]
    Megabeast,
    #[df_token(token_name = "MILKABLE")]
    Milkable,
    #[df_token(token_name = "MISCHIEVIOUS")]
    Mischievious,
    #[df_token(token_name = "MOUNT")]
    Mount,
    #[df_token(token_name = "MOUNT_EXOTIC")]
    MountExotic,
    #[df_token(token_name = "MULTIPART_FULL_VISION")]
    MultipartFullVision,
    #[df_token(token_name = "MULTIPLE_LITTER_RARE")]
    MultipleLitterRare,
    #[df_token(token_name = "NATURAL_ANIMAL")]
    NaturalAnimal,
    #[df_token(token_name = "NIGHT_CREATURE")]
    NightCreature,
    #[df_token(token_name = "NIGHT_CREATURE_BOGEYMAN")]
    NightCreatureBogeyman,
    #[df_token(token_name = "NIGHT_CREATURE_EXPERIMENTER")]
    NightCreatureExperimenter,
    #[df_token(token_name = "NIGHT_CREATURE_HUNTER")]
    NightCreatureHunter,
    #[df_token(token_name = "NIGHT_CREATURE_NIGHTMARE")]
    NightCreatureNightmare,
    #[df_token(token_name = "NOBONES")]
    NoBones,
    #[df_token(token_name = "NOBREATHE")]
    NoBreathe,
    #[df_token(token_name = "NOCTURNAL")]
    Nocturnal,
    #[df_token(token_name = "NOEMOTION")]
    NoEmotion,
    #[df_token(token_name = "NOEXERT")]
    NoExert,
    #[df_token(token_name = "NOFEAR")]
    NoFear,
    #[df_token(token_name = "NOMEAT")]
    NoMeat,
    #[df_token(token_name = "NONAUSEA")]
    NoNausea,
    #[df_token(token_name = "NOPAIN")]
    NoPain,
    #[df_token(token_name = "NOSKIN")]
    NoSkin,
    #[df_token(token_name = "NOSKULL")]
    NoSkull,
    #[df_token(token_name = "NOSMELLYROT")]
    NoSmellyRot,
    #[df_token(token_name = "NOSTUCKINS")]
    NoStuckins,
    #[df_token(token_name = "NOSTUN")]
    NoStun,
    #[df_token(token_name = "NOTHOUGHT")]
    NoThought,
    #[df_token(token_name = "NOT_BUTCHERABLE")]
    NotButcherable,
    #[df_token(token_name = "NOT_LIVING")]
    NotLiving,
    #[df_token(token_name = "NO_AUTUMN")]
    NoAutumn,
    #[df_token(token_name = "NO_CONNECTIONS_FOR_MOVEMENT")]
    NoConnectionsForMovement,
    #[df_token(token_name = "NO_DIZZINESS")]
    NoDizziness,
    #[df_token(token_name = "NO_DRINK")]
    NoDrink,
    #[df_token(token_name = "NO_EAT")]
    NoEat,
    #[df_token(token_name = "NO_FEVERS")]
    NoFevers,
    #[df_token(token_name = "NO_PHYS_ATT_GAIN")]
    NoPhysAttGain,
    #[df_token(token_name = "NO_PHYS_ATT_RUST")]
    NoPhysAttRust,
    #[df_token(token_name = "NO_SLEEP")]
    NoSleep,
    #[df_token(token_name = "NO_SPRING")]
    NoSpring,
    #[df_token(token_name = "NO_SUMMER")]
    NoSummer,
    #[df_token(token_name = "NO_THOUGHT_CENTER_FOR_MOVEMENT")]
    NoThoughtCenterForMovement,
    #[df_token(token_name = "NO_UNIT_TYPE_COLOR")]
    NoUnitTypeColor,
    #[df_token(token_name = "NO_VEGETATION_PERTURB")]
    NoVegetationPerturb,
    #[df_token(token_name = "NO_WINTER")]
    NoWinter,
    #[df_token(token_name = "OPPOSED_TO_LIFE")]
    OpposedToLife,
    #[df_token(token_name = "OUTSIDER_CONTROLLABLE")]
    OutsiderControllable,
    #[df_token(token_name = "PACK_ANIMAL")]
    PackAnimal,
    #[df_token(token_name = "PARALYZEIMMUNE")]
    Paralyzeimmune,
    #[df_token(token_name = "PATTERNFLIER")]
    Patternflier,
    #[df_token(token_name = "PEARL")]
    Pearl,
    #[df_token(token_name = "PET")]
    Pet,
    #[df_token(token_name = "PET_EXOTIC")]
    PetExotic,
    #[df_token(token_name = "POWER")]
    Power,
    #[df_token(token_name = "REMAINS_ON_VERMIN_BITE_DEATH")]
    RemainsOnVerminBiteDeath,
    #[df_token(token_name = "REMAINS_UNDETERMINED")]
    RemainsUndetermined,
    #[df_token(token_name = "RETURNS_VERMIN_KILLS_TO_OWNER")]
    ReturnsVerminKillsToOwner,
    #[df_token(token_name = "SEMIMEGABEAST")]
    SemiMegabeast,
    #[df_token(token_name = "SLOW_LEARNER")]
    SlowLearner,
    #[df_token(token_name = "SPOUSE_CONVERSION_TARGET")]
    SpouseConversionTarget,
    #[df_token(token_name = "SPOUSE_CONVERTER")]
    SpouseConverter,
    #[df_token(token_name = "SPREAD_EVIL_SPHERES_IF_RULER")]
    SpreadEvilSpheresIfRuler,
    #[df_token(token_name = "STANCE_CLIMBER")]
    StanceClimber,
    #[df_token(token_name = "STRANGE_MOODS")]
    StrangeMoods,
    #[df_token(token_name = "SUPERNATURAL")]
    Supernatural,
    #[df_token(token_name = "TENDONS")]
    Tendons,
    #[df_token(token_name = "THICKWEB")]
    Thickweb,
    #[df_token(token_name = "TITAN")]
    Titan,
    #[df_token(token_name = "TRAINABLE_HUNTING")]
    TrainableHunting,
    #[df_token(token_name = "TRAINABLE_WAR")]
    TrainableWar,
    #[df_token(token_name = "TRANCES")]
    Trances,
    #[df_token(token_name = "TRAPAVOID")]
    Trapavoid,
    #[df_token(token_name = "UNIQUE_DEMON")]
    UniqueDemon,
    #[df_token(token_name = "UTTERANCES")]
    Utterances,
    #[df_token(token_name = "VEGETATION")]
    Vegetation,
    #[df_token(token_name = "VERMIN_GOBBLER")]
    VerminGobbler,
    #[df_token(token_name = "VERMIN_HATEABLE")]
    VerminHateable,
    #[df_token(token_name = "VERMIN_MICRO")]
    VerminMicro,
    #[df_token(token_name = "VERMIN_NOFISH")]
    VerminNoFish,
    #[df_token(token_name = "VERMIN_NOROAM")]
    VerminNoRoam,
    #[df_token(token_name = "VERMIN_NOTRAP")]
    VerminNoTrap,
    #[df_token(token_name = "VESPERTINE")]
    Vespertine,
    #[df_token(token_name = "WAGON_PULLER")]
    WagonPuller,
    #[df_token(token_name = "WEBBER")]
    Webber,
    #[df_token(token_name = "WEBIMMUNE")]
    Webimmune,
}
impl Default for CasteFlagEnum {
    fn default() -> Self {
        Self::AdoptsOwner
    }
}
