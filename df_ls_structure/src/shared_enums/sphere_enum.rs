use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum SphereEnum {
    /// Sphere: `AGRICULTURE`
    /// - Friend Spheres: `FOOD`, `FERTILITY`, `RAIN`
    #[df_token(token_name = "AGRICULTURE")]
    Agriculture,
    /// Sphere: `ANIMALS`
    /// - Friend Spheres: `PLANTS`
    /// - Parent/Child Spheres: `NATURE`
    #[df_token(token_name = "ANIMALS")]
    Animals,
    /// Sphere: `ART`
    /// - Friend Spheres: `INSPIRATION`, `BEAUTY`
    /// - Parent/Child Spheres: `DANCE`, `MUSIC`, `PAINTING`, `POETRY`, `SONG`
    #[df_token(token_name = "ART")]
    Art,
    /// Sphere: `BALANCE`
    #[df_token(token_name = "BALANCE")]
    Balance,
    /// Sphere: `BEAUTY`
    /// - Friend Spheres: `ART`
    /// - Precluded Spheres: `BLIGHT`, `DEFORMITY`, `DISEASE`, `MUCK`
    #[df_token(token_name = "BEAUTY")]
    Beauty,
    /// Sphere: `BIRTH`
    /// - Friend Spheres: `CHILDREN`, `CREATION`, `FAMILY`, `MARRIAGE`, `PREGNANCY`, `REBIRTH`, `YOUTH`
    #[df_token(token_name = "BIRTH")]
    Birth,
    /// Sphere: `BLIGHT`
    /// - Friend Spheres: `DISEASE`, `DEATH`
    /// - Precluded Spheres: `BEAUTY`, `FOOD`, `FERTILITY`, `HEALING`
    #[df_token(token_name = "BLIGHT")]
    Blight,
    /// Sphere: `BOUNDARIES`
    /// - Parent/Child Spheres: `COASTS`
    #[df_token(token_name = "BOUNDARIES")]
    Boundaries,
    /// Sphere: `CAVERNS`
    /// - Friend Spheres: `MOUNTAINS`, `EARTH`
    #[df_token(token_name = "CAVERNS")]
    Caverns,
    /// Sphere: `CHAOS`
    /// - Friend Spheres: `WAR`
    /// - Precluded Spheres: `DISCIPLINE`, `ORDER`, `LAWS`
    #[df_token(token_name = "CHAOS")]
    Chaos,
    /// Sphere: `CHARITY`
    /// - Friend Spheres: `GENEROSITY`, `SACRIFICE`
    /// - Precluded Spheres: `JEALOUSY`
    #[df_token(token_name = "CHARITY")]
    Charity,
    /// Sphere: `CHILDREN`
    /// - Friend Spheres: `BIRTH`, `FAMILY`, `YOUTH`, `PREGNANCY`
    #[df_token(token_name = "CHILDREN")]
    Children,
    /// Sphere: `COASTS`
    /// - Friend Spheres: `LAKES`, `OCEANS`
    /// - Parent/Child Spheres: `BOUNDARIES`
    #[df_token(token_name = "COASTS")]
    Coasts,
    /// Sphere: `CONSOLATION`
    /// - Precluded Spheres: `MISERY`
    #[df_token(token_name = "CONSOLATION")]
    Consolation,
    /// Sphere: `COURAGE`
    /// - Parent/Child Spheres: `VALOR`
    #[df_token(token_name = "COURAGE")]
    Courage,
    /// Sphere: `CRAFTS`
    /// - Friend Spheres: `CREATION`, `LABOR`, `METALS`
    #[df_token(token_name = "CRAFTS")]
    Crafts,
    /// Sphere: `CREATION`
    /// - Friend Spheres: `CRAFTS`, `BIRTH`, `PREGNANCY`, `REBIRTH`
    #[df_token(token_name = "CREATION")]
    Creation,
    /// Sphere: `DANCE`
    /// - Friend Spheres: `FESTIVALS`, `MUSIC`, `REVELRY`
    /// - Parent/Child Spheres: `ART`
    #[df_token(token_name = "DANCE")]
    Dance,
    /// Sphere: `DARKNESS`
    /// - Friend Spheres: `NIGHT`
    /// - Precluded Spheres: `DAWN`, `DAY`, `LIGHT`, `TWILIGHT`, `SUN`
    #[df_token(token_name = "DARKNESS")]
    Darkness,
    /// Sphere: `DAWN`
    /// - Friend Spheres: `SUN`, `TWILIGHT`
    /// - Precluded Spheres: `NIGHT`, `DAY`, `DARKNESS`
    #[df_token(token_name = "DAWN")]
    Dawn,
    /// Sphere: `DAY`
    /// - Friend Spheres: `LIGHT`, `SUN`
    /// - Precluded Spheres: `DARKNESS`, `NIGHT`, `DAWN`, `DUSK`, `DREAMS`, `NIGHTMARES`, `TWILIGHT`
    #[df_token(token_name = "DAY")]
    Day,
    /// Sphere: `DEATH`
    /// - Friend Spheres: `BLIGHT`, `DISEASE`, `MURDER`, `REBIRTH`, `SUICIDE`, `WAR`
    /// - Precluded Spheres: `HEALING`, `LONGEVITY`, `YOUTH`
    #[df_token(token_name = "DEATH")]
    Death,
    /// Sphere: `DEFORMITY`
    /// - Friend Spheres: `DISEASE`
    /// - Precluded Spheres: `BEAUTY`
    #[df_token(token_name = "DEFORMITY")]
    Deformity,
    /// Sphere: `DEPRAVITY`
    /// - Friend Spheres: `LUST`
    /// - Precluded Spheres: `LAWS`
    #[df_token(token_name = "DEPRAVITY")]
    Depravity,
    /// Sphere: `DISCIPLINE`
    /// - Friend Spheres: `LAWS`, `ORDER`
    /// - Precluded Spheres: `CHAOS`
    #[df_token(token_name = "DISCIPLINE")]
    Discipline,
    /// Sphere: `DISEASE`
    /// - Friend Spheres: `BLIGHT`, `DEATH`, `DEFORMITY`
    /// - Precluded Spheres: `BEAUTY`, `HEALING`
    #[df_token(token_name = "DISEASE")]
    Disease,
    /// Sphere: `DREAMS`
    /// - Friend Spheres: `NIGHT`, `NIGHTMARES`
    /// - Precluded Spheres: `DAY`
    #[df_token(token_name = "DREAMS")]
    Dreams,
    /// Sphere: `DUSK`
    /// - Friend Spheres: `TWILIGHT`
    /// - Precluded Spheres: `NIGHT`, `DAY`
    #[df_token(token_name = "DUSK")]
    Dusk,
    /// Sphere: `DUTY`
    /// - Friend Spheres: `ORDER`
    #[df_token(token_name = "DUTY")]
    Duty,
    /// Sphere: `EARTH`
    /// - Friend Spheres: `CAVERNS`, `MOUNTAINS`, `VOLCANOS`
    /// - Parent/Child Spheres: `METALS`, `MINERALS`, `SALT`
    #[df_token(token_name = "EARTH")]
    Earth,
    /// Sphere: `FAMILY`
    /// - Friend Spheres: `BIRTH`, `CHILDREN`, `MARRIAGE`, `PREGNANCY`
    #[df_token(token_name = "FAMILY")]
    Family,
    /// Sphere: `FAME`
    /// - Friend Spheres: `RUMORS`
    /// - Precluded Spheres: `SILENCE`
    #[df_token(token_name = "FAME")]
    Fame,
    /// Sphere: `FATE`
    /// - Precluded Spheres: `LUCK`
    #[df_token(token_name = "FATE")]
    Fate,
    /// Sphere: `FERTILITY`
    /// - Friend Spheres: `AGRICULTURE`, `FOOD`, `RAIN`
    /// - Precluded Spheres: `BLIGHT`
    #[df_token(token_name = "FERTILITY")]
    Fertility,
    /// Sphere: `FESTIVALS`
    /// - Friend Spheres: `DANCE`, `MUSIC`, `REVELRY`, `SONG`
    /// - Precluded Spheres: `MISERY`
    #[df_token(token_name = "FESTIVALS")]
    Festivals,
    /// Sphere: `FIRE`
    /// - Friend Spheres: `METALS`, `SUN`, `VOLCANOS`
    /// - Precluded Spheres: `WATER`, `OCEANS`, `LAKES`, `RIVERS`
    #[df_token(token_name = "FIRE")]
    Fire,
    /// Sphere: `FISH`
    /// - Friend Spheres: `OCEANS`, `LAKES`, `RIVERS`, `WATER`
    /// - Parent/Child Spheres: `NATURE`, `ANIMALS`
    #[df_token(token_name = "FISH")]
    Fish,
    /// Sphere: `FISHING`
    /// - Friend Spheres: `FISH`, `HUNTING`
    #[df_token(token_name = "FISHING")]
    Fishing,
    /// Sphere: `FOOD`
    /// - Friend Spheres: `AGRICULTURE`, `FERTILITY`
    /// - Precluded Spheres: `BLIGHT`
    #[df_token(token_name = "FOOD")]
    Food,
    /// Sphere: `FORGIVENESS`
    /// - Friend Spheres: `MERCY`
    /// - Precluded Spheres: `REVENGE`
    #[df_token(token_name = "FORGIVENESS")]
    Forgiveness,
    /// Sphere: `FORTRESSES`
    /// - Friend Spheres: `WAR`
    #[df_token(token_name = "FORTRESSES")]
    Fortresses,
    /// Sphere: `FREEDOM`
    /// - Precluded Spheres: `ORDER`
    #[df_token(token_name = "FREEDOM")]
    Freedom,
    /// Sphere: `GAMBLING`
    /// - Friend Spheres: `GAMES`, `LUCK`
    #[df_token(token_name = "GAMBLING")]
    Gambling,
    /// Sphere: `GAMES`
    /// - Friend Spheres: `GAMBLING`, `LUCK`
    #[df_token(token_name = "GAMES")]
    Games,
    /// Sphere: `GENEROSITY`
    /// - Friend Spheres: `CHARITY`, `SACRIFICE`
    #[df_token(token_name = "GENEROSITY")]
    Generosity,
    /// Sphere: `HAPPINESS`
    /// - Friend Spheres: `REVELRY`
    /// - Precluded Spheres: `MISERY`
    #[df_token(token_name = "HAPPINESS")]
    Happiness,
    /// Sphere: `HEALING`
    /// - Precluded Spheres: `DISEASE`, `BLIGHT`, `DEATH`
    #[df_token(token_name = "HEALING")]
    Healing,
    /// Sphere: `HOSPITALITY`
    #[df_token(token_name = "HOSPITALITY")]
    Hospitality,
    /// Sphere: `HUNTING`
    /// - Friend Spheres: `FISHING`
    #[df_token(token_name = "HUNTING")]
    Hunting,
    /// Sphere: `INSPIRATION`
    /// - Friend Spheres: `ART`, `PAINTING`, `POETRY`
    #[df_token(token_name = "INSPIRATION")]
    Inspiration,
    /// Sphere: `JEALOUSY`
    /// - Precluded Spheres: `CHARITY`
    #[df_token(token_name = "JEALOUSY")]
    Jealousy,
    /// Sphere: `JEWELS`
    /// - Friend Spheres: `MINERALS`, `WEALTH`
    #[df_token(token_name = "JEWELS")]
    Jewels,
    /// Sphere: `JUSTICE`
    /// - Friend Spheres: `LAWS`
    #[df_token(token_name = "JUSTICE")]
    Justice,
    /// Sphere: `LABOR`
    /// - Friend Spheres: `CRAFTS`
    #[df_token(token_name = "LABOR")]
    Labor,
    /// Sphere: `LAKES`
    /// - Friend Spheres: `COASTS`, `FISH`, `OCEANS`, `RIVERS`
    /// - Parent/Child Spheres: `WATER`
    /// - Precluded Spheres: `FIRE`
    #[df_token(token_name = "LAKES")]
    Lakes,
    /// Sphere: `LAWS`
    /// - Friend Spheres: `DISCIPLINE`, `JUSTICE`, `OATHS`, `ORDER`
    /// - Precluded Spheres: `CHAOS`, `DEPRAVITY`, `MURDER`, `THEFT`
    #[df_token(token_name = "LAWS")]
    Laws,
    /// Sphere: `LIES`
    /// - Friend Spheres: `TREACHERY`, `TRICKERY`
    /// - Precluded Spheres: `TRUTH`
    #[df_token(token_name = "LIES")]
    Lies,
    /// Sphere: `LIGHT`
    /// - Friend Spheres: `DAY`, `RAINBOWS`, `SUN`
    /// - Precluded Spheres: `DARKNESS`, `TWILIGHT`
    #[df_token(token_name = "LIGHT")]
    Light,
    /// Sphere: `LIGHTNING`
    /// - Friend Spheres: `RAIN`, `STORMS`, `THUNDER`
    /// - Parent/Child Spheres: `WEATHER`
    #[df_token(token_name = "LIGHTNING")]
    Lightning,
    /// Sphere: `LONGEVITY`
    /// - Friend Spheres: `YOUTH`
    /// - Precluded Spheres: `DEATH`
    #[df_token(token_name = "LONGEVITY")]
    Longevity,
    /// Sphere: `LOVE`
    #[df_token(token_name = "LOVE")]
    Love,
    /// Sphere: `LOYALTY`
    /// - Friend Spheres: `OATHS`
    /// - Precluded Spheres: `TREACHERY`
    #[df_token(token_name = "LOYALTY")]
    Loyalty,
    /// Sphere: `LUCK`
    /// - Friend Spheres: `GAMBLING`, `GAMES`
    /// - Precluded Spheres: `FATE`
    #[df_token(token_name = "LUCK")]
    Luck,
    /// Sphere: `LUST`
    /// - Friend Spheres: `DEPRAVITY`
    #[df_token(token_name = "LUST")]
    Lust,
    /// Sphere: `MARRIAGE`
    /// - Friend Spheres: `BIRTH`, `FAMILY`, `OATHS`, `PREGNANCY`
    #[df_token(token_name = "MARRIAGE")]
    Marriage,
    /// Sphere: `MERCY`
    /// - Friend Spheres: `FORGIVENESS`
    /// - Precluded Spheres: `REVENGE`
    #[df_token(token_name = "MERCY")]
    Mercy,
    /// Sphere: `METALS`
    /// - Friend Spheres: `CRAFTS`, `FIRE`, `MINERALS`
    /// - Parent/Child Spheres: `EARTH`
    #[df_token(token_name = "METALS")]
    Metals,
    /// Sphere: `MINERALS`
    /// - Friend Spheres: `JEWELS`, `METALS`
    /// - Parent/Child Spheres: `EARTH`
    #[df_token(token_name = "MINERALS")]
    Minerals,
    /// Sphere: `MISERY`
    /// - Friend Spheres: `TORTURE`
    /// - Precluded Spheres: `CONSOLATION`, `FESTIVALS`, `REVELRY`, `HAPPINESS`
    #[df_token(token_name = "MISERY")]
    Misery,
    /// Sphere: `MIST`
    #[df_token(token_name = "MIST")]
    Mist,
    /// Sphere: `MOON`
    /// - Friend Spheres: `NIGHT`, `SKY`
    #[df_token(token_name = "MOON")]
    Moon,
    /// Sphere: `MOUNTAINS`
    /// - Friend Spheres: `CAVERNS`, `EARTH`, `VOLCANOS`
    #[df_token(token_name = "MOUNTAINS")]
    Mountains,
    /// Sphere: `MUCK`
    /// - Precluded Spheres: `BEAUTY`
    #[df_token(token_name = "MUCK")]
    Muck,
    /// Sphere: `MURDER`
    /// - Friend Spheres: `DEATH`
    /// - Precluded Spheres: `LAWS`
    #[df_token(token_name = "MURDER")]
    Murder,
    /// Sphere: `MUSIC`
    /// - Friend Spheres: `DANCE`, `FESTIVALS`, `REVELRY`, `SONG`
    /// - Parent/Child Spheres: `ART`
    /// - Precluded Spheres: `SILENCE`
    #[df_token(token_name = "MUSIC")]
    Music,
    /// Sphere: `NATURE`
    /// - Friend Spheres: `RAIN`, `SUN`, `WATER`, `WEATHER`
    /// - Parent/Child Spheres: `ANIMALS`, `FISH`, `PLANTS`, `TREES`
    #[df_token(token_name = "NATURE")]
    Nature,
    /// Sphere: `NIGHT`
    /// - Friend Spheres: `DARKNESS`, `DREAMS`, `MOON`, `NIGHTMARES`, `STARS`
    /// - Precluded Spheres: `DAY`, `DAWN`, `DUSK`, `TWILIGHT`
    #[df_token(token_name = "NIGHT")]
    Night,
    /// Sphere: `NIGHTMARES`
    /// - Friend Spheres: `DREAMS`, `NIGHT`
    /// - Precluded Spheres: `DAY`
    #[df_token(token_name = "NIGHTMARES")]
    Nightmares,
    /// Sphere: `OATHS`
    /// - Friend Spheres: `LAWS`, `LOYALTY`, `MARRIAGE`
    /// - Precluded Spheres: `TREACHERY`
    #[df_token(token_name = "OATHS")]
    Oaths,
    /// Sphere: `OCEANS`
    /// - Friend Spheres: `COASTS`, `FISH`, `LAKES`, `RIVERS`, `SALT`
    /// - Parent/Child Spheres: `WATER`
    /// - Precluded Spheres: `FIRE`
    #[df_token(token_name = "OCEANS")]
    Oceans,
    /// Sphere: `ORDER`
    /// - Friend Spheres: `DISCIPLINE`, `DUTY`, `LAWS`
    /// - Precluded Spheres: `CHAOS`, `FREEDOM`
    #[df_token(token_name = "ORDER")]
    Order,
    /// Sphere: `PAINTING`
    /// - Friend Spheres: `INSPIRATION`
    /// - Parent/Child Spheres: `ART`
    #[df_token(token_name = "PAINTING")]
    Painting,
    /// Sphere: `PEACE`
    #[df_token(token_name = "PEACE")]
    Peace,
    /// Sphere: `PERSUASION`
    /// - Friend Spheres: `POETRY`, `SPEECH`
    #[df_token(token_name = "PERSUASION")]
    Persuasion,
    /// Sphere: `PLANTS`
    /// - Friend Spheres: `ANIMALS`, `RAIN`
    /// - Parent/Child Spheres: `NATURE`
    #[df_token(token_name = "PLANTS")]
    Plants,
    /// Sphere: `POETRY`
    /// - Friend Spheres: `INSPIRATION`, `PERSUASION`, `SONG`, `WRITING`
    /// - Parent/Child Spheres: `ART`
    #[df_token(token_name = "POETRY")]
    Poetry,
    /// Sphere: `PREGNANCY`
    /// - Friend Spheres: `BIRTH`, `CHILDREN`, `CREATION`, `FAMILY`, `MARRIAGE`
    #[df_token(token_name = "PREGNANCY")]
    Pregnancy,
    /// Sphere: `RAIN`
    /// - Friend Spheres: `AGRICULTURE`, `FERTILITY`, `LIGHTNING`, `NATURE`, `PLANTS`, `RAINBOWS`, `STORMS`, `THUNDER`, `TREES`
    /// - Parent/Child Spheres: `WATER`, `WEATHER`
    #[df_token(token_name = "RAIN")]
    Rain,
    /// Sphere: `RAINBOWS`
    /// - Friend Spheres: `LIGHT`, `RAIN`, `SKY`
    /// - Parent/Child Spheres: `WEATHER`
    #[df_token(token_name = "RAINBOWS")]
    Rainbows,
    /// Sphere: `REBIRTH`
    /// - Friend Spheres: `BIRTH`, `CREATION`, `DEATH`
    #[df_token(token_name = "REBIRTH")]
    Rebirth,
    /// Sphere: `REVELRY`
    /// - Friend Spheres: `DANCE`, `FESTIVALS`, `HAPPINESS`, `MUSIC`, `SONG`
    /// - Precluded Spheres: `MISERY`
    #[df_token(token_name = "REVELRY")]
    Revelry,
    /// Sphere: `REVENGE`
    /// - Precluded Spheres: `FORGIVENESS`, `MERCY`
    #[df_token(token_name = "REVENGE")]
    Revenge,
    /// Sphere: `RIVERS`
    /// - Friend Spheres: `FISH`, `LAKES`, `OCEANS`
    /// - Parent/Child Spheres: `WATER`
    /// - Precluded Spheres: `FIRE`
    #[df_token(token_name = "RIVERS")]
    Rivers,
    /// Sphere: `RULERSHIP`
    #[df_token(token_name = "RULERSHIP")]
    Rulership,
    /// Sphere: `RUMORS`
    /// - Friend Spheres: `FAME`
    #[df_token(token_name = "RUMORS")]
    Rumors,
    /// Sphere: `SACRIFICE`
    /// - Friend Spheres: `CHARITY`, `GENEROSITY`
    /// - Precluded Spheres: `WEALTH`
    #[df_token(token_name = "SACRIFICE")]
    Sacrifice,
    /// Sphere: `SALT`
    /// - Friend Spheres: `OCEANS`
    /// - Parent/Child Spheres: `EARTH`
    #[df_token(token_name = "SALT")]
    Salt,
    /// Sphere: `SCHOLARSHIP`
    /// - Friend Spheres: `WISDOM`, `WRITING`
    #[df_token(token_name = "SCHOLARSHIP")]
    Scholarship,
    /// Sphere: `SEASONS`
    #[df_token(token_name = "SEASONS")]
    Seasons,
    /// Sphere: `SILENCE`
    /// - Precluded Spheres: `FAME`, `MUSIC`
    #[df_token(token_name = "SILENCE")]
    Silence,
    /// Sphere: `SKY`
    /// - Friend Spheres: `MOON`, `RAINBOWS`, `SUN`, `STARS`, `WEATHER`, `WIND`
    #[df_token(token_name = "SKY")]
    Sky,
    /// Sphere: `SONG`
    /// - Friend Spheres: `FESTIVALS`, `MUSIC`, `POETRY`, `REVELRY`
    /// - Parent/Child Spheres: `ART`
    #[df_token(token_name = "SONG")]
    Song,
    /// Sphere: `SPEECH`
    /// - Friend Spheres: `PERSUASION`
    #[df_token(token_name = "SPEECH")]
    Speech,
    /// Sphere: `STARS`
    /// - Friend Spheres: `NIGHT`, `SKY`
    #[df_token(token_name = "STARS")]
    Stars,
    /// Sphere: `STORMS`
    /// - Friend Spheres: `LIGHTNING`, `RAIN`, `THUNDER`
    /// - Parent/Child Spheres: `WEATHER`
    #[df_token(token_name = "STORMS")]
    Storms,
    /// Sphere: `STRENGTH`
    #[df_token(token_name = "STRENGTH")]
    Strength,
    /// Sphere: `SUICIDE`
    /// - Friend Spheres: `DEATH`
    #[df_token(token_name = "SUICIDE")]
    Suicide,
    /// Sphere: `SUN`
    /// - Friend Spheres: `DAWN`, `DAY`, `FIRE`, `LIGHT`, `NATURE`, `SKY`
    /// - Precluded Spheres: `DARKNESS`
    #[df_token(token_name = "SUN")]
    Sun,
    /// Sphere: `THEFT`
    /// - Precluded Spheres: `LAWS`, `TRADE`
    #[df_token(token_name = "THEFT")]
    Theft,
    /// Sphere: `THRALLDOM`
    #[df_token(token_name = "THRALLDOM")]
    Thralldom,
    /// Sphere: `THUNDER`
    /// - Friend Spheres: `LIGHTNING`, `RAIN`, `STORMS`
    /// - Parent/Child Spheres: `WEATHER`
    #[df_token(token_name = "THUNDER")]
    Thunder,
    /// Sphere: `TORTURE`
    /// - Friend Spheres: `MISERY`
    #[df_token(token_name = "TORTURE")]
    Torture,
    /// Sphere: `TRADE`
    /// - Friend Spheres: `WEALTH`
    /// - Precluded Spheres: `THEFT`
    #[df_token(token_name = "TRADE")]
    Trade,
    /// Sphere: `TRAVELERS`
    #[df_token(token_name = "TRAVELERS")]
    Travelers,
    /// Sphere: `TREACHERY`
    /// - Friend Spheres: `LIES`, `TRICKERY`
    /// - Precluded Spheres: `LOYALTY`, `OATHS`
    #[df_token(token_name = "TREACHERY")]
    Treachery,
    /// Sphere: `TREES`
    /// - Friend Spheres: `RAIN`
    /// - Parent/Child Spheres: `NATURE`, `PLANTS`
    #[df_token(token_name = "TREES")]
    Trees,
    /// Sphere: `TRICKERY`
    /// - Friend Spheres: `LIES`, `TREACHERY`
    /// - Precluded Spheres: `TRUTH`
    #[df_token(token_name = "TRICKERY")]
    Trickery,
    /// Sphere: `TRUTH`
    /// - Precluded Spheres: `LIES`, `TRICKERY`
    #[df_token(token_name = "TRUTH")]
    Truth,
    /// Sphere: `TWILIGHT`
    /// - Friend Spheres: `DAWN`, `DUSK`
    /// - Precluded Spheres: `LIGHT`, `DARKNESS`, `DAY`, `NIGHT`
    #[df_token(token_name = "TWILIGHT")]
    Twilight,
    /// Sphere: `VALOR`
    /// - Friend Spheres: `WAR`
    /// - Parent/Child Spheres: `COURAGE`
    #[df_token(token_name = "VALOR")]
    Valor,
    /// Sphere: `VICTORY`
    /// - Friend Spheres: `WAR`
    #[df_token(token_name = "VICTORY")]
    Victory,
    /// Sphere: `VOLCANOS`
    /// - Friend Spheres: `EARTH`, `FIRE`, `MOUNTAINS`
    #[df_token(token_name = "VOLCANOS")]
    Volcanos,
    /// Sphere: `WAR`
    /// - Friend Spheres: `CHAOS`, `DEATH`, `FORTRESSES`, `VALOR`, `VICTORY`
    #[df_token(token_name = "WAR")]
    War,
    /// Sphere: `WATER`
    /// - Friend Spheres: `FISH`, `NATURE`
    /// - Parent/Child Spheres: `LAKES`, `OCEANS`, `RIVERS`, `RAIN`
    /// - Precluded Spheres: `FIRE`
    #[df_token(token_name = "WATER")]
    Water,
    /// Sphere: `WEALTH`
    /// - Friend Spheres: `JEWELS`, `TRADE`
    /// - Precluded Spheres: `SACRIFICE`
    #[df_token(token_name = "WEALTH")]
    Wealth,
    /// Sphere: `WEATHER`
    /// - Friend Spheres: `NATURE`, `SKY`
    /// - Parent/Child Spheres: `LIGHTNING`, `RAIN`, `RAINBOWS`, `STORMS`, `THUNDER`, `WIND`
    #[df_token(token_name = "WEATHER")]
    Weather,
    /// Sphere: `WIND`
    /// - Friend Spheres: `SKY`
    /// - Parent/Child Spheres: `WEATHER`
    #[df_token(token_name = "WIND")]
    Wind,
    /// Sphere: `WISDOM`
    /// - Friend Spheres: `SCHOLARSHIP`
    #[df_token(token_name = "WISDOM")]
    Wisdom,
    /// Sphere: `WRITING`
    /// - Friend Spheres: `POETRY`, `SCHOLARSHIP`
    #[df_token(token_name = "WRITING")]
    Writing,
    /// Sphere: `YOUTH`
    /// - Friend Spheres: `BIRTH`, `CHILDREN`, `LONGEVITY`
    /// - Precluded Spheres: `DEATH`
    #[df_token(token_name = "YOUTH")]
    Youth,
}

impl Default for SphereEnum {
    fn default() -> Self {
        Self::Agriculture
    }
}
