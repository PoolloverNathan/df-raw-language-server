use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

// TODO: investigate if these are hardcoded to only be applicable to cerain body part CATEGORY's;
// the wiki groups these according to body part category, but categories are arbitrary and user definable!
// TODO: Fill this out; this likely matches to specific description string ingame; maybe use that here.
#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum BpAppModifersEnum {
    /// The height of the body part.
    #[df_token(token_name = "HEIGHT")]
    Height,
    /// The length of the body part.
    #[df_token(token_name = "LENGTH")]
    Length,
    /// The broadness of the body part.
    #[df_token(token_name = "BROADNESS")]
    Broadness,
    #[df_token(token_name = "CLOSE_SET")]
    CloseSet,
    #[df_token(token_name = "DEEP_SET")]
    DeepSet,
    #[df_token(token_name = "ROUND_VS_NARROW")]
    RoundVsNarrow,
    #[df_token(token_name = "LARGE_IRIS")]
    LargeIris,
    #[df_token(token_name = "THICKNESS")]
    Thickness,
    #[df_token(token_name = "UPTURNED")]
    Upturned,
    #[df_token(token_name = "CONVEX")]
    Convex,
    #[df_token(token_name = "SPLAYED_OUT")]
    SplayedOut,
    #[df_token(token_name = "HANGING_LOBES")]
    HangingLobes,
    #[df_token(token_name = "GAPS")]
    Gaps,
    #[df_token(token_name = "HIGH_CHEEKBONES")]
    HighCheekbones,
    #[df_token(token_name = "BROAD_CHIN")]
    BroadChin,
    #[df_token(token_name = "JUTTING_CHIN")]
    JuttingChin,
    #[df_token(token_name = "SQUARE_CHIN")]
    SquareChin,
    #[df_token(token_name = "DEEP_VOICE")]
    DeepVoice,
    #[df_token(token_name = "RASPY_VOICE")]
    RaspyVoice,
}
impl Default for BpAppModifersEnum {
    fn default() -> Self {
        Self::Height
    }
}
