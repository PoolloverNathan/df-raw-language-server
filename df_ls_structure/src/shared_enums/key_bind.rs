use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
/// For more key binds see: issue #76
pub enum KeyBindEnum {
    #[df_token(token_name = "HOTKEY_STILL_BREW")]
    HotkeyStillBrew,
    #[df_token(token_name = "HOTKEY_KITCHEN_RENDER_FAT")]
    HotkeyKitchenRenderFat,
    #[df_token(token_name = "CUSTOM_A")]
    CustomA,
    #[df_token(token_name = "CUSTOM_B")]
    CustomB,
    #[df_token(token_name = "CUSTOM_C")]
    CustomC,
    #[df_token(token_name = "CUSTOM_D")]
    CustomD,
    #[df_token(token_name = "CUSTOM_E")]
    CustomE,
    #[df_token(token_name = "CUSTOM_F")]
    CustomF,
    #[df_token(token_name = "CUSTOM_G")]
    CustomG,
    #[df_token(token_name = "CUSTOM_H")]
    CustomH,
    #[df_token(token_name = "CUSTOM_I")]
    CustomI,
    #[df_token(token_name = "CUSTOM_J")]
    CustomJ,
    #[df_token(token_name = "CUSTOM_K")]
    CustomK,
    #[df_token(token_name = "CUSTOM_L")]
    CustomL,
    #[df_token(token_name = "CUSTOM_M")]
    CustomM,
    #[df_token(token_name = "CUSTOM_N")]
    CustomN,
    #[df_token(token_name = "CUSTOM_O")]
    CustomO,
    #[df_token(token_name = "CUSTOM_P")]
    CustomP,
    #[df_token(token_name = "CUSTOM_Q")]
    CustomQ,
    #[df_token(token_name = "CUSTOM_R")]
    CustomR,
    #[df_token(token_name = "CUSTOM_S")]
    CustomS,
    #[df_token(token_name = "CUSTOM_T")]
    CustomT,
    #[df_token(token_name = "CUSTOM_U")]
    CustomU,
    #[df_token(token_name = "CUSTOM_V")]
    CustomV,
    #[df_token(token_name = "CUSTOM_W")]
    CustomW,
    #[df_token(token_name = "CUSTOM_X")]
    CustomX,
    #[df_token(token_name = "CUSTOM_Y")]
    CustomY,
    #[df_token(token_name = "CUSTOM_Z")]
    CustomZ,
    #[df_token(token_name = "CUSTOM_SHIFT_A")]
    CustomShiftA,
    #[df_token(token_name = "CUSTOM_SHIFT_B")]
    CustomShiftB,
    #[df_token(token_name = "CUSTOM_SHIFT_C")]
    CustomShiftC,
    #[df_token(token_name = "CUSTOM_SHIFT_D")]
    CustomShiftD,
    #[df_token(token_name = "CUSTOM_SHIFT_E")]
    CustomShiftE,
    #[df_token(token_name = "CUSTOM_SHIFT_F")]
    CustomShiftF,
    #[df_token(token_name = "CUSTOM_SHIFT_G")]
    CustomShiftG,
    #[df_token(token_name = "CUSTOM_SHIFT_H")]
    CustomShiftH,
    #[df_token(token_name = "CUSTOM_SHIFT_I")]
    CustomShiftI,
    #[df_token(token_name = "CUSTOM_SHIFT_J")]
    CustomShiftJ,
    #[df_token(token_name = "CUSTOM_SHIFT_K")]
    CustomShiftK,
    #[df_token(token_name = "CUSTOM_SHIFT_L")]
    CustomShiftL,
    #[df_token(token_name = "CUSTOM_SHIFT_M")]
    CustomShiftM,
    #[df_token(token_name = "CUSTOM_SHIFT_N")]
    CustomShiftN,
    #[df_token(token_name = "CUSTOM_SHIFT_O")]
    CustomShiftO,
    #[df_token(token_name = "CUSTOM_SHIFT_P")]
    CustomShiftP,
    #[df_token(token_name = "CUSTOM_SHIFT_Q")]
    CustomShiftQ,
    #[df_token(token_name = "CUSTOM_SHIFT_R")]
    CustomShiftR,
    #[df_token(token_name = "CUSTOM_SHIFT_S")]
    CustomShiftS,
    #[df_token(token_name = "CUSTOM_SHIFT_T")]
    CustomShiftT,
    #[df_token(token_name = "CUSTOM_SHIFT_U")]
    CustomShiftU,
    #[df_token(token_name = "CUSTOM_SHIFT_V")]
    CustomShiftV,
    #[df_token(token_name = "CUSTOM_SHIFT_W")]
    CustomShiftW,
    #[df_token(token_name = "CUSTOM_SHIFT_X")]
    CustomShiftX,
    #[df_token(token_name = "CUSTOM_SHIFT_Y")]
    CustomShiftY,
    #[df_token(token_name = "CUSTOM_SHIFT_Z")]
    CustomShiftZ,
    #[df_token(token_name = "CUSTOM_CTRL_A")]
    CustomCtrlA,
    #[df_token(token_name = "CUSTOM_CTRL_B")]
    CustomCtrlB,
    #[df_token(token_name = "CUSTOM_CTRL_C")]
    CustomCtrlC,
    #[df_token(token_name = "CUSTOM_CTRL_D")]
    CustomCtrlD,
    #[df_token(token_name = "CUSTOM_CTRL_E")]
    CustomCtrlE,
    #[df_token(token_name = "CUSTOM_CTRL_F")]
    CustomCtrlF,
    #[df_token(token_name = "CUSTOM_CTRL_G")]
    CustomCtrlG,
    #[df_token(token_name = "CUSTOM_CTRL_H")]
    CustomCtrlH,
    #[df_token(token_name = "CUSTOM_CTRL_I")]
    CustomCtrlI,
    #[df_token(token_name = "CUSTOM_CTRL_J")]
    CustomCtrlJ,
    #[df_token(token_name = "CUSTOM_CTRL_K")]
    CustomCtrlK,
    #[df_token(token_name = "CUSTOM_CTRL_L")]
    CustomCtrlL,
    #[df_token(token_name = "CUSTOM_CTRL_M")]
    CustomCtrlM,
    #[df_token(token_name = "CUSTOM_CTRL_N")]
    CustomCtrlN,
    #[df_token(token_name = "CUSTOM_CTRL_O")]
    CustomCtrlO,
    #[df_token(token_name = "CUSTOM_CTRL_P")]
    CustomCtrlP,
    #[df_token(token_name = "CUSTOM_CTRL_Q")]
    CustomCtrlQ,
    #[df_token(token_name = "CUSTOM_CTRL_R")]
    CustomCtrlR,
    #[df_token(token_name = "CUSTOM_CTRL_S")]
    CustomCtrlS,
    #[df_token(token_name = "CUSTOM_CTRL_T")]
    CustomCtrlT,
    #[df_token(token_name = "CUSTOM_CTRL_U")]
    CustomCtrlU,
    #[df_token(token_name = "CUSTOM_CTRL_V")]
    CustomCtrlV,
    #[df_token(token_name = "CUSTOM_CTRL_W")]
    CustomCtrlW,
    #[df_token(token_name = "CUSTOM_CTRL_X")]
    CustomCtrlX,
    #[df_token(token_name = "CUSTOM_CTRL_Y")]
    CustomCtrlY,
    #[df_token(token_name = "CUSTOM_CTRL_Z")]
    CustomCtrlZ,
    #[df_token(token_name = "CUSTOM_ALT_A")]
    CustomAltA,
    #[df_token(token_name = "CUSTOM_ALT_B")]
    CustomAltB,
    #[df_token(token_name = "CUSTOM_ALT_C")]
    CustomAltC,
    #[df_token(token_name = "CUSTOM_ALT_D")]
    CustomAltD,
    #[df_token(token_name = "CUSTOM_ALT_E")]
    CustomAltE,
    #[df_token(token_name = "CUSTOM_ALT_F")]
    CustomAltF,
    #[df_token(token_name = "CUSTOM_ALT_G")]
    CustomAltG,
    #[df_token(token_name = "CUSTOM_ALT_H")]
    CustomAltH,
    #[df_token(token_name = "CUSTOM_ALT_I")]
    CustomAltI,
    #[df_token(token_name = "CUSTOM_ALT_J")]
    CustomAltJ,
    #[df_token(token_name = "CUSTOM_ALT_K")]
    CustomAltK,
    #[df_token(token_name = "CUSTOM_ALT_L")]
    CustomAltL,
    #[df_token(token_name = "CUSTOM_ALT_M")]
    CustomAltM,
    #[df_token(token_name = "CUSTOM_ALT_N")]
    CustomAltN,
    #[df_token(token_name = "CUSTOM_ALT_O")]
    CustomAltO,
    #[df_token(token_name = "CUSTOM_ALT_P")]
    CustomAltP,
    #[df_token(token_name = "CUSTOM_ALT_Q")]
    CustomAltQ,
    #[df_token(token_name = "CUSTOM_ALT_R")]
    CustomAltR,
    #[df_token(token_name = "CUSTOM_ALT_S")]
    CustomAltS,
    #[df_token(token_name = "CUSTOM_ALT_T")]
    CustomAltT,
    #[df_token(token_name = "CUSTOM_ALT_U")]
    CustomAltU,
    #[df_token(token_name = "CUSTOM_ALT_V")]
    CustomAltV,
    #[df_token(token_name = "CUSTOM_ALT_W")]
    CustomAltW,
    #[df_token(token_name = "CUSTOM_ALT_X")]
    CustomAltX,
    #[df_token(token_name = "CUSTOM_ALT_Y")]
    CustomAltY,
    #[df_token(token_name = "CUSTOM_ALT_Z")]
    CustomAltZ,
}

impl Default for KeyBindEnum {
    fn default() -> Self {
        Self::CustomA
    }
}
