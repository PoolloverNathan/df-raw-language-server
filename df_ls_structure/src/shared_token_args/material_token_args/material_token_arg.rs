use super::{CoalMaterialEnum, MaterialTypeEnum};
use crate::{CreatureToken, InorganicToken, NoMatGlossEnum, NoneEnum, PlantToken};
use df_ls_core::{Choose, Reference, ReferenceTo};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_syntax_analysis::{Token, TokenDeserialize, TryFromArgumentGroup};
use serde::{Deserialize, Serialize};

/// Wiki page: https://dwarffortresswiki.org/index.php/Material_token
/// Some local creature/plant materials are separated out because they are only allowed
/// in some cases, see #114.
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq, Eq)]
pub struct MaterialTokenArg {
    // |---0------|---------1--------|----2----|---3-----|
    // [TOKEN_NAME:PLANT_MAT         :APPLE    :FRUIT    ]
    // [TOKEN_NAME:CREATURE_MAT      :ANIMAL   :WOOL     ]
    // [TOKEN_NAME:INORGANIC                   :QUICKLIME]
    // [TOKEN_NAME:LYE                                   ]
    /// Argument group 1: with Enum arguments
    pub material: MaterialTypeEnum,
}

// Deserialize a token with following pattern: `[REF:material_token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(MaterialTokenArg);

impl TryFromArgumentGroup for MaterialTokenArg {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Safe first argument (is not token_name) for error case
        let arg0 = match token.get_current_arg() {
            Ok(arg) => Ok(arg.clone()),
            Err(err) => Err(err),
        };
        // Arg 0
        let reference_arg0 =
            Reference::try_from_argument_group(token, source, diagnostics, add_diagnostics_on_err)?;
        // Match on Arg 0
        let material_type = match reference_arg0.0.as_ref() {
            "INORGANIC" | "STONE" | "METAL" => {
                // TODO: add alias warning
                // Arg 1
                let material_name = ReferenceTo::<InorganicToken>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Inorganic(material_name)
            }
            "CREATURE_MAT" => {
                // Arg 1
                let material_object_id = ReferenceTo::<CreatureToken>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                // Arg 2
                let material_name = Reference::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::CreatureMat((material_object_id, material_name))
            }
            "PLANT_MAT" => {
                // Arg 1
                let material_object_id = ReferenceTo::<PlantToken>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                // Arg 2
                let material_name = Reference::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::PlantMat((material_object_id, material_name))
            }
            "AMBER" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Amber(material_name)
            }
            "CORAL" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Coral(material_name)
            }
            "GLASS_GREEN" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::GlassGreen(material_name)
            }
            "GLASS_CLEAR" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::GlassClear(material_name)
            }
            "GLASS_CRYSTAL" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::GlassCrystal(material_name)
            }
            "WATER" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Water(material_name)
            }
            "COAL" => {
                // Arg 1
                let material_name = CoalMaterialEnum::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Coal(material_name)
            }
            "POTASH" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Potash(material_name)
            }
            "LYE" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Lye(material_name)
            }
            "ASH" => {
                // Arg 1
                let material_name =
                    Option::<Choose<NoneEnum, NoMatGlossEnum>>::try_from_argument_group(
                        token,
                        source,
                        diagnostics,
                        add_diagnostics_on_err,
                    )?;
                MaterialTypeEnum::Ash(material_name)
            }
            "PEARLASH" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Pearlash(material_name)
            }
            "MUD" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Mud(material_name)
            }
            "VOMIT" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Vomit(material_name)
            }
            "SALT" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Salt(material_name)
            }
            "FILTH_B" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::FilthB(material_name)
            }
            "FILTH_Y" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::FilthY(material_name)
            }
            "UNKNOWN_SUBSTANCE" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::UnknownSubstance(material_name)
            }
            "GRIME" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Grime(material_name)
            }
            _ => {
                Self::diagnostics_wrong_enum_type(
                    &arg0?,
                    vec![
                        "INORGANIC",
                        "STONE",
                        "METAL",
                        "CREATURE_MAT",
                        "PLANT_MAT",
                        "AMBER",
                        "CORAL",
                        "GLASS_GREEN",
                        "GLASS_CLEAR",
                        "GLASS_CRYSTAL",
                        "WATER",
                        "COAL",
                        "POTASH",
                        "LYE",
                        "ASH",
                        "PEARLASH",
                        "MUD",
                        "VOMIT",
                        "SALT",
                        "FILTH_B",
                        "FILTH_Y",
                        "UNKNOWN_SUBSTANCE",
                        "GRIME",
                    ],
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                );
                return Err(());
            }
        };
        Ok(MaterialTokenArg {
            material: material_type,
        })
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
    use df_ls_syntax_analysis::test_tree_structure;
    use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

    #[test]
    fn test_material_correct() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [REF:INORGANIC:IRON]
                [REF:STONE:SLADE]
                [REF:METAL:IRON]
                [REF:CREATURE_MAT:HONEY_BEE:HONEY]
                [REF:PLANT_MAT:NETHER_CAP:WOOD]

                [REF:AMBER:NONE]
                [REF:AMBER]
                [REF:CORAL:NONE]
                [REF:GLASS_GREEN:NONE]
                [REF:GLASS_GREEN]
                [REF:GLASS_CLEAR:NONE]
                [REF:GLASS_CRYSTAL:NONE]
                [REF:WATER:NONE]
                [REF:WATER]
                [REF:COAL:CHARCOAL]
                [REF:COAL:COKE]
                [REF:COAL:NO_MATGLOSS]
                [REF:POTASH:NONE]
                [REF:ASH:NONE] // Both look to be valid judging for RAW collection
                [REF:ASH:NO_MATGLOSS] // Both look to be valid judging for RAW collection
                [REF:PEARLASH:NONE]
                [REF:LYE]
                [REF:LYE:NONE]
                [REF:MUD]
                [REF:MUD:NONE]
                [REF:VOMIT]
                [REF:VOMIT:NONE]
                [REF:SALT:NONE]
                [REF:FILTH_B:NONE]
                [REF:FILTH_Y:NONE]
                [REF:UNKNOWN_SUBSTANCE:NONE]
                [REF:GRIME:NONE]
                ",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec![])
        .add_test_syntax_diagnostics_ranges(vec![]);

        test_tree_structure!(
            test_builder,
            [
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Inorganic(ReferenceTo::new("IRON".to_owned())),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Inorganic(ReferenceTo::new("SLADE".to_owned())),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Inorganic(ReferenceTo::new("IRON".to_owned())),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::CreatureMat((
                        ReferenceTo::new("HONEY_BEE".to_owned()),
                        Reference("HONEY".to_owned())
                    )),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::PlantMat((
                        ReferenceTo::new("NETHER_CAP".to_owned()),
                        Reference("WOOD".to_owned())
                    )),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Amber(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Amber(None),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Coral(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::GlassGreen(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::GlassGreen(None),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::GlassClear(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::GlassCrystal(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Water(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Water(None),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Coal(CoalMaterialEnum::Charcoal),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Coal(CoalMaterialEnum::Coke),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Coal(CoalMaterialEnum::NoMatgloss),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Potash(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Ash(Some(Choose::Choice1(NoneEnum::None))),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Ash(Some(Choose::Choice2(NoMatGlossEnum::NoMatgloss))),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Pearlash(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Lye(None),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Lye(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Mud(None),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Mud(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Vomit(None),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Vomit(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Salt(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::FilthB(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::FilthY(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::UnknownSubstance(Some(NoneEnum::None)),
                },
                MaterialTokenArg => MaterialTokenArg {
                    material: MaterialTypeEnum::Grime(Some(NoneEnum::None)),
                },
            ]
        );
    }
}
