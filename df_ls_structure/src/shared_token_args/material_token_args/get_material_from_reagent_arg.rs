use crate::{NoneEnum, ReactionToken, ReagentToken};
use df_ls_core::{Choose, Reference, ReferenceTo};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_syntax_analysis::{Token, TryFromArgumentGroup};
use serde::{Deserialize, Serialize};

/// Specifies a material related to a reagent's material within a reaction.
/// `REAGENT_ID` must match a `[REAGENT]`, and `REACTION_PRODUCT_ID` must either match a
/// `[MATERIAL_REACTION_PRODUCT]` belonging to the reagent's material
/// or be equal to `NONE` to use the reagent's material itself.
// #[df_token(token_name = "GET_MATERIAL_FROM_REAGENT")]
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq, Eq)]
pub struct GetMaterialFromReagentArg(
    pub  (
        ReferenceTo<ReactionToken>,
        Choose<NoneEnum, ReferenceTo<ReagentToken>>,
    ),
);

impl TryFromArgumentGroup for GetMaterialFromReagentArg {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Safe first argument (is not token_name) for error case
        let arg0 = match token.get_current_arg() {
            Ok(arg) => Ok(arg.clone()),
            Err(err) => Err(err),
        };
        // Arg 0
        let reference_arg0 =
            Reference::try_from_argument_group(token, source, diagnostics, add_diagnostics_on_err)?;
        // Match on Arg 0
        let material_type = match reference_arg0.0.as_ref() {
            "GET_MATERIAL_FROM_REAGENT" => {
                // Arg 1
                let material_object_id = ReferenceTo::<ReactionToken>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                // Arg 2
                let material_name =
                    Choose::<NoneEnum, ReferenceTo<ReagentToken>>::try_from_argument_group(
                        token,
                        source,
                        diagnostics,
                        add_diagnostics_on_err,
                    )?;
                (material_object_id, material_name)
            }
            _ => {
                Self::diagnostics_wrong_enum_type(
                    &arg0?,
                    vec!["GET_MATERIAL_FROM_REAGENT"],
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                );
                return Err(());
            }
        };
        Ok(Self(material_type))
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
    use df_ls_syntax_analysis::test_tree_structure;
    use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

    #[test]
    fn test_material_correct() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [REF:GET_MATERIAL_FROM_REAGENT:PLANT:PRESS_PAPER_MAT]
                [REF:GET_MATERIAL_FROM_REAGENT:BONE:NONE]
                ",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec![])
        .add_test_syntax_diagnostics_ranges(vec![]);

        test_tree_structure!(
            test_builder,
            [
                (GetMaterialFromReagentArg,) => (GetMaterialFromReagentArg((
                    ReferenceTo::new("PLANT".to_owned()),
                    Choose::Choice2(ReferenceTo::new("PRESS_PAPER_MAT".to_owned()))
                )),),
                (GetMaterialFromReagentArg,) => (GetMaterialFromReagentArg((
                    ReferenceTo::new("BONE".to_owned()),
                    Choose::Choice1(NoneEnum::None)
                )),),
            ]
        );
    }
}
